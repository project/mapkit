<?php

namespace Drupal\mapkit\Exception;

/**
 * Exception for when an invalid location entry is provided.
 */
class InvalidLocationException extends \InvalidArgumentException {

  /**
   * The invalid location value that produced the exception.
   *
   * @var string
   */
  protected string $value;

  /**
   * Create a new instance of the the InvalidLocationException.
   *
   * @param string $location_value
   *   The location entry that triggered the invalid location exception.
   * @param int $code
   *   An internal error code.
   * @param \Throwable|null $prev
   *   A previous throwable or exception for error chaining.
   */
  public function __construct(string $location_value, int $code = 0, \Throwable $prev = NULL) {
    $message = sprintf('Invalid location "%s" could not be resolved.', $location_value);

    parent::__construct($message, $code, $prev);
    $this->value = $location_value;
  }

  /**
   * Get the invalid location value that cause this exception.
   *
   * @return string
   *   The invalid location value string.
   */
  public function getInvalidLocation(): string {
    return $this->value;
  }

}
