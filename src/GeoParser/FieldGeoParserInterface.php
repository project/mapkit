<?php

namespace Drupal\mapkit\GeoParser;

use Drupal\Core\Field\FieldItemListInterface;

/**
 * Interface for geo-parsers which support getting lat/long from entity fields.
 */
interface FieldGeoParserInterface extends GeoParserInterface {

  /**
   * Extracts the lat/long information from a content entity field.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $field_items
   *   The field items to extract lat/lng data from.
   *
   * @return array
   *   An array of lat/long values extracted from the field items.
   */
  public function parseField(FieldItemListInterface $field_items): array;

}
