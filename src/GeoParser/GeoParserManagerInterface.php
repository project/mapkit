<?php

namespace Drupal\mapkit\GeoParser;

use Drupal\toolshed\Strategy\StrategyManagerInterface;
use Drupal\views\Plugin\views\field\FieldHandlerInterface;

/**
 * Interface for the geo-parser plugins used to extract lat/long data.
 */
interface GeoParserManagerInterface extends StrategyManagerInterface {

  /**
   * Get a list of geo-parser plugin IDs keyed by the field or views plugin ID.
   *
   * @param string $type
   *   Either "field" or "view_handler" depending on which list to fetch.
   *
   * @return array
   *   Get an array of geo-parser plugin IDs keyed by the field or view handler
   *   plugin ID.
   */
  public function getParserByType($type): array;

  /**
   * Get the geo-parser for extracting the lat/long data from a field.
   *
   * @param string $field_type
   *   The field type to find a compatible field geo-parser for.
   *
   * @return \Drupal\mapkit\GeoParser\FieldGeoParserInterface|null
   *   A geo-parser compatible for the field type requested.
   */
  public function getFieldParser($field_type): ?FieldGeoParserInterface;

  /**
   * The geo-parser for extracting the lat/long data from a views result row.
   *
   * @param \Drupal\views\Plugin\views\field\FieldHandlerInterface $field
   *   The views field handler to find a compatible parser for.
   *
   * @return \Drupal\mapkit\GeoParser\ViewFieldGeoParserInterface|null
   *   A geo-parser compatible for the views field handler. NULL if no
   *   compatible parsers can be found.
   */
  public function getViewFieldParser(FieldHandlerInterface $field): ?ViewFieldGeoParserInterface;

}
