<?php

namespace Drupal\mapkit\GeoParser;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\toolshed\Strategy\ContainerInjectionStrategyInterface;
use Drupal\toolshed\Strategy\StrategyBase;
use Drupal\toolshed\Strategy\StrategyDefinitionInterface;
use Drupal\toolshed\Strategy\StrategyInterface;
use Drupal\views\Plugin\views\field\EntityField;
use Drupal\views\Plugin\views\field\FieldHandlerInterface;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Geo parser plugin for most views entity fields.
 */
class ViewFieldGeoParser extends StrategyBase implements ViewFieldGeoParserInterface, ContainerInjectionStrategyInterface {

  use LoggerChannelTrait;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The field geo_parser plugin manager.
   *
   * @var \Drupal\mapkit\GeoParser\GeoParserManagerInterface
   */
  protected $parserManager;

  /**
   * Creates a new instance of the ViewFieldGeoParser class.
   *
   * @param string $id
   *   The strategy ID.
   * @param \Drupal\toolshed\Strategy\StrategyDefinitionInterface $definition
   *   The strategy definition.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\mapkit\GeoParser\GeoParserManagerInterface $geo_parser_manager
   *   The field geo_parser plugin manager.
   */
  public function __construct(string $id, StrategyDefinitionInterface $definition, EntityFieldManagerInterface $entity_field_manager, GeoParserManagerInterface $geo_parser_manager) {
    parent::__construct($id, $definition);

    $this->entityFieldManager = $entity_field_manager;
    $this->parserManager = $geo_parser_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, string $id, StrategyDefinitionInterface $definition): StrategyInterface {
    return new static(
      $id,
      $definition,
      $container->get('entity_field.manager'),
      $container->get('strategy.manager.mapkit.geo_parser')
    );
  }

  /**
   * Get the entity field type from the views field instance.
   *
   * @param \Drupal\views\Plugin\views\field\FieldHandlerInterface $field
   *   The views field handler to extract the entity field information for.
   *
   * @return string
   *   The machine name of the field type.
   *
   * @throws \InvalidArgumentException
   *   If field type cannot be found for this views field handler.
   */
  protected function getFieldType(FieldHandlerInterface $field) {
    $entityType = $field->definition['entity_type'] ?? NULL;
    $fieldName = $field->definition['field_name'] ?? NULL;

    $storageDefs = $this->entityFieldManager->getFieldStorageDefinitions($entityType);
    if (isset($storageDefs[$fieldName])) {
      return $storageDefs[$fieldName]->getType();
    }

    throw new \InvalidArgumentException('Invalid field handler. Views field needs to be of type EntityField.');
  }

  /**
   * {@inheritdoc}
   */
  public function isApplicable(FieldHandlerInterface $field) {
    if ($field instanceof EntityField) {
      $fieldType = $this->getFieldType($field);
      $fieldPluginIds = $this->parserManager->getParserByType('field');

      return isset($fieldPluginIds[$fieldType]);
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function parseViewField(ResultRow $row, FieldHandlerInterface $field): array {
    if ($entity = $field->getEntity($row)) {
      /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
      /** @var \Drupal\views\Plugin\views\field\FieldPluginBase $field */
      $fieldName = $field->definition['field_name'];
      $fieldType = $this->getFieldType($field);

      try {
        $parser = $this->parserManager->getFieldParser($fieldType);

        if ($parser && $entity->hasField($fieldName)) {
          return $parser->parseField($entity->{$fieldName});
        }
      }
      catch (PluginException $e) {
        // Unable to get the geo-parser plugin, which is likely to happen if
        // a providing module is no longer available. Log the error, and return
        // no coordinate data.
        $this->getLogger('mapkit')->error('Unable to read @field_name from view field with error: @message', [
          '@field_name' => $fieldName,
          '@message' => $e->getMessage(),
        ]);
      }
    }

    return [];
  }

}
