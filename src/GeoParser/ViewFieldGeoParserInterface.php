<?php

namespace Drupal\mapkit\GeoParser;

use Drupal\views\Plugin\views\field\FieldHandlerInterface;
use Drupal\views\ResultRow;

/**
 * Interface for geo-parsers which support getting lat/long from views fields.
 */
interface ViewFieldGeoParserInterface extends GeoParserInterface {

  /**
   * Is this geo-parser compatible with the views field handler.
   *
   * @param \Drupal\views\Plugin\views\field\FieldHandlerInterface $field
   *   The field handler to check if this handler is compatible.
   *
   * @return bool
   *   TRUE if this parser works with the views field requested.
   */
  public function isApplicable(FieldHandlerInterface $field);

  /**
   * Extract the lat/long coordinates for a field from a views result row.
   *
   * @param \Drupal\views\ResultRow $row
   *   The result row to get the coordinate data from.
   * @param \Drupal\views\Plugin\views\field\FieldHandlerInterface $field
   *   The views field handler to get values from the result row.
   *
   * @return array
   *   An array of lat/long values extracted from the views result row.
   */
  public function parseViewField(ResultRow $row, FieldHandlerInterface $field): array;

}
