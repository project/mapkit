<?php

namespace Drupal\mapkit\GeoParser;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\search_api\Item\FieldInterface;
use Drupal\toolshed\Strategy\ContainerInjectionStrategyInterface;
use Drupal\toolshed\Strategy\StrategyBase;
use Drupal\toolshed\Strategy\StrategyDefinitionInterface;
use Drupal\toolshed\Strategy\StrategyInterface;
use Drupal\views\Plugin\views\field\FieldHandlerInterface;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * GeoParser for reading Search API fields, which are of data type "location".
 */
class SearchApiParser extends StrategyBase implements ViewFieldGeoParserInterface, ContainerInjectionStrategyInterface {

  /**
   * The entity storage handler for fetching Search API index entities.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $indexStorage;

  /**
   * Creates a new instance of the SearchApiParser plugin.
   *
   * @param string $id
   *   The plugin ID.
   * @param \Drupal\toolshed\Strategy\StrategyDefinitionInterface $definition
   *   The plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(string $id, StrategyDefinitionInterface $definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($id, $definition);

    $this->indexStorage = $entity_type_manager->getStorage('search_api_index');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, string $id, StrategyDefinitionInterface $definition): StrategyInterface {
    return new static(
      $id,
      $definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * Get the Search API field pointed to by the views field plugin.
   *
   * @param \Drupal\views\Plugin\views\field\FieldHandlerInterface $field
   *   The views field plugin handler to find the Search API field.
   *
   * @return \Drupal\search_api\Item\FieldInterface|null
   *   The Search API field pointed to by the views field if there is one.
   */
  protected function getSearchApiField(FieldHandlerInterface $field): ?FieldInterface {
    $searchApiName = $field->definition['search_api field'] ?? NULL;

    /** @var \Drupal\views\Plugin\views\field\FieldPluginBase $field */
    if ($searchApiName && preg_match('/^search_api_index_([a-z_]+)$/i', $field->table, $matches)) {
      if ($index = $this->indexStorage->load($matches[1])) {
        /** @var \Drupal\search_api\IndexInterface $index */
        return $index->getField($searchApiName);
      }
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function isApplicable(FieldHandlerInterface $field) {
    $searchField = $this->getSearchApiField($field);
    return $searchField && $searchField->getType() === 'location';
  }

  /**
   * {@inheritdoc}
   */
  public function parseViewField(ResultRow $row, FieldHandlerInterface $field): array {
    $values = [];
    foreach ($field->getValue($row) as $value) {
      [$lat, $lng] = explode(',', $value);

      $values[] = [
        'lat' => floatval($lat),
        'lng' => floatval($lng),
      ];
    }

    return $values;
  }

}
