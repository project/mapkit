<?php

namespace Drupal\mapkit\GeoParser;

use Drupal\toolshed\Strategy\StrategyInterface;

/**
 * Interface for geo-parser strategies to extract lat/long values from data.
 *
 * Geo-parser plugins should implement either:
 *  - \Drupal\mapkit\GeoParser\FieldGeoParserInterface -  entity field type
 *  - \Drupal\mapkit\GeoParser\ViewFieldGeoParserInterface - views field handler
 * and be described in a module's *.mapkit.geo_parser.yml file.
 *
 * @see \Drupal\mapkit\GeoParser\GeoParserManagerInterface
 * @see \Drupal\mapkit\GeoParser\GeoParserManager
 * @see \Drupal\mapkit\GeoParser\FieldGeoParserInterface
 * @see \Drupal\mapkit\GeoParser\ViewFieldGeoParserInterface
 * @see \Drupal\mapkit\GeoParser\FieldGeoParser
 */
interface GeoParserInterface extends StrategyInterface {
}
