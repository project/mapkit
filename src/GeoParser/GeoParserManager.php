<?php

namespace Drupal\mapkit\GeoParser;

use Drupal\toolshed\Strategy\StrategyManager;
use Drupal\views\Plugin\views\field\EntityField;
use Drupal\views\Plugin\views\field\FieldHandlerInterface;

/**
 * Default geo-parser plugin manager.
 */
class GeoParserManager extends StrategyManager implements GeoParserManagerInterface {

  /**
   * Find geo-parser plugin ID by field type.
   *
   * @var array
   */
  protected $parserByType;

  /**
   * {@inheritdoc}
   */
  protected function defaultStrategyClass(array $definition): string {
    throw new \InvalidArgumentException('Geo parser strategies must specify a class to construct objects from, no default class is available.');
  }

  /**
   * {@inheritdoc}
   */
  public function getParserByType($type): array {
    if (!isset($this->parserByType)) {
      $this->parserByType = [
        'field' => [],
        'view_handler' => [],
      ];

      foreach ($this->getDefinitions() as $id => $def) {
        if ($fields = $def->field ?? []) {
          $this->parserByType['field'] += array_fill_keys($fields, $id);
        }
        if ($handlers = $def->view_handler ?? []) {
          $this->parserByType['view_handler'] += array_fill_keys($handlers, $id);
        }
      }
    }

    return $this->parserByType[$type] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldParser($field_type): ?FieldGeoParserInterface {
    $strategies = $this->getParserByType('field');
    return isset($strategies[$field_type]) ? $this->getInstance($strategies[$field_type]) : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getViewFieldParser(FieldHandlerInterface $field): ?ViewFieldGeoParserInterface {
    $handlerId = $field->getPluginId();
    $viewHandlers = $this->getParserByType('view_handler');

    if (isset($viewHandlers[$handlerId])) {
      $strategy = $this->getInstance($viewHandlers[$handlerId]);
    }
    elseif ($field instanceof EntityField) {
      // Special case for views fields that inherit from the views entity field
      // handler (includes search_api_field) to catch view field plugins that
      // are still likely to work without needing to explicitly have them listed
      // in the geo_parser definitions.
      $strategy = $this->getInstance($viewHandlers['field']);
    }

    /** @var \Drupal\mapkit\GeoParser\ViewFieldGeoParserInterface $strategy */
    return (isset($strategy) && $strategy->isApplicable($field)) ? $strategy : NULL;
  }

}
