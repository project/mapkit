<?php

namespace Drupal\mapkit\GeoParser;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\toolshed\Strategy\StrategyBase;

/**
 * Geo-parser to extract lat/long from "geofield" content fields.
 */
class GeoLocationParser extends StrategyBase implements FieldGeoParserInterface {

  /**
   * {@inheritdoc}
   */
  public function parseField(FieldItemListInterface $field_items): array {
    $values = [];
    foreach ($field_items as $delta => $item) {
      $values[$delta] = [
        'lat' => floatval($item->lat),
        'lng' => floatval($item->lng),
      ];
    }

    return $values;
  }

}
