<?php

namespace Drupal\mapkit\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\FormElement;

/**
 * Creates a form element for supporting a Lat/Long bounding rectangle.
 *
 * @FormElement("latlng_rect")
 */
class LatLngRect extends FormElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    return [
      '#input' => TRUE,
      '#process' => [
        static::class . '::processElement',
        static::class . '::processGroup',
      ],
      '#element_validate' => [
        static::class . '::validateElement',
      ],
      '#pre_render' => [],
      '#theme_wrappers' => ['form_element'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    if ($input !== FALSE && $input !== NULL) {
      return $input;
    }
  }

  /**
   * Process the textfield element into the form element components.
   *
   * @param array $element
   *   Reference to the form element array passed from the form definition.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Current form build and state information.
   * @param array $complete_form
   *   Reference to the complete form definition.
   *
   * @return array
   *   The processed element.
   */
  public static function processElement(array &$element, FormStateInterface $form_state, array &$complete_form) {
    $element['#tree'] = TRUE;

    $element['top_left'] = [
      '#type' => 'latlng_point',
      '#title' => t('Top left'),
    ];

    $element['bottom_right'] = [
      '#type' => 'latlng_point',
      '#title' => t('Bottom right'),
    ];

    if (!empty($element['#value'])) {
      $element['top_left']['#default_value'] = $element['#value']['top_left'];
      $element['bottom_right']['#default_value'] = $element['#value']['bottom_right'];
    }

    return $element;
  }

  /**
   * Validate that the CSS classes entered here are in a valid CSS format.
   *
   * @param array $element
   *   Array definition of this css class element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object, containing the build, info and values of the
   *   current form.
   */
  public static function validateElement(array $element, FormStateInterface $form_state) {
    if (!empty($element['#value']['top_left']) && !empty($element['#value']['bottom_right'])) {
      $topLeft = $element['#value']['top_left'];
      $botRight = $element['#value']['bottom_right'];

      if ($topLeft['lat'] > $botRight['lat']) {
        $form_state->setError($element['bottom_right']['lat'], t('Bottom latitude value must be larger than the top latitude value.'));
      }

      if ($topLeft['lng'] > $botRight['lng']) {
        $form_state->setError($element['bottom_right']['lng'], t('The right longitude value must be larger than the left longitude value.'));
      }
    }
  }

}
