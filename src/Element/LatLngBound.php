<?php

namespace Drupal\mapkit\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\Details;
use Drupal\Core\Render\Element\FormElement;

/**
 * Creates a form element for supporting a Lat/Long bounding rectangle.
 *
 * @FormElement("latlng_bound")
 */
class LatLngBound extends FormElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    return [
      '#input' => TRUE,
      '#process' => [
        static::class . '::processElement',
        static::class . '::processGroup',
      ],
      '#element_validate' => [
        static::class . '::validateElement',
      ],
      '#pre_render' => [
        static::class . '::preRenderGroup',
      ],
      '#theme_wrappers' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    if ($input !== FALSE && $input !== NULL) {
      return array_filter([
        'top' => floatval($input['top']),
        'left' => floatval($input['left']),
        'bottom' => floatval($input['bottom']),
        'right' => floatval($input['right']),
      ]);
    }
  }

  /**
   * Process the textfield element into the form element components.
   *
   * @param array $element
   *   Reference to the form element array passed from the form definition.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Current form build and state information.
   * @param array $complete_form
   *   Reference to the complete form definition.
   *
   * @return array
   *   The processed element.
   */
  public static function processElement(array &$element, FormStateInterface $form_state, array &$complete_form) {
    $value = $element['#value'] ?? [];

    $element['#tree'] = TRUE;
    $element['top'] = [
      '#type' => 'textfield',
      '#title' => t('Top'),
      '#pattern' => '[\-+]?\d*(\.\d+)?',
      '#size' => 12,
      '#default_value' => $value['top'] ?? NULL,
    ];

    $element['left'] = [
      '#type' => 'textfield',
      '#title' => t('Left'),
      '#pattern' => '[\-+]?\d*(\.\d+)?',
      '#size' => 12,
      '#default_value' => $value['left'] ?? NULL,
    ];

    $element['bottom'] = [
      '#type' => 'textfield',
      '#title' => t('Bottom'),
      '#pattern' => '[\-+]?\d*(\.\d+)?',
      '#size' => 12,
      '#default_value' => $value['bottom'] ?? NULL,
    ];

    $element['right'] = [
      '#type' => 'textfield',
      '#title' => t('Right'),
      '#pattern' => '[\-+]?\d*(\.\d+)?',
      '#size' => 12,
      '#default_value' => $value['right'] ?? NULL,
    ];

    // If a custom theme wrapper has not been set, then set the appropriate
    // form elements wrapper based on if this needs to be collapsible.
    if (empty($element['#theme_wrappers'])) {
      if (empty($element['#collapsible'])) {
        $element['#theme_wrappers'][] = 'fieldset';
      }
      else {
        $element['#theme_wrappers'][] = 'details';
        array_unshift($element['#pre_render'], Details::class . '::preRenderDetails');
      }
    }

    return $element;
  }

  /**
   * Validate that the CSS classes entered here are in a valid CSS format.
   *
   * @param array $element
   *   Array definition of this css class element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object, containing the build, info and values of the
   *   current form.
   */
  public static function validateElement(array $element, FormStateInterface $form_state) {
    $values = $element['#value'] ?? FALSE;

    if ($values) {
      if (floatval($values['top']) < floatval($values['bottom'])) {
        $form_state->setError($element['bottom'], t('Bottom latitude value must be larger than the top latitude value.'));
      }

      if (floatval($values['left']) < floatval($values['right'])) {
        $form_state->setError($element['right'], t('The right longitude value must be larger than the left longitude value.'));
      }
    }
  }

}
