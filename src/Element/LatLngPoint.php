<?php

namespace Drupal\mapkit\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\FormElement;

/**
 * Creates a form element for supporting a Lat/Long point.
 *
 * @FormElement("latlng_point")
 */
class LatLngPoint extends FormElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    return [
      '#input' => TRUE,
      '#required' => FALSE,
      '#process' => [
        static::class . '::processElement',
        static::class . '::processGroup',
      ],
      '#element_validate' => [
        static::class . '::validateElement',
      ],
      '#pre_render' => [],
      '#theme_wrappers' => ['fieldset'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    if ($input !== FALSE && $input !== NULL) {
      return [
        'lat' => floatval($input['lat']),
        'lng' => floatval($input['lng']),
      ];
    }
  }

  /**
   * Process the point into a latitude and longitude value entries.
   *
   * @param array $element
   *   Reference to the form element array passed from the form definition.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Current form build and state information.
   * @param array $complete_form
   *   Reference to the complete form.
   *
   * @return array
   *   The processed element.
   */
  public static function processElement(array &$element, FormStateInterface $form_state, array &$complete_form) {
    $element['#tree'] = TRUE;

    $element['lat'] = [
      '#type' => 'textfield',
      '#title' => t('Latitude'),
      '#pattern' => '[\-+]?\d*(\.\d+)?',
      '#size' => 12,
    ];

    $element['lng'] = [
      '#type' => 'textfield',
      '#title' => t('Longitude'),
      '#pattern' => '[\-+]?\d*(\.\d+)?',
      '#size' => 12,
    ];

    if (!empty($element['#value'])) {
      $element['lat']['#default_value'] = $element['#value']['lat'];
      $element['lng']['#default_value'] = $element['#value']['lng'];
    }

    return $element;
  }

  /**
   * Validate that the latitude and longitude values.
   *
   * @param array $element
   *   Array definition of the latitude and longitude values.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object, containing the build, info and values of the
   *   current form.
   */
  public static function validateElement(array $element, FormStateInterface $form_state) {
    $values = $element['#value'];

    if (!empty($values['lat']) && ($values['lat'] > 90 || $values['lat'] < -90)) {
      $form_state->setError($element['lat'], t('Latitude values must be between -90 and 90 degrees.'));
    }

    if (!empty($values['lng']) && ($values['lng'] > 180 || $values['lng'] < -180)) {
      $form_state->setError($element['lng'], t('Longitude values must be between -180 and 180 degrees.'));
    }
  }

}
