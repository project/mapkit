<?php

namespace Drupal\mapkit\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\FormElement;
use Drupal\toolshed\Strategy\Exception\StrategyException;

/**
 * Form element for creating a location autocomplete widget.
 *
 * This utilizes map providers that support autocomplete functionality to
 * provide autocomplete functionality to help users populate and resolve
 * a geo-location.
 *
 * @FormElement("mapkit_autocomplete")
 */
class MapkitAutocomplete extends FormElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    return [
      '#theme' => 'mapkit_autocomplete',
      '#input' => FALSE,
      '#required' => FALSE,
      '#disabled' => NULL,
      '#size' => 60,
      '#placeholder' => '',
      '#maxlength' => 255,
      '#data_keys' => ['latlng'],
      '#handler' => NULL,
      '#bounds' => NULL,
      '#filter_pattern' => NULL,
      '#device_location' => FALSE,
      '#autocomplete_min_length' => 2,
      '#autocomplete_delay' => 200,
      '#process' => [
        static::class . '::processElement',
        static::class . '::processGroup',
      ],
      '#element_validate' => [
        static::class . '::geolocationValidate',
      ],
      '#pre_render' => [
        static::class . '::preRenderGroup',
      ],
    ];
  }

  /**
   * List of data-keys available for fetching places data for.
   *
   * @return array
   *   List data-keys available to fetch from the geocoded values.
   */
  public static function allowedDataKeys() {
    return [
      'country',
      'postal_code',
      'administrative_area_level_1',
      'administrative_area_level_2',
      'locality',
      'street_address',
      'latlng',
    ];
  }

  /**
   * Element validation callback to properly flag requirements and value format.
   *
   * @param array $element
   *   Reference to the form element being validated.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form build info, input and values.
   * @param array $complete_form
   *   Reference to the entire rest of the form.
   */
  public static function geolocationValidate(array &$element, FormStateInterface $form_state, array &$complete_form) {
    if (!empty($element['#value']['latlng'])) {
      [$lat, $lng] = explode(',', $element['#value']['latlng']);

      $lat = floatval($lat);
      $lng = floatval($lng);

      // If latitude or longitude values are out of range, or invalid set them
      // to zero, unless a location is required.
      if (is_nan($lat) || is_nan($lng) || $lat > 90 || $lat < -90 || $lng > 180 || $lng < -180) {
        $form_state->setValue($element['#parents'], '0,0');
        $element['#value']['latlng'] = '0,0';
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    $value = [];

    if ($input && is_array($input)) {
      $value = array_filter($input);
    }
    elseif (!empty($element['#default_value'])) {
      $value = $element['#default_value'];

      if (is_string($value)) {
        $value = ['text' => $value];
      }
      elseif (!is_array($value)) {
        $value = [];
      }
    }

    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public static function processElement(&$element, FormStateInterface $form_state, &$complete_form) {
    $value = $element['#value'];

    $element['#needs_validation'] = FALSE;
    $element['#tree'] = TRUE;
    $element['text'] = [
      '#theme_wrappers' => [],
      '#type' => 'textfield',
      '#required' => $element['#required'],
      '#size' => $element['#size'],
      '#disabled' => $element['#disabled'],
      '#maxlength' => $element['#maxlength'],
      '#default_value' => $value['text'] ?? NULL,
      '#placeholder' => $element['#placeholder'] ?? NULL,
      '#attributes' => [
        'class' => ['mapkit-autocomplete__text'],
      ],
    ];

    // Iterate through each of the data components.
    foreach (array_intersect($element['#data_keys'], static::allowedDataKeys()) as $key) {
      $element[$key] = [
        '#theme_wrappers' => [],
        '#type' => 'hidden',
        '#default_value' => $value[$key] ?? NULL,
        '#disabled' => $element['#disabled'],
        '#attributes' => [
          'data-key' => $key,
          'class' => [
            'mapkit-autocomplete__value',
          ],
        ],
      ];
    }

    try {
      /** @var \Drupal\toolshed\Strategy\StrategyManagerInterface $acManager */
      $acManager = \Drupal::service('strategy.manager.mapkit.autocomplete');
      /** @var \Drupal\mapkit\Autocomplete\AutocompleteDefinitionInterface $handler */
      $handler = $acManager->getInstance($element['#handler']);

      $element['#attributes']['data-autocomplete'] = json_encode(array_filter([
        'handler' => $handler->getHandler(),
        'matchType' => $element['#autocomplete_match_type'],
        'minLength' => intval($element['#autocomplete_min_length']),
        'delay' => intval($element['#autocomplete_delay']),
        'bounds' => $element['#bounds'] ?: NULL,
        'pattern' => $element['#filter_pattern'] ?: NULL,
      ]));

      $element['#attached']['library'][] = 'mapkit/autocomplete';
      $element['#attributes']['class'][] = 'mapkit-autocomplete';
      unset($element['#attributes']['data-drupal-selector']);

      $element['#attached']['library'] = array_merge($handler->getLibraries(), $element['#attached']['library']);
    }
    catch (StrategyException $e) {
      // Missing autocomplete provider - don't load any libraries.
    }

    // Disable these to appease the FAPI form element build process.
    $element['#required'] = FALSE;
    unset($element['#maxlength']);
    return $element;
  }

}
