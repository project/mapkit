<?php

namespace Drupal\mapkit\EventSubscriber;

use Drupal\search_api\Event\MappingViewsHandlersEvent;
use Drupal\search_api\Event\SearchApiEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * The mapkit subscriber for Search API events.
 *
 * Adds Mapkit plugins as Views handlers for location types.
 */
class SearchApiSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      SearchApiEvents::MAPPING_VIEWS_HANDLERS => 'onViewsHandlerMapping',
    ];
  }

  /**
   * Add Search API views plugin handlers for the location data types.
   *
   * @param \Drupal\search_api\Event\MappingViewsHandlersEvent $event
   *   The mapping views handler event with the definitions to alter.
   */
  public function onViewsHandlerMapping(MappingViewsHandlersEvent $event): void {
    $mapping = &$event->getHandlerMapping();
    $mapping['location']['filter']['id'] = 'mapkit_location_filter';
    $mapping['location']['argument']['id'] = 'mapkit_location_argument';
  }

}
