<?php

namespace Drupal\mapkit;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Form\SubformStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Trait which provides map provider and marker configuration form methods.
 *
 * Trait helps with building elements for map provider selection and
 * configuration. Also provides a consistent way to manage the markers based
 * on map provider.
 *
 * Use MapConfigureFormTrait::mapConfigElementValidate() on the
 * $element["config"] when normal form submit and validation don't get called
 * (i.e. field formatter) and it is preferred to use:
 *   - MapConfigureFormTrait::mapConfigValidate()
 *   - MapConfigureFormTrait::mapConfigSubmit()
 * During form validation and submission when possible.
 */
trait MapConfigureFormTrait {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The map provider manager.
   *
   * @var \Drupal\mapkit\Plugin\MapProviderManagerInterface
   */
  protected $mapProviderManager;

  /**
   * The mapkit marker plugin manager.
   *
   * @var \Drupal\mapkit\Plugin\MarkerPluginManagerInterface
   */
  protected $markerManager;

  /**
   * Get the entity type manager.
   *
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   *   An instance of the entity type manager.
   */
  protected function getEntityTypeManager() {
    if (!$this->entityTypeManager) {
      $this->entityTypeManager = \Drupal::entityTypeManager();
    }

    return $this->entityTypeManager;
  }

  /**
   * Get the map provider plugin manager.
   *
   * @return \Drupal\mapkit\Plugin\MapProviderManagerInterface
   *   The map provider plugin manager.
   */
  protected function getMapProviderManager() {
    if (!$this->mapProviderManager) {
      $this->mapProviderManager = \Drupal::service('plugin.manager.mapkit.map');
    }

    return $this->mapProviderManager;
  }

  /**
   * Get the marker plugin manager.
   *
   * @return \Drupal\mapkit\Plugin\MarkerPluginManagerInterface
   *   The marker plugin manager.
   */
  protected function getMarkerManager() {
    if (!$this->markerManager) {
      $this->markerManager = \Drupal::service('plugin.manager.mapkit.marker');
    }

    return $this->markerManager;
  }

  /**
   * Get the value suggested for HTML element ID attribute of the map wrapper.
   *
   * This value will be used to identity the HTML element in Javascript to apply
   * the map display to. This value will be run through
   * \Drupal\Component\Utility\Html::getUniqueId() in the map preprocess to
   * ensure multiple instances will still work and get the correct settings.
   *
   * @return string
   *   An ID to use for the HTML element containing the map output.
   */
  abstract protected function getHtmlDomId();

  /**
   * Get the list of marker set configurations available for a map provider.
   *
   * @param string $providerId
   *   The map provider plugin ID to fetch compatible marker sets for.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup[]|string[]
   *   A list of marker sets labels of compatible marker sets available for
   *   the map provider reqested.
   */
  protected function getMarkerSetOptions($providerId) {
    $markerSets = $this->getEntityTypeManager()
      ->getStorage('mapkit_marker_set')
      ->loadByProperties(['map_type' => $providerId]);

    $options = [];
    foreach ($markerSets as $set) {
      $options[$set->id()] = $set->label();
    }

    return $options;
  }

  /**
   * Generate the map display configurations for mapkit maps.
   *
   * @param array $elements
   *   The form wrapper elements.
   * @param array $settings
   *   The default map configurations to use as a fallback.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state and build information.
   *
   * @return array
   *   Form elements for configuring a map display.
   */
  protected function buildMapConfigForm(array $elements, array $settings, FormStateInterface $form_state) {
    $mapManager = $this->getMapProviderManager();
    $providerOpts = $mapManager->getProviderLabels();
    $configWrapperId = 'mapkit-map-provider';

    // Ensure valid map provider configurations.
    reset($providerOpts);
    $providerId = $settings['provider'] ?? key($providerOpts);
    if (!isset($providerOpts[$providerId])) {
      $providerId = key($providerOpts);
    }

    // Configure map provider settings.
    $elements['provider'] = [
      '#type' => 'select',
      '#title' => $this->t('Map provider'),
      '#required' => TRUE,
      '#options' => $providerOpts,
      '#default_value' => $providerId,
    ];

    if (count($providerOpts) > 1) {
      $elements['provider']['#ajax'] = [
        'wrapper' => $configWrapperId,
        'callback' => static::class . '::mapConfigUpdateAjax',
      ];
    }
    else {
      // There is only one provider, always used it.
      $elements['provider']['#disabled'] = TRUE;
      $elements['provider']['#value'] = $providerId;
    }

    // Generate the wrapper for the map provider plugin configurations.
    // This needs to always be generated so AJAX has a wrapper to replace.
    $elements['config'] = [
      '#prefix' => '<div id="' . $configWrapperId . '-config">',
      '#suffix' => '</div>',
    ];

    $elements['markers'] = [
      '#prefix' => '<div id="' . $configWrapperId . '-markers">',
      '#suffix' => '</div>',
    ];

    // Delay the building of the configuration of the map provider plugin
    // until the form processing stage. This allows us to build this
    // configuration at any depth and have knowledge of the form element
    // "#parents" and "#array_parents".
    $elements['#process'][] = [$this, 'mapProviderConfigProcess'];
    $elements['#mapkit_provider'] = $settings;

    return $elements;
  }

  /**
   * Form element process callback to expand provider configuration elements.
   *
   * Build the configuration form after 'array_parents' and 'parents' element
   * properties have been created. This way the subform can find the correct
   * form state values to work with.
   *
   * @param array $elements
   *   The config form elements being processed.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param array $complete_form
   *   The complete form structure and elements.
   */
  public function mapProviderConfigProcess(array $elements, FormStateInterface $form_state, array &$complete_form) {
    $settings = $elements['#mapkit_provider'];

    if ($values = $form_state->getValue($elements['#parents'])) {
      $pluginId = $values['provider'];
      $config = $values['config'] ?? $settings['config'];
    }
    else {
      $pluginId = $settings['provider'];
      $config = $settings['config'];
    }

    try {
      $provider = $this->getMapProviderManager()
        ->createInstance($pluginId, $config ?? []);

      if ($provider instanceof PluginFormInterface) {
        $subformState = SubformState::createForSubform($elements['config'], $complete_form, $form_state);
        $elements['config'] += $provider->buildConfigurationForm($elements['config'], $subformState);
      }
      else {
        // When there are no provider configurations, the "config" should just
        // be an empty array.
        $elements['config']['#value'] = [];
      }

      // @todo Expand this to allow more multiple markers and options to match
      // each of the location data to the marker sets.
      $elements['markers'][0] = [
        '#type' => 'select',
        '#title' => $this->t('Marker set'),
        '#options' => $this->getMarkerSetOptions($pluginId),
        '#default_value' => $settings['markers'][0] ?? '',
      ];
    }
    catch (PluginException $e) {
      // Invalid provider plugin selected, we can neither create map
      // configurations or generate marker options. Catch this exception so
      // user can remedy this and still select a valid provider plugin option.
      $elements['config']['#value'] = $settings['config'] ?? [];
    }

    return $elements;
  }

  /**
   * Element validate callback for map provider configurations.
   *
   * Can be used to validate the map provider configurations in situations which
   * don't allow for normal validate and submit callbacks to set values. A field
   * formatter configuration an example where this would get used.
   *
   * @param array $elements
   *   The form elements that present the configuration options for the map
   *   provider plugin.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state values and build information.
   * @param array $form
   *   The complete form elements and structure.
   */
  public function mapConfigElementValidate(array $elements, FormStateInterface $form_state, array &$form) {
    $parents = $elements['#parents'];
    array_pop($parents);

    try {
      $values = $form_state->getValue($parents);
      /** @var \Drupal\mapkit\Plugin\MapProviderInterface $provider */
      $provider = $this->getMapProviderManager()
        ->createInstance($values['provider'], $values['config']);

      if ($provider instanceof PluginFormInterface) {
        $subformState = SubformState::createForSubform($elements, $form, $form_state);
        $provider->validateConfigurationForm($elements, $subformState);

        if (!$form_state->hasAnyErrors()) {
          $provider->submitConfigurationForm($elements, $subformState);
          $form_state->setValue($elements['#parents'], $provider->getConfiguration());
        }
      }
    }
    catch (PluginException $e) {
      // If form is rebuilding than this is okay, since the form values are not
      // being saved and likely to be in a transition during an AJAX transition
      // of the map provider being changed.
      $form_state->setValue($elements['#parents'], []);
    }
  }

  /**
   * Method to validate the map provider plugin configuration.
   *
   * @param array $elements
   *   The map full configuration elements (include provider selection and
   *   markers elements).
   * @param array $complete_form
   *   The complete submitted form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state, values and build information.
   */
  public function mapConfigValidate(array $elements, array &$complete_form, FormStateInterface $form_state) {
    $completeState = $form_state instanceof SubformStateInterface
      ? $form_state->getCompleteFormState() : $form_state;

    // Allow the map and marker plugin to process configuration form values.
    $values = $completeState->getValue($elements['#parents']);

    try {
      $provider = $this->getMapProviderManager()
        ->createInstance($values['provider'], $values['config']);

      if ($provider instanceof PluginFormInterface) {
        $subformState = SubformState::createForSubform($elements, $complete_form, $completeState);
        $provider->validateConfigurationForm($elements, $subformState);
      }
    }
    catch (PluginException $e) {
      $completeState->setError($elements['provider'], $this->t('Unable to load map provider selected, @plugin_name is not available.', [
        '@plugin_name' => $elements['provider']['#options'][$values['provider']],
      ]));
    }
  }

  /**
   * Method to submit the map provider plugin configurations.
   *
   * @param array $elements
   *   The map full configuration elements (include provider selection and
   *   markers elements).
   * @param array $complete_form
   *   The complete submitted form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state, values and build information.
   */
  public function mapConfigSubmit(array $elements, array &$complete_form, FormStateInterface $form_state) {
    $completeState = $form_state instanceof SubformStateInterface
      ? $form_state->getCompleteFormState() : $form_state;

    // Allow the map and marker plugin to process configuration form values.
    $values = $completeState->getValue($elements['#parents']);

    try {
      /** @var \Drupal\mapkit\Plugin\MapProviderInterface $provider */
      $provider = $this->getMapProviderManager()
        ->createInstance($values['provider'], $values['config']);

      if ($provider instanceof PluginFormInterface) {
        $subformState = SubformState::createForSubform($elements, $complete_form, $completeState);
        $provider->submitConfigurationForm($elements, $subformState);

        $valueParents = $elements['#parents'];
        $valueParents[] = 'config';
        $form_state->setValue($valueParents, $provider->getConfiguration());
      }
    }
    catch (PluginException $e) {
      $completeState->setError($elements['provider'], $this->t('Unable to load map provider selected, @plugin_name is not available.', [
        '@plugin_name' => $elements['provider']['#options'][$values['provider']],
      ]));
    }
  }

  /**
   * AJAX callback in response to the map provider being changed.
   *
   * Updates the map provider configs and the map markers options, since these
   * both rely on the map provider selected.
   *
   * @param array $form
   *   Reference to the complete form elements.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state, values and build info.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The AJAX commands to update the map provider configurations and the
   *   map marker options.
   */
  public static function mapConfigUpdateAjax(array &$form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    $parents = $trigger['#array_parents'];
    array_pop($parents);

    // Get the form elements for the entire map configurations.
    $elements = NestedArray::getValue($form, $parents);

    $baseId = $trigger['#ajax']['wrapper'];
    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand("#{$baseId}-config", $elements['config']));
    $response->addCommand(new ReplaceCommand("#{$baseId}-markers", $elements['markers']));

    return $response;
  }

}
