<?php

namespace Drupal\mapkit\Autocomplete;

use Drupal\toolshed\Strategy\StrategyManager;

/**
 * Strategy manager for location autocomplete providers.
 */
class AutocompleteManager extends StrategyManager {

  /**
   * Get a list of autocomplete strategy providers.
   *
   * The list is compatible for use as "#options" for select and checkbox form
   * elements.
   *
   * @return array
   *   List of available autocomplete handlers for the Mapkit autocomplete
   *   elements.
   */
  public function getAutocompleteOptions(): array {
    $options = [];
    foreach ($this->getDefinitions() as $id => $definition) {
      $options[$id] = $definition->getLabel();
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  protected function defaultStrategyClass(array $definition): string {
    return AutocompleteDefinition::class;
  }

  /**
   * {@inheritdoc}
   */
  protected function processDefinition(string $id, array $definition): AutocompleteDefinitionInterface {
    if (empty($definition['class'])) {
      $definition['class'] = $this->defaultStrategyClass($definition);
    }

    return new AutocompleteDefinition($id, $definition);
  }

  /**
   * {@inheritdoc}
   */
  public function getInstance(string $id, array $contexts = []): AutocompleteStrategyInterface {
    if (!isset($this->instances[$id])) {
      $definition = $this->getDefinition($id, $contexts);

      if (AutocompleteDefinition::class === $definition->getClass()) {
        $this->instances[$id] = $definition;
      }
      else {
        $instance = $this->getFactory()->create($id, $definition);
        $this->initInstance($id, $instance);
        $this->instances[$id] = $instance;
      }
    }

    return $this->instances[$id];

  }

}
