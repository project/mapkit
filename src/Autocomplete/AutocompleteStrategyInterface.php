<?php

namespace Drupal\mapkit\Autocomplete;

use Drupal\toolshed\Strategy\StrategyInterface;

/**
 * Interface for strategy implementation classes for the autocomplete provider.
 */
interface AutocompleteStrategyInterface extends StrategyInterface {

  /**
   * Get the autocomplete handler identifier to use for this provider.
   *
   * Used for the JS autocomplete element to specify which autocomplete
   * Javascript handler class to implement the autocomplete suggestions data
   * fetches.
   *
   * @return string
   *   The autocomplete handler identifier for this autocomplete instance.
   */
  public function getHandler(): string;

  /**
   * Get libraries to attach with this autocomplete provider.
   *
   * @return string[]|null
   *   The list of required libraries that should be attached when using this
   *   autocomplete provider.
   */
  public function getLibraries(): ?array;

}
