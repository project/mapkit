<?php

namespace Drupal\mapkit\Autocomplete;

use Drupal\toolshed\Strategy\Exception\InvalidDefinitionException;
use Drupal\toolshed\Strategy\StrategyDefinitionBase;
use Drupal\toolshed\Strategy\StrategyDefinitionInterface;

/**
 * The autocomplete strategy definition.
 *
 * Defines autocomplete providers that can be used with the Mapkit autocomplete
 * form elements (or other JS components).
 *
 * @see \Drupal\mapkit\Element\MapkitAutocomplete
 */
class AutocompleteDefinition extends StrategyDefinitionBase implements AutocompleteDefinitionInterface, AutocompleteStrategyInterface {

  /**
   * The Javascript handler ID to identify as the autocomplete handler.
   *
   * @var string
   */
  protected string $handler;

  /**
   * Libraries that should be loaded for this autocomplete provider.
   *
   * @var string[]
   */
  protected array $libraries;

  /**
   * Creates a new instance of the AutocompleteDefinition class.
   *
   * @param string $id
   *   The autocomplete strategy identifier.
   * @param array $definition
   *   The strategy definition values from the autocomplete manager discovery.
   */
  public function __construct(string $id, array $definition) {
    parent::__construct($id, $definition);

    if (!isset($definition['handler'])) {
      throw new InvalidDefinitionException('Mapkit Autocomplete strategy is required to have a "handler" identifier for "' . $id . '".');
    }

    $this->handler = $definition['handler'];
    $this->libraries = $definition['libraries'] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getHandler(): string {
    return $this->handler;
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(): array {
    return $this->libraries;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinition(): StrategyDefinitionInterface {
    return $this;
  }

}
