<?php

namespace Drupal\mapkit\Entity;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\mapkit\Plugin\MarkerPluginInterface;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;

/**
 * Configurations for pre-configured map markers.
 *
 * @ConfigEntityType(
 *   id = "mapkit_marker_set",
 *   label = @Translation("Marker set"),
 *   label_collection = @Translation("Marker sets"),
 *   handlers = {
 *     "list_builder" = "\Drupal\mapkit\Entity\Controller\MarkerSetListBuilder",
 *     "storage" = "\Drupal\mapkit\Entity\Storage\MarkerSetStorage",
 *     "form" = {
 *       "default" = "\Drupal\mapkit\Entity\Form\MarkerSetEditForm",
 *       "delete" = "\Drupal\toolshed\Entity\Form\EntityDeleteConfirm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "marker_set",
 *   admin_permission = "administer mapkit markers",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "status" = "status",
 *   },
 *   links = {
 *     "collection" = "/admin/config/services/mapkit/marker",
 *     "add-form" = "/admin/config/services/mapkit/marker/add",
 *     "edit-form" = "/admin/config/services/mapkit/marker/{mapkit_marker_set}/edit",
 *     "delete-form" = "/admin/config/services/mapkit/marker/{mapkit_marker_set}/delete",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "map_type",
 *     "status",
 *     "plugin",
 *     "config",
 *   },
 * )
 */
class MapkitMarkerSet extends ConfigEntityBase {

  /**
   * The marker set identifier.
   *
   * @var string
   */
  protected $id;

  /**
   * The administrative name for the marker set.
   *
   * @var \Drupal\Core\StringTranslation\TranslatableMarkup|string
   */
  protected $label;

  /**
   * Machine name of the type of maps this marker works with.
   *
   * @var string
   */
  protected $map_type;

  /**
   * The marker set plugin identifier.
   *
   * @var string
   */
  protected $plugin;

  /**
   * The marker set plugin configuration.
   *
   * @var array
   */
  protected $config;

  /**
   * A marker set plugin instance, or boolean FALSE, if unable to load.
   *
   * @var \Drupal\mapkit\Plugin\MarkerPluginInterface|false
   */
  protected $loadedPlugin;

  /**
   * Get the plugin manager for creating new plugin instances.
   *
   * @return \Drupal\association\Plugin\BehaviorPluginManagerInterface
   *   Plugin manager for creating and managing association behavior plugins.
   */
  protected static function getMarkerManager() {
    return \Drupal::service('plugin.manager.mapkit.marker');
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $values, $entity_type) {
    // Fallback to the default marker plugin. All map providers should implement
    // a default map marker so this should work regardless of the map type.
    $values += [
      'plugin' => 'default',
      'config' => [],
    ];
    parent::__construct($values, $entity_type);
  }

  /**
   * {@inheritdoc}
   */
  public function set($property_name, $value) {
    parent::set($property_name, $value);

    // Ensure that if the marker set plugin configurations are updated, the
    // loaded plugin will get rebuilt when next requested.
    if ($property_name === 'plugin' || $property_name === 'config') {
      unset($this->loadedPlugin);
    }

    return $this;
  }

  /**
   * Get the map provider type this marker is configured for.
   *
   * @return string|null
   *   The map type this marker is configured for.
   */
  public function getMapType(): ?string {
    return $this->map_type;
  }

  /**
   * Gets the plugin ID of the marker plugin used by this marker set config.
   *
   * @return string
   *   The plugin ID for the marker plugin.
   */
  public function getPluginId() {
    return $this->plugin;
  }

  /**
   * Get the marker set plugin if able to load.
   *
   * @param bool $force_rebuild
   *   Force the rebuilding of the marker set plugin instance.
   *
   * @return \Drupal\mapkit\Plugin\MarkerPluginInterface|null
   *   The marker set plugin for this map marker configuration.
   */
  public function getPlugin($force_rebuild = FALSE): ?MarkerPluginInterface {
    if (!isset($this->loadedPlugin) || $force_rebuild) {
      try {
        // Fallback to the default plugin if one has not been specified.
        $plugin = $this->plugin ?? 'default';
        $this->loadedPlugin = static::getMarkerManager()
          ->createInstance($plugin, $this->config);
      }
      catch (ServiceNotFoundException | PluginNotFoundException $e) {
        $this->loadedPlugin = FALSE;

        if (!$this->isNew()) {
          watchdog_exception('mapkit.marker', $e);
        }
      }
    }

    return $this->loadedPlugin ?: NULL;
  }

}
