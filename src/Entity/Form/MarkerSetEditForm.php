<?php

namespace Drupal\mapkit\Entity\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\mapkit\Plugin\MapProviderManagerInterface;
use Drupal\mapkit\Plugin\MarkerPluginManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Entity form for creating and editing MarkerSet config entities.
 */
class MarkerSetEditForm extends EntityForm {

  /**
   * Plugin manager for map provider plugins.
   *
   * @var \Drupal\mapkit\Plugin\MapProviderManagerInterface
   */
  protected $mapProviderManager;

  /**
   * Plugin manager for map marker plugins.
   *
   * @var \Drupal\mapkit\Plugin\MarkerPluginManagerInterface
   */
  protected $markerPluginManager;

  /**
   * Create a new instance of the AssociationTypeForm object.
   *
   * @param \Drupal\mapkit\Plugin\MapProviderManagerInterface $map_plugin_manager
   *   Plugin manager for map provider plugins.
   * @param \Drupal\mapkit\Plugin\MarkerPluginManagerInterface $marker_plugin_manager
   *   Plugin manager for map marker plugins.
   */
  public function __construct(MapProviderManagerInterface $map_plugin_manager, MarkerPluginManagerInterface $marker_plugin_manager) {
    $this->mapProviderManager = $map_plugin_manager;
    $this->markerPluginManager = $marker_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.mapkit.map_provider'),
      $container->get('plugin.manager.mapkit.marker')
    );
  }

  /**
   * Check to see if an ID is already in use for association type bundle.
   *
   * @return bool
   *   The boolean to indicate if this ID is already in use. TRUE if it already
   *   exists, and FALSE otherwise.
   */
  public function exists($id) {
    // Reserve the ID of "_custom" and "_inline" for custom instances that get
    // loaded from the local configurations or settings.
    if ('_custom' === $id || '_inline' === $id) {
      return TRUE;
    }

    $entityType = $this
      ->getEntity()
      ->getEntityType();

    $entityIds = $this->entityTypeManager
      ->getStorage($entityType->id())
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition($entityType->getKey('id'), $id)
      ->execute();

    return (bool) $entityIds;
  }

  /**
   * {@inheritdoc}
   */
  protected function copyFormValuesToEntity(EntityInterface $entity, array $form, FormStateInterface $form_state) {
    /** @var \Drupal\Core\Config\Entity\ConfigEntityInterface $entity */
    $values = $form_state->getValues();

    parent::copyFormValuesToEntity($entity, $form, $form_state);

    // The form has the plugin configurations in a slightly different structure
    // than the way the entity has these values. Move them to their correct
    // location for the marker set entity to use them.
    if (!empty($values['plugin']['id'])) {
      $entity->set('plugin', $values['plugin']['id']);
      $entity->set('config', $values['plugin']['config'] ?? []);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\mapkit\Entity\MapkitMarkerSet $entity */
    $entity = $this->entity;
    $form = parent::form($form, $form_state);

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $entity->label(),
      '#description' => $this->t('Label for the marker set.'),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $entity->id(),
      '#machine_name' => [
        'source' => ['label'],
        'exists' => [$this, 'exists'],
      ],
      '#disabled' => !$entity->isNew(),
    ];

    $mapOpts = $this->mapProviderManager->getProviderLabels();

    if (count($mapOpts) > 1) {
      // Determine what the current map settings are.
      $mapType = $entity->getMapType();
      $mapType = $mapType && isset($mapOpts[$mapType]) ? $mapType : NULL;

      $form['map_type'] = [
        '#type' => 'select',
        '#title' => $this->t('Map type'),
        '#required' => TRUE,
        '#options' => $this->mapProviderManager->getProviderLabels(),
        '#default_value' => $mapType,
        '#ajax' => [
          'wrapper' => 'mapkit-marker-plugin',
          'callback' => static::class . '::pluginUpdateAjax',
        ],
      ];

      // Should not be changed after created.
      if (!$entity->isNew() || empty($mapType)) {
        $form['map_type']['#disabled'] = TRUE;
      }
    }
    else {
      reset($mapOpts);
      $mapType = key($mapOpts);

      // Only one type of map is available, force it.
      $form['map_type'] = [
        '#type' => 'value',
        '#value' => $mapType,
      ];
    }

    // Wrapper for the marker plugin configurations.
    $form['plugin'] = [
      '#prefix' => '<div id="mapkit-marker-plugin">',
      '#suffix' => '</div>',
      '#type' => 'fieldset',
      '#tree' => TRUE,
    ];

    if ($mapType) {
      $form['plugin']['id'] = [
        '#type' => 'select',
        '#title' => $this->t('Marker plugin'),
        '#required' => TRUE,
        '#options' => $this->markerPluginManager->getMarkerPluginLabels($mapType),
        '#ajax' => [
          'wrapper' => 'mapkit-marker-plugin-config',
          'callback' => static::class . '::pluginConfigurationAjax',
        ],
      ];

      if ($entity->getPluginId() && ($markerPlugin = $entity->getPlugin(TRUE))) {
        $form['plugin']['id']['#default_value'] = $markerPlugin->getPluginId();

        if ($markerPlugin instanceof PluginFormInterface) {
          // Ensure that subform has the parents and array_parents it needs to
          // determine form element context.
          $form['plugin']['config'] = [
            '#parents' => ['plugin', 'config'],
            '#array_parents' => ['plugin', 'config'],
          ];

          // Create the marker plugin configuration form, using the internal
          // value state.
          $pluginForm = &$form['plugin']['config'];
          $subformState = SubformState::createForSubform($pluginForm, $form, $form_state);
          $pluginForm = $markerPlugin->buildConfigurationForm($pluginForm, $subformState);
        }
        else {
          $form['plugin']['config'] = [
            '#type' => 'value',
            '#value' => [],
          ];
        }
      }
    }

    // Ensure AJAX wrapper is always available.
    $form['plugin']['config']['#prefix'] = '<div id="mapkit-marker-plugin-config">';
    $form['plugin']['config']['#suffix'] = '</div>';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\mapkit\Entity\MapkitMarkerSet $entity */
    $entity = $this->getEntity();

    if ($entity->getPluginId() && ($markerPlugin = $entity->getPlugin(TRUE))) {
      if ($markerPlugin instanceof PluginFormInterface) {
        $subformState = SubformState::createForSubform($form['plugin']['config'], $form, $form_state);
        $markerPlugin->validateConfigurationForm($form['plugin']['config'], $subformState);
      }
    }
    else {
      $form_state->setError($form['plugin'], $this->t('Marker plugin settings are required.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\mapkit\Entity\MapkitMarkerSet $entity */
    $entity = $this->entity;

    try {
      $markerPlugin = $entity->getPlugin(TRUE);

      // If marker plugin has a configuration, allow it to process the form
      // submitted values and save them to the marker plugin configurations.
      if ($markerPlugin instanceof PluginFormInterface) {
        $subformState = SubformState::createForSubform($form['plugin']['config'], $form, $form_state);
        $markerPlugin->submitConfigurationForm($form['plugin']['config'], $subformState);

        $entity->set('config', $markerPlugin->getConfiguration());
      }

      // Save the bundle entity changes.
      $status = $entity->save();
      $form_state->setRedirectUrl($entity->toUrl('collection'));

      $msgParams = ['%label' => $entity->label()];
      $msg = SAVED_NEW === $status
        ? $this->t('Created the %label marker set.', $msgParams)
        : $this->t('Saved the %label marker set changes.', $msgParams);

      $this->messenger()->addStatus($msg);
      return $status;
    }
    catch (\Exception $e) {
      $this->messenger()->addError($this->t('Unable to save the marker set changes.'));
    }

    return 0;
  }

  /**
   * AJAX callback to update form when the map type changes.
   *
   * @param array $form
   *   Complete form structure and element definitions.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Current state, build information and user input for the form.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse|array
   *   A set of AJAX commands or a renderable array with plugin form elements.
   */
  public static function pluginUpdateAjax(array $form, FormStateInterface $form_state) {
    return $form['plugin'];
  }

  /**
   * AJAX callback for when the marker plugin is changed.
   *
   * @param array $form
   *   Complete form structure and element definitions.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Current state, build information and user input for the form.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse|array
   *   Marker plugin configuration elements to update.
   */
  public static function pluginConfigurationAjax(array $form, FormStateInterface $form_state) {
    return $form['plugin']['config'];
  }

}
