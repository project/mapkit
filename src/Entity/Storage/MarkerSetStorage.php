<?php

namespace Drupal\mapkit\Entity\Storage;

use Drupal\Core\Config\Entity\ConfigEntityStorage;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityMalformedException;

/**
 * Entity storage handler for Mapkit marker sets config entities.
 */
class MarkerSetStorage extends ConfigEntityStorage {

  /**
   * {@inheritdoc}
   */
  public function save(EntityInterface $entity) {
    $id = $entity->id();

    if ('_custom' === $id || '_inline' === $id) {
      throw new EntityMalformedException('Marker set cannot be saved using the reserved IDs of "_custom" or "_inline".');
    }

    return parent::save($entity);
  }

}
