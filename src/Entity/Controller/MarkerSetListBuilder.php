<?php

namespace Drupal\mapkit\Entity\Controller;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\mapkit\Plugin\MapProviderManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class to build a listing of defined marker sets.
 */
class MarkerSetListBuilder extends ConfigEntityListBuilder {

  /**
   * The mapkit map provider manager.
   *
   * @var \Drupal\mapkit\Plugin\MapProviderManagerInterface
   */
  protected $mapProviderManager;

  /**
   * Create a new instance of the MarkerSetListBuilder class.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type instance.
   * @param \Drupal\Core\Entity\EntityStorageInterface $entity_storage
   *   The entity storage for this entity type.
   * @param \Drupal\mapkit\Plugin\MapProviderManagerInterface $map_provider_manager
   *   The map provider manager.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $entity_storage, MapProviderManagerInterface $map_provider_manager) {
    parent::__construct($entity_type, $entity_storage);

    $this->mapProviderManager = $map_provider_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('plugin.manager.mapkit.map_provider')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Marker set');
    $header['map_type'] = $this->t('Map type');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();

    /** @var \Drupal\Core\Config\Entity\ConfigEntityInterface $entity */
    if ($mapType = $entity->get('map_type')) {
      $def = $this->mapProviderManager->getDefinition($mapType, FALSE);
      $row['map_type'] = $def['label'] ?: $this->t('Missing map plugin');
    }

    return $row + parent::buildRow($entity);
  }

}
