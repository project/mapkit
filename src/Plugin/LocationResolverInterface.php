<?php

namespace Drupal\mapkit\Plugin;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Cache\CacheableDependencyInterface;

/**
 * Plugin interface for fetching location information from the context.
 */
interface LocationResolverInterface extends PluginInspectionInterface, ConfigurableInterface, CacheableDependencyInterface {

  /**
   * Get the lat/lng geolocation value from the current context.
   *
   * @return array|null
   *   Either return an array with lat/lng data or NULL if no geolocation is
   *   available.
   */
  public function getLatLng(): ?array;

  /**
   * Get a text representation of the the location.
   *
   * The location text is intended as a text display value and is normally
   * the address if it is possible to get one.
   *
   * @return string|null
   *   A text formatted version of the location display.
   */
  public function getLocationText(): ?string;

}
