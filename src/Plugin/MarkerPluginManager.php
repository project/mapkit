<?php

namespace Drupal\mapkit\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides a plugin manager for managing marker set plugins.
 *
 * @see \Drupal\mapkit\Annotation\MapkitMarker
 * @see \Drupal\mapkit\Plugin\MarkerPluginInterface
 * @see plugin_api
 */
class MarkerPluginManager extends DefaultPluginManager implements MarkerPluginManagerInterface {

  /**
   * Constructs a marker set plugin manager instance.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/Mapkit/Marker',
      $namespaces,
      $module_handler,
      'Drupal\mapkit\Plugin\MarkerPluginInterface',
      'Drupal\mapkit\Annotation\MapkitMarker'
    );

    $this->setCacheBackend($cache_backend, 'mapkit_marker');
  }

  /**
   * {@inheritdoc}
   */
  public function getMarkerPluginLabels($map_type = NULL) {
    $definitions = $this->getDefinitions();

    $markers = [];
    foreach ($definitions as $pluginId => $def) {
      if (empty($map_type) || empty($def['map_types']) || in_array($map_type, $def['map_types'])) {
        $markers[$pluginId] = $def['label'];
      }
    }

    return $markers;
  }

}
