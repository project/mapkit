<?php

namespace Drupal\mapkit\Plugin;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Interface for location input plugins.
 *
 * Location input plugins generate and configure form input elements for
 * entering geolocation values. These are fields that resolve to lat/lng value
 * to use with proximity searches.
 */
interface LocationInputInterface extends PluginInspectionInterface, ConfigurableInterface {

  /**
   * Is the exposed input value usable for processing?
   *
   * @param mixed $input
   *   The input form the location input elements to check.
   *
   * @return bool
   *   Is this input valid for processing.
   */
  public function acceptInput($input): bool;

  /**
   * Process form submitted values from the location input elements.
   *
   * This should resolve values into lat/lng values and possibly text display
   * values.
   *
   * @param mixed $input
   *   The input values from the form submission to process into a geolocation.
   *
   * @return array|null
   *   The lat/lng values if the input can be resolved into a location. NULL
   *   if the input does not resolve to a valid location.
   */
  public function processInput($input): ?array;

  /**
   * Create form input elements for entering the geolocation.
   *
   * @param mixed $values
   *   The input default values.
   * @param array $elements
   *   A reference to the form elements to place the input form elements into.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state and build info.
   * @param \Drupal\mapkit\Plugin\LocationResolverInterface|null $resolver
   *   A location resolver to generate a default location value.
   */
  public function buildInputElements($values, array &$elements, FormStateInterface $form_state, LocationResolverInterface $resolver = NULL): void;

}
