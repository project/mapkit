<?php

namespace Drupal\mapkit\Plugin;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Interface for the mapkit map markers plugin manager.
 */
interface MarkerPluginManagerInterface extends PluginManagerInterface {

  /**
   * Get the labels of the marker set compatible with a specified map type.
   *
   * @param string|null $map_type
   *   A map plugin type which to filter the compatible marker sets. If omitted
   *   all marker sets will be included.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup[]|string[]
   *   An array of marker set names that is compatible with the specified
   *   map type. Each array item is keyed by the MarkerSet plugin ID.
   */
  public function getMarkerPluginLabels($map_type = NULL);

}
