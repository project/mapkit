<?php

namespace Drupal\mapkit\Plugin;

use Drupal\Component\Plugin\FallbackPluginManagerInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides a plugin manager for managing location input.
 *
 * Location input plugin manager.
 *
 * @see \Drupal\mapkit\Annotation\MapkitLocationInput
 * @see \Drupal\mapkit\Plugin\LocationInputInterface
 * @see plugin_api
 */
class LocationInputPluginManager extends DefaultPluginManager implements FallbackPluginManagerInterface {

  /**
   * Constructs a location input plugin manager instance.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/Mapkit/LocationInput',
      $namespaces,
      $module_handler,
      'Drupal\mapkit\Plugin\LocationInputInterface',
      'Drupal\mapkit\Annotation\MapkitLocationInput'
    );

    $this->setCacheBackend($cache_backend, 'mapkit_location_input');
  }

  /**
   * Get lists of available location input options.
   *
   * The returned array is suitable for use as the options with "select" and
   * "checkboxes" form elements.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup[]|string[]
   *   Get lists of available location input options.
   */
  public function getInputOptions(): array {
    $options = [];
    foreach ($this->getDefinitions() as $def) {
      $options[$def['id']] = $def['label'];
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function getFallbackPluginId($plugin_id, array $configuration = []) {
    return 'textfield';
  }

}
