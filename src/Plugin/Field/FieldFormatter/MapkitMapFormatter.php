<?php

namespace Drupal\mapkit\Plugin\Field\FieldFormatter;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\mapkit\GeoParser\GeoParserManagerInterface;
use Drupal\mapkit\MapConfigureFormTrait;
use Drupal\mapkit\Plugin\MapProviderManagerInterface;
use Drupal\mapkit\Plugin\MarkerPluginManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A field formatter for rendering Mapkit maps for lat/lng fields.
 *
 * The formatter plugin definition "field_types" is set in
 * mapkit_field_formatter_info_alter() method so it can dynamically be set to
 * support any field types we have a geo_parser plugin for.
 *
 * @FieldFormatter(
 *   id = "mapkit_map",
 *   label = @Translation("Mapkit map"),
 *   field_types = { },
 * )
 */
class MapkitMapFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  use LoggerChannelTrait;
  use MapConfigureFormTrait;

  /**
   * The geo-parser plugin manager.
   *
   * @var \Drupal\mapkit\GeoParser\GeoParserManagerInterface
   */
  protected $geoParserManager;

  /**
   * Create a new instance of the MapkitMapFormatter plugin.
   *
   * @param string $plugin_id
   *   The plugin identifier.
   * @param mixed $plugin_definition
   *   The plugin definition from the plugin manager.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   * @param array $settings
   *   The field formatter settings.
   * @param string $label
   *   The field label for the formatter.
   * @param string $view_mode
   *   The entity view mode being rendered.
   * @param array $third_party_settings
   *   The field formatter third party settings.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\mapkit\GeoParser\GeoParserManagerInterface $geo_parser_manager
   *   The geo-parers plugin manager.
   * @param \Drupal\mapkit\Plugin\MapProviderManagerInterface $map_provider_manager
   *   The map provider plugin manager.
   * @param \Drupal\mapkit\Plugin\MarkerPluginManagerInterface $marker_plugin_manager
   *   The marker plugin manager.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, EntityTypeManagerInterface $entity_type_manager, GeoParserManagerInterface $geo_parser_manager, MapProviderManagerInterface $map_provider_manager, MarkerPluginManagerInterface $marker_plugin_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);

    $this->entityTypeManager = $entity_type_manager;
    $this->geoParserManager = $geo_parser_manager;
    $this->mapProviderManager = $map_provider_manager;
    $this->markerManager = $marker_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager'),
      $container->get('strategy.manager.mapkit.geo_parser'),
      $container->get('plugin.manager.mapkit.map'),
      $container->get('plugin.manager.mapkit.marker')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'height' => '500px',
      'width' => '100%',
      'map' => [
        'provider' => 'gmap',
        'config' => [],
        'markers' => [],
      ],
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  protected function getHtmlDomId() {
    $fieldName = $this->fieldDefinition->getName();
    return strtr($fieldName, '_', '-') . '-map-display';
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $settings = $this->getSettings();

    $form['width'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Map width'),
      '#required' => TRUE,
      '#default_value' => $settings['width'],
      '#pattern' => '\d+\s*(%|px|em|rem|vw)',
      '#description' => $this->t('Width needs to be a valid HTML/CSS width dimension (%, px, em, rem, vw units are accepted).'),
    ];

    $form['height'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Map height'),
      '#required' => TRUE,
      '#default_value' => $settings['height'],
      '#pattern' => '\d+\s*(%|px|em|rem|vh)',
      '#description' => $this->t('Height needs to be a valid HTML/CSS height dimension (%, px, em, rem and vh units are accepted).'),
    ];

    $form['map'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Map display settings'),
      '#tree' => TRUE,
    ];

    $form['map'] = $this->buildMapConfigForm($form['map'], $settings['map'], $form_state);

    // Because the field formatter does not get to run normal form submit
    // and form validate. We combine these form callback in an element validate
    // callback which will clean the config values for saving.
    $form['map']['config']['#element_validate'][] = [
      $this,
      'mapConfigElementValidate',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $settings = $this->getSettings();
    $elements = [];

    try {
      $fieldType = $this->fieldDefinition->getType();
      $geoParser = $this->geoParserManager->getFieldParser($fieldType);

      // Load the map provider plugin to build the map configurations.
      $provider = $this->getMapProviderManager()
        ->createInstance($settings['map']['provider'], $settings['map']['config']);

      // Load and add the map marker configurations.
      $markers = [];
      /** @var \Drupal\mapkit\Entity\MapkitMarkerSet[] $markersets */
      $markersets = $this->getEntityTypeManager()
        ->getStorage('mapkit_marker_set')
        ->loadMultiple();

      foreach ($settings['map']['markers'] as $markerId) {
        if (!empty($markersets[$markerId])) {
          $markers[] = $markersets[$markerId]->getPlugin();
        }
      }

      $elements[0] = [
        '#theme' => 'mapkit_map',
        '#width' => $settings['width'],
        '#height' => $settings['height'],
        '#map_id' => $this->getHtmlDomId(),
        '#map_provider' => $provider,
        '#markers' => $markers,
        '#data' => $geoParser->parseField($items),
      ];
    }
    catch (PluginException $e) {
      $this->getLogger('mapkit')->error('Failed to load mapkit resource for @field_name with: @message', [
        '@field_name' => $this->fieldDefinition->getName(),
        '@message' => $e->getMessage(),
      ]);
    }

    return $elements;
  }

}
