<?php

namespace Drupal\mapkit\Plugin\search_api\data_type;

use Drupal\search_api\DataType\DataTypePluginBase;

/**
 * A Search API data type for representing a point (lat/lng) location.
 *
 * This data type needs to be supported by the Search API backend and is
 * in the "lat,lng" format (decimal values separated with a comma). Search API
 * Solr provides this type configuration in the Solr schema files.
 *
 * @SearchApiDataType(
 *   id = "location",
 *   label = @Translation("Latitude/Longitude"),
 *   description = @Translation("A location (lat/lng) data type."),
 * )
 */
class LocationDataType extends DataTypePluginBase {

  /**
   * {@inheritdoc}
   */
  public function getValue($value) {
    if ($geometry = \geoPHP::load($value)) {
      $cent = $geometry->getCentroid();
      return $cent->getY() . ',' . $cent->getX();
    }
    else {
      // @todo verify string is already in a lat/lon format?
      return $value;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getFallbackType() {
    // Ensure Search API doesn't attempt to use a fallback type. Search API
    // will typically fallback to using a string format which is incompatible
    // for filtering proximity and computing distances.
    return NULL;
  }

}
