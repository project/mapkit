<?php

namespace Drupal\mapkit\Plugin;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Interface for defining map plugin handlers.
 */
interface MapProviderInterface extends PluginInspectionInterface, ConfigurableInterface {

  /**
   * Provide the libraries that provides the default map marker.
   *
   * @return string[]
   *   List of libraries that implemented the default map marker.
   */
  public function getDefaultMarkerLibraries(): array;

  /**
   * Get the libraries that need to be included for map rendering functionality.
   *
   * @return string[]
   *   List of libraries that are required to produce the map display.
   */
  public function getLibraries(): array;

  /**
   * The JavaScript settings for the map settings.
   *
   * @return array
   *   The Google maps display configurations.
   */
  public function getJsSettings(): array;

}
