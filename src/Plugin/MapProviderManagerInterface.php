<?php

namespace Drupal\mapkit\Plugin;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Interface for the mapkit map providers plugin manager.
 */
interface MapProviderManagerInterface extends PluginManagerInterface {

  /**
   * Get a list of available map provider plugins keyed by plugin ID.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup[]|string[]
   *   The list of map provider plugin labels keyed by the plugin ID.
   */
  public function getProviderLabels(): array;

}
