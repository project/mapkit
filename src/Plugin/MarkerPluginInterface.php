<?php

namespace Drupal\mapkit\Plugin;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Interface for defining sets of markers that can be used with maps.
 */
interface MarkerPluginInterface extends PluginInspectionInterface, ConfigurableInterface {

  /**
   * Get the libraries that need to be included for map marker functionality.
   *
   * @return string[]
   *   List of libraries that are required to implement this marker set.
   */
  public function getLibraries(MapProviderInterface $map);

  /**
   * The JavaScript settings for the marker settings.
   *
   * @return array
   *   Map marker settings to pass into drupalSettings for the marker display.
   */
  public function getJsSettings(MapProviderInterface $map);

  /**
   * Set the current start index for where to start marker label indexing.
   *
   * This helps with pagination, set where these set of markers will start
   * counting up from. Just be aware that large numbers aren't likely to fit
   * well in the space provided by markers.
   *
   * @return int
   *   The current marker label start index.
   */
  public function getStartIndex();

  /**
   * Set the starting index for marker labels to start counting from.
   *
   * @param int $start
   *   The index to start counting up from.
   *
   * @see \Drupal\mapkit\Plugin\MarkerSetInterface::getStartIndex()
   */
  public function setStartIndex($start);

}
