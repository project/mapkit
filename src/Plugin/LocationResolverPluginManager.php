<?php

namespace Drupal\mapkit\Plugin;

use Drupal\Component\Plugin\FallbackPluginManagerInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides a plugin manager for managing location resolvers.
 *
 * Location resolvers are used to extract location (lat/lng) information from
 * the current context. This can commonly be used to populate default location
 * data based on site defaults, current user, etc.
 *
 * @see \Drupal\mapkit\Annotation\MapkitLocationResolver
 * @see \Drupal\mapkit\Plugin\LocationResolverInterface
 * @see plugin_api
 */
class LocationResolverPluginManager extends DefaultPluginManager implements FallbackPluginManagerInterface {

  /**
   * Constructs a location resolver plugin manager instance.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/Mapkit/LocationResolver',
      $namespaces,
      $module_handler,
      'Drupal\mapkit\Plugin\LocationResolverInterface',
      'Drupal\mapkit\Annotation\MapkitLocationResolver'
    );

    $this->setCacheBackend($cache_backend, 'mapkit_location_resolver');
  }

  /**
   * Get lists of available location resolver options.
   *
   * The returned array is suitable for use as the options with "select" and
   * "checkboxes" form elements.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup[]|string[]
   *   Get lists of available location resolver options.
   */
  public function getResolverOptions(): array {
    $options = [];
    foreach ($this->getDefinitions() as $def) {
      $options[$def['id']] = $def['label'];
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function getFallbackPluginId($plugin_id, array $configuration = []) {
    return 'null_resolver';
  }

}
