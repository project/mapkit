<?php

namespace Drupal\mapkit\Plugin\Mapkit\Marker;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\mapkit\Plugin\MapProviderInterface;
use Drupal\mapkit\Plugin\MarkerPluginInterface;

/**
 * The default Mapkit marker set.
 *
 * All map providers should at a minimum provide a default map marker. This
 * allows at least a single marker to be supported, and a common marker set
 * configuration.
 *
 * @MapkitMarker(
 *   id = "default",
 *   label = @Translation("Standard Marker"),
 *   map_types = {
 *   }
 * )
 */
class DefaultMarker extends PluginBase implements MarkerPluginInterface, PluginFormInterface {

  /**
   * The current starting index to start the markers at.
   *
   * @var int
   */
  protected $startIndex = 1;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'offset' => 0,
      'label' => [
        'style' => 'numbers',
        'color' => '000',
        'fontSize' => 14,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration + $this->defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(MapProviderInterface $provider) {
    return $provider->getDefaultMarkerLibraries();
  }

  /**
   * {@inheritdoc}
   */
  public function getJsSettings(MapProviderInterface $map) {
    $mapType = $map->getPluginId();
    $config = $this->getConfiguration();

    $config['label']['color'] = '#' . $config['label']['color'];
    $config['label']['fontSize'] = $config['label']['fontSize'] . 'px';

    // These settings are needed to determine the marker callback and
    // starting index for this marker set.
    $config['type'] = strtr($mapType, '_', '-') . 'Marker';
    $config['start'] = $this->getStartIndex();

    return $config;
  }

  /**
   * Get available marker styles.
   *
   * @return string[]
   *   The friendly name of all the available marker styles.
   */
  protected function getMarkerStyles() {
    return [
      'none' => $this->t('None'),
      'numbers' => $this->t('Numbers'),
      'letters' => $this->t('Letters'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getStartIndex() {
    return $this->startIndex;
  }

  /**
   * {@inheritdoc}
   */
  public function setStartIndex($start_at) {
    $this->startIndex = intval($start_at);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $config = $this->getConfiguration() + $this->defaultConfiguration();

    $form['offset'] = [
      '#type' => 'number',
      '#title' => $this->t('Marker label index offset'),
      '#default_value' => $config['offset'],
    ];

    $form['label'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Label settings'),
      '#tree' => TRUE,

      'style' => [
        '#type' => 'select',
        '#title' => $this->t('Label style'),
        '#options' => $this->getMarkerStyles(),
        '#default_value' => $config['label']['style'],
      ],
      'color' => [
        '#type' => 'textfield',
        '#title' => $this->t('Text color'),
        '#size' => 6,
        '#field_prefix' => '#',
        '#pattern' => '[a-fA-F0-9]{3}|[a-fA-F0-9]{6}',
        '#default_value' => trim($config['label']['color'], '# '),
      ],
      'fontSize' => [
        '#type' => 'number',
        '#title' => $this->t('Text size'),
        '#size' => 2,
        '#field_suffix' => 'px',
        '#min' => 4,
        '#max' => 40,
        '#default_value' => $config['label']['fontSize'],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    // No validation required.
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $defaults = $this->defaultConfiguration();
    $labelSettings = $form_state->getValue('label') + $defaults['label'];

    $this->configuration = [];
    $this->configuration['offset'] = intval($form_state->getValue('offset'));

    // Ensure clean label settings.
    $this->configuration['label']['style'] = $labelSettings['style'];
    $this->configuration['label']['color'] = trim($labelSettings['color'], ' #');
    $this->configuration['label']['fontSize'] = intval($labelSettings['fontSize']);
  }

}
