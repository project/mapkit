<?php

namespace Drupal\mapkit\Plugin\Mapkit\LocationInput;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\mapkit\Autocomplete\AutocompleteManager;
use Drupal\mapkit\Plugin\LocationResolverInterface;
use Drupal\mapkit\Utility\MapkitHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Input for entering lat/lng using the Mapkit autocomplete form element.
 *
 * @MapkitLocationInput(
 *   id = "autocomplete",
 *   label = @Translation("Location autocomplete"),
 * )
 */
class AutocompleteLocationInput extends TextfieldLocationInput implements ContainerFactoryPluginInterface {

  /**
   * The autocomplete strategy manager service.
   *
   * @var \Drupal\mapkit\Autocomplete\AutocompleteManager
   */
  protected AutocompleteManager $acManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);

    $acManager = $container->get('strategy.manager.mapkit.autocomplete');
    $instance->setAutocompleteManager($acManager);

    return $instance;
  }

  /**
   * Set the autocomplete strategy manager service.
   *
   * @param \Drupal\mapkit\Autocomplete\AutocompleteManager $autocomplete_manager
   *   The autocomplete strategy manager service.
   */
  public function setAutocompleteManager(AutocompleteManager $autocomplete_manager): void {
    $this->acManager = $autocomplete_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $defaults = parent::defaultConfiguration();
    $defaults += [
      'handler' => NULL,
      'match_type' => 'address',
      'pattern' => NULL,
      'bound_type' => 'bias',
      'bounds' => [],
    ];

    return $defaults;
  }

  /**
   * {@inheritdoc}
   */
  public function processInput($input): ?array {
    $value = $input['loc'];

    if (empty($value)) {
      return NULL;
    }

    // Check if there is a valid lat/long value, if not check if we are able
    // to fallback on geocoding the text provided value.
    $config = $this->getConfiguration();
    $latlng = MapkitHelper::parseLatLng($value['latlng']) ?: $this->geocodeInput($value['text'], $config['geocoder']);

    // A valid lat/lng value was available.
    if ($latlng) {
      $radius = $input['radius'] ?? 1000;
      $latlng['radius'] = $this->normalizeRadius($radius);

      return $latlng;
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function buildInputElements($values, array &$elements, FormStateInterface $form_state, ?LocationResolverInterface $resolver = NULL): void {
    $config = $this->getConfiguration();
    $titleDisplay = $config['label_display'] ?? 'before';

    // Create and update caching dependencies for this form input when using
    // a location resolver.
    $cache = CacheableMetadata::createFromRenderArray($elements);
    if ($resolver) {
      $cache->addCacheableDependency($resolver);

      // If missing a default value, see if one can be populated.
      if (empty($values['loc'])) {
        if ($latLng = $resolver->getLatLng()) {
          $values['loc']['text'] = $resolver->getLocationText();
          $values['loc']['latlng'] = $latLng['lat'] . ',' . $latLng['lng'];
        }
      }
    }

    $radii = [];
    foreach ($config['radii'] ?? [] as $radius) {
      $radii[$radius['value']] = $radius['label'];
    }

    $elements['#tree'] = TRUE;
    $elements['radius'] = [
      '#type' => 'select',
      '#title' => $config['radius_label'] ?? '',
      '#title_display' => $titleDisplay,
      '#options' => $radii,
      '#default_value' => $values['radius'] ?? NULL,
    ];

    if ($bounds = $config['bounds']) {
      $bounds['type'] = $config['bound_type'];
    }

    $elements['loc'] = [
      '#type' => 'mapkit_autocomplete',
      '#title' => $config['label'],
      '#title_display' => $titleDisplay,
      '#default_value' => $values['loc'] ?? NULL,
      '#data_keys' => ['latlng'],
      '#handler' => $config['handler'],
      '#bounds' => $bounds,
      '#device_location' => $config['device_location'] ?? FALSE,
      '#filter_pattern' => $config['pattern'],
      '#placeholder' => $config['placeholder'],
      '#autocomplete_match_type' => $config['match_type'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $config = $this->getConfiguration();
    $handlerOpts = $this->acManager->getAutocompleteOptions();
    if (1 === count($handlerOpts)) {
      $config['handler'] = key($handlerOpts);
    }

    $form['handler'] = [
      '#type' => 'select',
      '#title' => $this->t('Autocomplete handler'),
      '#required' => TRUE,
      '#options' => $handlerOpts,
      '#default_value' => $config['handler'],
      '#weight' => -5,
    ];
    $form['match_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Result types'),
      '#options' => [
        '' => $this->t('Any'),
        'address' => $this->t('Address level results'),
        'city' => $this->t('Cities'),
        'region' => $this->t('City, State or ZIP'),
      ],
      '#default_value' => $config['match_type'],
      '#weight' => -4,
    ];
    $form['pattern'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Results pattern (regular expression)'),
      '#default_value' => $config['pattern'],
      '#description' => $this->t('Used to filter which suggestions get displayed in the autocomplete. Leave empty to show all results from the autocomplete service.'),
    ];

    $form['bounds'] = [
      '#type' => 'details',
      '#title' => $this->t('Search bounds'),
      '#tree' => TRUE,
      '#description_display' => 'before',
      '#description' => $this->t('Provide a map bounds (in latitude/longitude ranges) to bias results. Results are more likely to appear from these bounds, though the specific implemention and rules will be up to the autocomplete service used.'),
      '#weight' => 20,

      'bound_type' => [
        '#type' => 'select',
        '#title' => $this->t('Bounds type'),
        '#options' => [
          'bias' => $this->t('Bias'),
          'restriction' => $this->t('Restriction'),
        ],
        '#default_value' => $config['bound_type'],
      ],
      'bounds' => [
        '#type' => 'latlng_bound',
        '#title' => $this->t('Bounds latitude/longitude'),
        '#default_value' => $config['bounds'],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $this->configuration['handler'] = $form_state->getValue('handler');
    $this->configuration['match_type'] = $form_state->getValue('match_type');
    $this->configuration['pattern'] = $form_state->getValue('pattern');

    $boundValues = $form_state->getValue('bounds');
    $this->configuration['bound_type'] = $boundValues['bound_type'];
    $this->configuration['bounds'] = $boundValues['bounds'];
  }

}
