<?php

namespace Drupal\mapkit\Plugin\Mapkit\LocationInput;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\geocoder\GeocoderInterface;
use Drupal\mapkit\Plugin\GeocodedInputTrait;
use Drupal\mapkit\Plugin\LocationInputInterface;
use Drupal\mapkit\Plugin\LocationResolverInterface;
use Drupal\mapkit\Utility\MapkitHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Basic textfield for entering lat/lng or an address if geocoder is available.
 *
 * @MapkitLocationInput(
 *   id = "textfield",
 *   label = @Translation("Textfield"),
 * )
 */
class TextfieldLocationInput extends PluginBase implements LocationInputInterface, ContainerFactoryPluginInterface, PluginFormInterface {

  use GeocodedInputTrait;

  /**
   * Create a new instance of the TextFieldLocationInput plugin instance.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->configuration += $this->defaultConfiguration();
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );

    // Should only be available when the Geocoder service is available and
    // allows the location plugin to use a geocoding fallback.
    if ($geocoder = $container->get('geocoder', ContainerInterface::NULL_ON_INVALID_REFERENCE)) {
      $instance->setGeocoderService($geocoder);
    }

    return $instance;
  }

  /**
   * Set the geocoder service if the feature is available.
   *
   * Should be set during dependency injection in the ::create() method or
   * in the service file using:
   *
   * @code
   * example.plugin.service:
   *   arguments:
   *     - '@entity_type.manager'
   *   calls:
   *     - [setGeocoderService, ['@?geocoder]]
   * @endcode
   *
   * This allows the service to be optionally set if it is available.
   *
   * @param \Drupal\geocoder\GeocoderInterface $geocoder
   *   The geocoder manager service.
   */
  public function setGeocoderService(GeocoderInterface $geocoder): void {
    $this->geocoder = $geocoder;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'label_display' => 'before',
      'radius_label' => $this->t('Located'),
      'label' => $this->t('From'),
      'placeholder' => '',
      'device_location' => FALSE,
      'units' => 'mi',
      'radii' => [
        [
          'value' => 10,
          'label' => '10 miles',
        ],
        [
          'value' => 15,
          'label' => '15 miles',
        ],
        [
          'value' => 25,
          'label' => '25 miles',
        ],
        [
          'value' => 50,
          'label' => '50 miles',
        ],
      ],
      'geocoder' => $this->getDefaultGeocodeConfiguration(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration + $this->defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function acceptInput($input): bool {
    return !(empty($input['radius']) || empty($input['loc']));
  }

  /**
   * Ensure that the radius value is converted to kilometers.
   *
   * The Search API location search functionality requires that the distance
   * values are all in kilometers. Ensure that the value is converted from
   * other units of measurement to kilometers.
   *
   * @param string|float $radius
   *   The radius value in the currently configured unit of measure.
   *
   * @return float
   *   The radius value represent in kilometers.
   */
  public function normalizeRadius($radius): float {
    if (!is_float($radius)) {
      $radius = floatval($radius);
    }

    $config = $this->configuration;
    return $radius > 0 ? MapkitHelper::toKilometers($radius, $config['units']) : 0;
  }

  /**
   * {@inheritdoc}
   */
  public function processInput($input): ?array {
    $value = trim($input['loc']);

    if (empty($value)) {
      return NULL;
    }

    $config = $this->getConfiguration();
    $latlng = MapkitHelper::parseLatLng($value) ?: $this->geocodeInput($value, $config['geocoder']);

    // A valid lat/lng value was available.
    if ($latlng) {
      $radius = $input['radius'] ?? 1000;
      $latlng['radius'] = $this->normalizeRadius($radius);

      return $latlng;
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function buildInputElements($values, array &$elements, FormStateInterface $form_state, LocationResolverInterface $resolver = NULL): void {
    $config = $this->getConfiguration();
    $titleDisplay = $config['label_display'] ?? 'before';

    $radii = [];
    foreach ($config['radii'] ?? [] as $radius) {
      $radii[$radius['value']] = $radius['label'];
    }

    $elements['#tree'] = TRUE;
    $elements['radius'] = [
      '#type' => 'select',
      '#title' => $config['radius_label'] ?? '',
      '#title_display' => $titleDisplay,
      '#options' => $radii,
      '#default_value' => $values['radius'] ?? NULL,
    ];

    $cache = CacheableMetadata::createFromRenderArray($elements);
    // If missing a default value, see if one can be populated.
    if (empty($values['loc']) && $resolver) {
      $cache->addCacheableDependency($resolver);

      if ($latLng = $resolver->getLatLng()) {
        $values['loc'] = $latLng['lat'] . ',' . $latLng['lng'];
      }
    }

    $elements['loc'] = [
      '#type' => 'textfield',
      '#title' => $config['label'] ?? '',
      '#title_display' => $titleDisplay,
      '#default_value' => $values['loc'] ?? NULL,
    ];

    if ($config['placeholder']) {
      $elements['loc']['#placeholder'] = $config['placeholder'];
    }

    $cache->applyTo($elements);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();

    $form['label_display'] = [
      '#type' => 'select',
      '#title' => $this->t('Input label display'),
      '#options' => [
        'before' => $this->t('Default'),
        'inline' => $this->t('Inline labels'),
        'invisible' => $this->t('Invisibile'),
      ],
      '#default_value' => $config['label_display'],
      '#description' => $this->t('Defines how will the input element labels display for this location input. Do not wish to display any of the labels, it is recommended to provided labels for accessibility, and display them as "invisible" here.'),
    ];
    $form['radius_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Radius selection label'),
      '#default_value' => $config['radius_label'],
    ];
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Location entry label'),
      '#default_value' => $config['label'],
    ];
    $form['placeholder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Location placeholder text'),
      '#default_value' => $config['placeholder'],
      '#description' => $this->t('Keep in mind that addresses will only be accepted if geocoding is enabled and configured.'),
    ];
    $form['device_location'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow the use of the device location'),
      '#default_value' => $config['device_location'],
    ];

    // Add the radius values and settings.
    $radiiText = [];
    foreach ($config['radii'] as $radius) {
      $radiiText[] = $radius['value'] . ' ' . $radius['label'] ?? '';
    }
    $form['units'] = [
      '#type' => 'select',
      '#title' => $this->t('Distance units'),
      '#options' => MapkitHelper::getDistanceUnits('label'),
      '#default_value' => $config['units'] ?? 'km',
      '#weight' => 10,
    ];
    $form['radii'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Available radius values'),
      '#required' => TRUE,
      '#rows' => 6,
      '#default_value' => implode("\n", $radiiText),
      '#description' => $this->t('Add distance ranges with just one value per line. The first value should be the distance value (single number in kilometers or miles), and a second value (separated by a space) should be the display label.'),
      '#weight' => 10,
    ];

    // Add geocoder settings and configure providers.
    $form['geocoder'] = [];
    $this->buildGeocoderFormElements($config['geocoder'], $form['geocoder']);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    // Validate the radii settings from the text form entry.
    $radiiText = preg_split("/\r?\n/", $form_state->getValue('radii') ?: '', -1, PREG_SPLIT_NO_EMPTY);
    foreach ($radiiText as $radiusText) {
      $radiusParts = explode(' ', $radiusText, 2);

      if (!is_numeric($radiusParts[0])) {
        $form_state->setError($form['radii'], $this->t('Radius values need to be distance values (numbers): "@value" is not valid.', [
          '@value' => $radiusParts[0],
        ]));
        break;
      }
    }

    $this->validateGeocoderFormElements($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $valueKeys = [
      'label_display',
      'radius_label',
      'label',
      'placeholder',
      'device_location',
      'units',
    ];

    $values = $form_state->getValues();
    foreach ($valueKeys as $key) {
      $this->configuration[$key] = $values[$key];
    }

    // Parse the radii options.
    $radiiText = preg_split("/\r?\n/", $values['radii'] ?? '', -1, PREG_SPLIT_NO_EMPTY);
    $this->configuration['radii'] = [];
    foreach ($radiiText as $radiusText) {
      $radiusParts = explode(' ', $radiusText, 2);
      $radius = floatval($radiusParts[0]);

      if ($radius > 0) {
        $this->configuration['radii'][] = [
          'value' => $radius,
          'label' => trim($radiusParts[1] ?? "$radius {$values['units']}"),
        ];
      }
    }

    $this->configuration['geocoder'] = $this->submitGeocoderFormElements($form['geocoder'], $form_state);
  }

}
