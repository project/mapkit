<?php

namespace Drupal\mapkit\Plugin\Mapkit\LocationResolver;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Cache\Cache;
use Drupal\mapkit\Plugin\LocationResolverInterface;

/**
 * The NULL location resolver.
 *
 * Provided for use when a location does not need to be resolved. Will always
 * return NULL for the location resolution.
 *
 * @MapkitLocationResolver(
 *   id = "null_resolver",
 *   label = @Translation("No location"),
 * )
 */
class NullLocationResolver extends PluginBase implements LocationResolverInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration + $this->defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return Cache::PERMANENT;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getLatLng(): ?array {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getLocationText(): ?string {
    return NULL;
  }

}
