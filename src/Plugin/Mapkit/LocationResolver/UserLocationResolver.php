<?php

namespace Drupal\mapkit\Plugin\Mapkit\LocationResolver;

use Drupal\Component\Plugin\Exception\ContextException;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\ContextAwarePluginAssignmentTrait;
use Drupal\Core\Plugin\ContextAwarePluginInterface;
use Drupal\Core\Plugin\ContextAwarePluginTrait;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\TypedData\Exception\MissingDataException;
use Drupal\mapkit\GeoParser\GeoParserManagerInterface;
use Drupal\mapkit\Plugin\LocationResolverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Location resolver for determining a location from a user account entity.
 *
 * @MapkitLocationResolver(
 *   id = "user_location",
 *   label = @Translation("Location from user account"),
 *   context_definitions = {
 *     "user" = @ContextDefinition("entity:user", label=@Translation("User"), required=TRUE),
 *   },
 * )
 */
class UserLocationResolver extends PluginBase implements LocationResolverInterface, ContextAwarePluginInterface, ContainerFactoryPluginInterface, PluginFormInterface {

  use ContextAwarePluginAssignmentTrait;
  use ContextAwarePluginTrait;
  use StringTranslationTrait;

  /**
   * Entity field manager service for getting field information.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $fieldManager;

  /**
   * The geo-parser strategy manager.
   *
   * @var \Drupal\mapkit\GeoParser\GeoParserManagerInterface
   */
  protected GeoParserManagerInterface $geoParserManager;

  /**
   * Create a new user location resolver plugin.
   *
   * @param array $configuration
   *   Configurations for the user address field to use and the values.
   * @param string $plugin_id
   *   The string ID of the plugin.
   * @param mixed $plugin_definition
   *   The plugin definition from the discovery.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   Entity field manager, to get information about entity fields.
   * @param \Drupal\mapkit\GeoParser\GeoParserManagerInterface $geo_parser_manager
   *   The geo-parser strategy manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityFieldManagerInterface $entity_field_manager, GeoParserManagerInterface $geo_parser_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->configuration += $this->defaultConfiguration();
    $this->fieldManager = $entity_field_manager;
    $this->geoParserManager = $geo_parser_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_field.manager'),
      $container->get('strategy.manager.mapkit.geo_parser')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'field' => NULL,
      'address' => NULL,
      'context_mapping' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration + $this->defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return Cache::PERMANENT;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    try {
      $account = $this->getEntity();
      return $account ? $account->getCacheTags() : [];
    }
    catch (ContextException $e) {
      // Missing user context, error is already recorded in the
      // self::getCacheContexts() - avoid double logs of the error.
    }

    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    $contexts = ['user.roles:anonymous'];

    try {
      /** @var \Drupal\user\UserInterface $account */
      $account = $this->getEntity();

      if ($account && $account->isAuthenticated()) {
        $contexts = Cache::mergeContexts($contexts, $account->getCacheContexts());
      }
    }
    catch (ContextException $e) {
      // Missing user context. Should not happen normally since this is a
      // a required context, but handle it properly if it does.
      watchdog_exception('mapkit.resolver', $e);
    }

    return $contexts;
  }

  /**
   * Get the entity type ID for the entity to get the location information form.
   *
   * @return string
   *   The machine name of the entity type being worked on by this location
   *   resolver plugin type.
   */
  public function getEntityType() {
    return 'user';
  }

  /**
   * Get the entity that is the source of the location information.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface|null
   *   The entity to extract the location data from.
   */
  public function getEntity(): ?ContentEntityInterface {
    $entityTypeId = $this->getEntityType();
    return $this->getContextValue($entityTypeId);
  }

  /**
   * Get the list of address item properties to display as the location text.
   *
   * @return array
   *   The list of address item properties to include in the text display.
   */
  public function getDisplayProperties(): array {
    // In the future this should be a configuration.
    return [
      'address_line1',
      'locality',
      'administrative_area',
      'postal_code',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getLatLng(): ?array {
    try {
      /** @var \Drupal\user\UserInterface $account */
      $account = $this->getEntity();
      $fieldName = $this->configuration['field'] ?? NULL;

      if (!$fieldName || $account->isAnonymous()) {
        return NULL;
      }

      if ($fieldDef = $account->getFieldDefinition($fieldName)) {
        $fieldType = $fieldDef->getType();

        if ($parser = $this->geoParserManager->getFieldParser($fieldType)) {
          return $parser->parseField($account->{$fieldName}) ?: NULL;
        }
      }
    }
    catch (ContextException $e) {
      // Missing the user context to generate the location from.
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getLocationText(): ?string {
    try {
      /** @var \Drupal\user\UserInterface $account */
      $account = $this->getEntity();
      $addressField = $this->configuration['address'] ?? NULL;

      if (!$addressField || $account->isAnonymous()) {
        return NULL;
      }

      /** @var \Drupal\address\AddressInterface $address */
      $address = $account->get($addressField)->first();
      if ($address) {
        $text = [];
        $values = $address->toArray();

        foreach ($this->getDisplayProperties() as $key) {
          if (!empty($values[$key])) {
            $text[] = $values[$key];
          }
        }

        return $text ? implode(', ', $text) : NULL;
      }
    }
    catch (ContextException $e) {
      // Missing the user context to generate the location from.
    }
    catch (\InvalidArgumentException | MissingDataException $e) {
      // Address field doesn't exist.
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $fields = $this->getGeolocationFields();

    $form['field'] = [
      '#type' => 'select',
      '#title' => $this->t('User field to geolocation from'),
      '#options' => $fields,
      '#default_value' => $this->configuration['field'],
    ];

    // Optional address field value to generate display text for.
    $form['address'] = [
      '#type' => 'select',
      '#title' => $this->t('Address display field'),
      '#options' => $this->getAddressFields(),
      '#default_value' => $this->configuration['address'],
    ];

    // Add user context selection.
    $contexts = $form_state->getTemporaryValue('gathered_contexts') ?: [];
    $form['context_mapping'] = $this->addContextAssignmentElement($this, $contexts);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->setConfiguration([
      'field' => $form_state->getValue('field'),
      'address' => $form_state->getValue('address'),
    ]);

    $this->setContextMapping($form_state->getValue('context_mapping'));
  }

  /**
   * Find entity field candidates for extracting lat/lng data.
   *
   * Get a list of fields for the target entity which are compatible with the
   * available geo-parsers.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup[]|string[]
   *   List of available fields for the target entity type which is compatible
   *   with a location geo-parser.
   */
  protected function getGeolocationFields(): array {
    $entityTypeId = $this->getEntityType();
    $fieldTypes = array_keys($this->geoParserManager->getParserByType('field'));
    $fieldDefinitions = $this->fieldManager->getFieldDefinitions($entityTypeId, 'user');

    $fields = [];
    foreach ($fieldDefinitions as $fieldName => $fieldDef) {
      if (in_array($fieldDef->getType(), $fieldTypes)) {
        $fields[$fieldName] = $fieldDef->getLabel() ?: $fieldName;
      }
    }

    return $fields;
  }

  /**
   * Find user fields that are candidates for the address field display.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup[]|string[]
   *   List of available address fields with the label as the value and
   *   fieldname as the array key.
   */
  protected function getAddressFields(): array {
    $entityTypeId = $this->getEntityType();
    $fieldDefinitions = $this->fieldManager->getFieldDefinitions($entityTypeId, 'user');

    $fields = [];
    foreach ($fieldDefinitions as $fieldName => $fieldDef) {
      if ('address' === $fieldDef->getType()) {
        $fields[$fieldName] = $fieldDef->getLabel() ?: $fieldName;
      }
    }

    return $fields;
  }

}
