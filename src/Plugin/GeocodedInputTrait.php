<?php

namespace Drupal\mapkit\Plugin;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformStateInterface;
use Drupal\Core\Link;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\geocoder\GeocoderInterface;
use Drupal\geocoder\GeocoderProviderInterface;
use Drupal\mapkit\Exception\InvalidLocationException;

/**
 * Trait for supporting geocoder services for location input plugins.
 *
 * Adds helpers for creating configuration forms, setting configuration values
 * and utilizing the geocoder services when needed.
 */
trait GeocodedInputTrait {

  use StringTranslationTrait;

  /**
   * Geocoder service if available.
   *
   * @var \Drupal\geocoder\GeocoderInterface|null
   */
  protected ?GeocoderInterface $geocoder = NULL;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface|null
   */
  protected ?EntityTypeManagerInterface $entityTypeManager;

  /**
   * Set the geocoder service if the feature is available.
   *
   * Should be set during dependency injection in the ::create() method or
   * in the service file using:
   *
   * @code
   *   calls:
   *     - [setGeocoderService, ['@?geocoder]]
   * @endcode
   *
   * This allows the service to be optionally set if it is available.
   *
   * @param \Drupal\geocoder\GeocoderInterface $geocoder
   *   The geocoder manager service.
   */
  public function setGeocoderService(GeocoderInterface $geocoder): void {
    $this->geocoder = $geocoder;
  }

  /**
   * Get the entity type manager service.
   *
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   *   The entity type manager.
   */
  protected function getEntityTypeManager(): EntityTypeManagerInterface {
    if (!isset($this->entityTypeManager)) {
      $this->entityTypeManager = \Drupal::entityTypeManager();
    }

    return $this->entityTypeManager;
  }

  /**
   * Get the default values for geocoder configurations.
   *
   * @return array
   *   Get the default values for setting up the geocoder providers and how we
   *   enable the geocoding features.
   */
  protected function getDefaultGeocodeConfiguration(): array {
    return [
      'enabled' => FALSE,
      'country' => NULL,
      'admin_areas' => [],
      'providers' => [],
    ];
  }

  /**
   * Geocode the address value using the configured geocoder settings.
   *
   * Address will only be geocoded if the geocoder services are available
   * and the geocoder configurations are enabled.
   *
   * @param string $value
   *   The string value to attempt to geocode.
   * @param array $configuration
   *   The geocoder configurations.
   *
   * @return array|null
   *   An array with a valid lat/lng value if successfully geocoded, otherwise
   *   NULL if geocoder is not enabled or address was invalid.
   */
  protected function geocodeInput(string $value, array $configuration = []): ?array {
    if ($this->geocoder && !empty($configuration['enabled'])) {
      $providers = [];

      if ($providerIds = $configuration['providers'] ?? FALSE) {
        $providers = $this->getEntityTypeManager()
          ->getStorage('geocoder_provider')
          ->loadMultiple($providerIds);
      }

      if ($configuration['country']) {
        // Append a country code to geocoding value. It would be okay
        // even if this value was already there.
        $value .= ', ' . $configuration['country'];
      }

      $locations = $this->geocoder->geocode($value, $providers);

      // Location could not be resolved.
      if (!$locations || $locations->isEmpty()) {
        throw new InvalidLocationException($value);
      }

      if ($configuration['admin_areas']) {
        foreach ($locations->getIterator() as $location) {
          try {
            $adminLvl = $location->getAdminLevels()->get(1);

            // Look for a preferred location in the matching admin areas.
            if (in_array($adminLvl->getCode(), $configuration['admin_areas'])) {
              $coords = $location->getCoordinates();
              return [
                'lat' => $coords->getLatitude(),
                'lng' => $coords->getLongitude(),
              ];
            }
          }
          catch (\InvalidArgumentException $e) {
            // Skip, it doesn't have the admin level to compare.
          }
        }
      }
      else {
        $coords = $locations->first()->getCoordinates();
        return [
          'lat' => $coords->getLatitude(),
          'lng' => $coords->getLongitude(),
        ];
      }
    }

    return NULL;
  }

  /**
   * Build the geocoder configurations elements for the location input plugins.
   *
   * @param array $values
   *   The current geocoder configuration values.
   * @param array $elements
   *   The form elements to add the geocoder configurations to.
   */
  protected function buildGeocoderFormElements(array $values, array &$elements): void {
    $elements['#type'] = 'details';
    $elements['#tree'] = TRUE;
    $elements += [
      '#title' => $this->t('Address Geocoding'),
      '#open' => empty($values['providers']),
      '#description' => $this->t('Enabled the geocoder providers in the order that they should be tried to resolve locations.'),
      '#weight' => 99,
    ];

    // Ensure defaults.
    $values += $this->getDefaultGeocodeConfiguration();
    $entityTypeManager = $this->getEntityTypeManager();

    // If geocoder options are available and using geocoder 4.x or newer.
    if ($this->geocoder && $entityTypeManager->hasDefinition('geocoder_provider')) {
      // Get list of currently available geocoder providers.
      $providers = $entityTypeManager
        ->getStorage('geocoder_provider')
        ->loadMultiple();

      if ($providers) {
        $elements['enabled'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Enable geocoder for address entry'),
          '#default_value' => $values['enabled'],
        ];
        $elements['country'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Country code to bias geocoding searches'),
          '#max_length' => 2,
          '#length' => 2,
          '#default_value' => $values['country'],
          '#description' => $this->t('Allowes for the use of a single the 2 letter country code, of the country to bias geocoding results with, or empty if no bias.'),
        ];
        $elements['admin_areas'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Allowed level 1 administrative areas'),
          '#default_value' => implode(', ', $values['admin_areas']),
          '#description' => $this->t('List allowed administrative area codes to allow in the results (for the USA, these are the state abbreviations, or counties in Canada, etc). Leave empty to accept all results.'),
        ];

        $weight = 0;
        $elements['providers'] = [
          '#type' => 'table',
          '#header' => [
            'enabled' => $this->t('Provider'),
            'weight' => $this->t('Order'),
          ],
          '#tabledrag' => [
            [
              'action' => 'order',
              'relationship' => 'sibling',
              'group' => 'draggable-weight',
            ],
          ],
        ];

        // Ensure that enabled providers are always shown first.
        foreach ($values['providers'] as $pid) {
          if ($provider = $providers[$pid] ?? NULL) {
            $elements['providers'][$pid] = $this->buildGeocoderProviderRow($provider, TRUE, $weight++);
          }
        }

        // Add all the other available providers after the enabled ones.
        foreach (array_diff_key($providers, $values['providers']) as $pid => $provider) {
          $elements['providers'][$pid] = $this->buildGeocoderProviderRow($provider, FALSE, $weight++);
        }
      }
      else {
        $elements['content'] = [
          '#markup' => $this->t('There are currently no Geocoder providers available. Go to @link page and setup a provider to use this feature.', [
            '@link' => Link::createFromRoute('Geocoder providers administration', 'entity.geocoder_provider.collection')->toString(),
          ]),
        ];
      }
    }

    // Ensure configuration values event if no geocoding is available.
    $elements += [
      'enabled' => ['#value' => FALSE],
      'providers' => ['#value' => []],
    ];
  }

  /**
   * Validate the geocoder configuration elements.
   *
   * @param array $elements
   *   The geocoder form elements.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The input plugin element configuration state. This can be a subform
   *   state as plugin forms can be embedded into a larger form.
   */
  protected function validateGeocoderFormElements(array &$elements, FormStateInterface $form_state): void {
  }

  /**
   * Submit the geocoder configurations from the form elements.
   *
   * @param array $elements
   *   The geocoder configuration form elements.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The input plugin element configuration state. This can be a subform
   *   state as plugin forms can be embedded into a larger form.
   *
   * @return array
   *   The geocoder configurations from the submitted form elements.
   */
  protected function submitGeocoderFormElements(array &$elements, FormStateInterface $form_state): array {
    $form_state = $form_state instanceof SubformStateInterface
      ? $form_state->getCompleteFormState()
      : $form_state;

    $values = $form_state->getValue($elements['#parents']);
    $config = [
      'enabled' => !empty($values['enabled']),
      'country' => strtoupper($values['country']) ?: NULL,
      'admin_areas' => [],
      'providers' => [],
    ];

    // Capture the available administrative areas. Ensure they are capitalized
    // as the geocoding services tend to return them in all capitals.
    if (!empty($values['admin_areas'])) {
      $admin_areas = array_map(function ($area) {
        return strtoupper(trim($area));
      }, explode(',', $values['admin_areas']));

      $config['admin_areas'] = array_filter($admin_areas, 'strlen');
    }

    // Extract and organize the geocoder providers.
    if (!empty($values['providers'])) {
      uasort($values['providers'], '\Drupal\Component\Utility\SortArray::sortByWeightElement');

      foreach ($values['providers'] as $pid => $providerValues) {
        if (!empty($providerValues['enabled'])) {
          $config['providers'][$pid] = $pid;
        }
      }
    }

    return $config;
  }

  /**
   * Create the form element table row for the geocoder provider configuration.
   *
   * @param \Drupal\geocoder\GeocoderProviderInterface $provider
   *   The geocode provider instance.
   * @param bool $enabled
   *   Is this provider enabled currently?
   * @param int $weight
   *   The sorting weight to determine the ordering of providers when geocoding.
   *
   * @return array
   *   The table draggable row elements to represent the geocoder provider.
   */
  protected function buildGeocoderProviderRow(GeocoderProviderInterface $provider, bool $enabled, int $weight): array {
    return [
      '#attributes' => [
        'class' => ['draggable'],
      ],
      'enabled' => [
        '#type' => 'checkbox',
        '#title' => $provider->label(),
        '#default_value' => $enabled,
      ],
      'weight' => [
        '#type' => 'number',
        '#title' => $this->t('Sort order'),
        '#title_display' => 'invisible',
        '#default_value' => $weight++,
        '#attributes' => [
          'class' => ['draggable-weight'],
        ],
      ],
    ];
  }

}
