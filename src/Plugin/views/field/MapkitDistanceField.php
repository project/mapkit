<?php

namespace Drupal\mapkit\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\mapkit\Utility\MapkitHelper;
use Drupal\search_api\Plugin\views\field\SearchApiFieldTrait;
use Drupal\views\Plugin\views\field\MultiItemsFieldHandlerInterface;
use Drupal\views\Plugin\views\field\NumericField;
use Drupal\views\ResultRow;

/**
 * Displays distances returned from Search API.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("mapkit_distance_field")
 */
class MapkitDistanceField extends NumericField implements MultiItemsFieldHandlerInterface {

  use SearchApiFieldTrait {
    defineOptions as traitDefineOptions;
  }

  /**
   * {@inheritdoc}
   */
  public function defineOptions() {
    $options = $this->traitDefineOptions();

    $options['distance_units'] = ['default' => 'km'];
    $options['display_units'] = ['default' => TRUE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    // Hide these options as they are not relevant when dealing with distances
    // returned from the Search API or Solr.
    $form['format_plural']['#access'] = FALSE;
    $form['format_plural_string']['#access'] = FALSE;
    $form['format_plural_values']['#access'] = FALSE;
    $form['prefix']['#access'] = FALSE;
    $form['suffix']['#access'] = FALSE;

    $form['distance_units'] = [
      '#type' => 'select',
      '#title' => $this->t('Display in units of'),
      '#required' => TRUE,
      '#options' => MapkitHelper::getDistanceUnits('label'),
      '#default_value' => $this->options['distance_units'],
      '#weight' => -1,
    ];

    $form['display_units'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show unit as suffix'),
      '#default_value' => $this->options['display_units'],
      '#weight' => -1,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $value = $this->getValue($values);

    if (is_array($value)) {
      $value = reset($value);
    }
    if (empty($value) || !is_numeric($value)) {
      return '';
    }

    // Check to see if hiding should happen before adding prefix and suffix
    // and before rewriting.
    if ($this->options['hide_empty'] && empty($value) && ($value !== 0 || $this->options['empty_zero'])) {
      return '';
    }

    // Convert the value to miles if needed, the Search API results will always
    // return the distance value in kilometers. No conversion needed for 'km'.
    if ('mi' === $this->options['distance_units']) {
      $value = MapkitHelper::toMiles($value, 'km');
    }

    if (!empty($this->options['set_precision'])) {
      $precision = $this->options['precision'];
    }
    elseif ($decimal_position = strpos($value, '.')) {
      $precision = strlen($value) - $decimal_position - 1;
    }
    else {
      $precision = 0;
    }

    // Use round first to avoid negative zeros.
    $value = round($value, $precision);
    // Test against both integer zero and float zero.
    if ($this->options['empty_zero'] && ($value === 0 || $value === 0.0)) {
      return '';
    }

    $formatted = number_format($value, $precision, $this->options['decimal'], $this->options['separator']);
    return $formatted . ' ' . $this->sanitizeValue($this->options['distance_units'], 'xss');
  }

}
