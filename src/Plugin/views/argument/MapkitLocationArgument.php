<?php

namespace Drupal\mapkit\Plugin\views\argument;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\mapkit\GeoParser\GeoParserManagerInterface;
use Drupal\mapkit\Plugin\views\SearchApiProximityTrait;
use Drupal\mapkit\Utility\MapkitHelper;
use Drupal\search_api\Plugin\views\SearchApiHandlerTrait;
use Drupal\views\Plugin\views\argument\ArgumentPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Contextual filter for location based searches.
 *
 * @ingroup views_argument_plugins
 *
 * @ViewsArgument("mapkit_location_argument")
 */
class MapkitLocationArgument extends ArgumentPluginBase implements ContainerFactoryPluginInterface {

  use SearchApiHandlerTrait;
  use SearchApiProximityTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected EntityTypeBundleInfoInterface $entityBundleInfo;

  /**
   * Location geo-parser strategy manager.
   *
   * @var \Drupal\mapkit\GeoParser\GeoParserManagerInterface
   */
  protected GeoParserManagerInterface $geoParserManager;

  /**
   * Create a new instance of the MapkitLocationArgument view argument plugin.
   *
   * @param array $configuration
   *   The plugin configurations.
   * @param string $plugin_id
   *   The plugin identifier.
   * @param mixed $plugin_definition
   *   The views plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info service.
   * @param \Drupal\mapkit\GeoParser\GeoParserManagerInterface $geo_parser_manager
   *   Location geo-parser strategy manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info, GeoParserManagerInterface $geo_parser_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->entityBundleInfo = $entity_type_bundle_info;
    $this->geoParserManager = $geo_parser_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('strategy.manager.mapkit.geo_parser')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['radius'] = ['default' => 80.46];
    $options['entity_fields'] = ['default' => []];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    if ($fieldOptions = $this->getEntityLocationFieldOptions()) {
      $form['radius'] = [
        '#type' => 'number',
        '#title' => $this->t('Search radius'),
        '#field_suffix' => 'km',
        '#required' => TRUE,
        '#min' => 0,
        '#step' => 0.01,
        '#weight' => -10,
        '#default_value' => $this->options['radius'],
      ];

      $form['entity_fields'] = [
        '#type' => 'details',
        '#title' => $this->t('Location fields'),
        '#open' => TRUE,
        '#weight' => -5,
        '#description' => $this->t('Entities with location fields can be used as the location argument. Only entity types and bundles that have compatible location fields are displayed and can be set here. <br/>NOTE: that in order to use entities as arguments you must set the argument validator to an entity of a supported type.'),
      ];

      foreach ($fieldOptions as $entityTypeId => $bundles) {
        foreach ($bundles as $id => $fieldOpts) {
          $form['entity_fields'][$entityTypeId][$id] = [
            '#type' => 'select',
            '#title' => $fieldOpts['label'],
            '#options' => $fieldOpts['options'],
            '#default_value' => $this->options['entity_fields'][$entityTypeId][$id] ?? NULL,
          ];
        }
      }
    }
    else {
      // No available fields, so just set the options to an empty array.
      $form['entity_fields'] = [
        '#type' => 'value',
        '#value' => [],
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function query($group_by = FALSE) {
    if (empty($this->argument)) {
      return;
    }

    if ($latlng = MapkitHelper::parseLatLng($this->argument)) {
      $this->value = $latlng;
    }
    else {
      $entityTypeId = FALSE;
      $validator = $this->getPlugin('argument_validator');

      // @todo Determine if there are other ways to determine the entity types of
      // arguments? Would be nice have more flexibility here.
      if ($validator && $contextDef = $validator->getContextDefinition()) {
        if (preg_match('/^entity:([a-z_]+)$/', $contextDef->getDataType(), $matches)) {
          $entityTypeId = $matches[1];
        }
      }

      // If an entity type can be determined and it has valid location fields,
      // try to extract proximity values.
      if ($entityTypeId && isset($this->options['entity_fields'][$entityTypeId])) {
        $entityFields = $this->options['entity_fields'][$entityTypeId];

        /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
        $entity = $this->entityTypeManager
          ->getStorage($entityTypeId)
          ->load($this->argument);

        if ($entity && $fieldName = $entityFields[$entity->bundle()] ?? FALSE) {
          $fieldType = $entity->hasField($fieldName) ? $entity->getFieldDefinition($fieldName)->getType() : FALSE;
          $parser = $fieldType ? $this->geoParserManager->getFieldParser($fieldType) : NULL;

          if ($parser) {
            $this->value = reset($parser->parseField($entity->get($fieldName)));
          }
        }
      }
    }

    if ($this->value) {
      $this->ensureMyTable();

      $radius = $this->options['radius'];
      $this->addProximitySearch($this->getQuery(), $this->realField, $this->value, $radius);
    }

  }

  /**
   * Add a spatial field search criteria to the Search API query.
   *
   * @param array $location_search
   *   Current spatial search condition to add the field proximity values to.
   * @param string $field
   *   The ID of the field to search on.
   * @param array $value
   *   The lat/lng value to run the spatial search from.
   * @param float $radius
   *   The radius to limit the proximity search range to.
   */
  protected function addLocationQuery(array &$location_search, string $field, array $value, $radius): void {
    $cond = [
      'field' => $field,
      'lat' => $value['lat'],
      'lon' => $value['lng'],
      'radius' => $radius,
    ];

    foreach ($location_search as $key => $search) {
      if ($search['field'] === $field) {
        $location_search[$key] = $cond + $search;
        return;
      }
    }

    // Create a new location field condition with the proximity info.
    $location_search[] = $cond;
  }

  /**
   * Get a list of location compatible fields by entity type and bundle.
   *
   * @code
   * 'entity_type_id' => [
   *   'bundle_id' => [<field_names>],
   *   ...
   * ],
   * @endcode
   *
   * @return array
   *   Array of location compatible fields by entity types and bundles.
   */
  protected function getEntityLocationFields(): array {
    $fieldTypes = $this->geoParserManager->getParserByType('field');

    $entityMap = [];
    foreach (array_keys($fieldTypes) as $fieldType) {
      $fieldMap = $this->entityFieldManager->getFieldMapByFieldType($fieldType);

      foreach ($fieldMap as $entityTypeId => $fields) {
        foreach ($fields as $fieldName => $field) {
          foreach ($field['bundles'] as $bundle) {
            $entityMap[$entityTypeId][$bundle][] = $fieldName;
          }
        }
      }
    }

    return $entityMap;
  }

  /**
   * Get field options for location field select elements by entity bundle.
   *
   * Gets a list of entity field options which are compatible with location
   * filter. The field options are organized by entity type and bundles.
   *
   * @code
   * 'entity_type_id' => [
   *   'bundle_id' => [
   *      'label' => <bundle_label>,
   *      'options' => [
   *        <field_names> => <field_label>,
   *        ...
   *      ],
   *   ],
   *   ...
   * ],
   * @endcode
   *
   * @return array
   *   The location field options available by entity type and bundle.
   *   The "options" are compatible with form element "#options" attribute.
   */
  protected function getEntityLocationFieldOptions(): array {
    $locationOpts = [];

    foreach ($this->getEntityLocationFields() as $entityTypeId => $bundles) {
      $entityType = $this->entityTypeManager->getDefinition($entityTypeId);

      // Entity types that don't define separate bundles.
      // Set the labels at the entity type level (instead of by bundle).
      if (!$entityType->hasKey('bundle')) {
        $options = [
          'label' => $entityType->getLabel(),
          'options' => ['' => $this->t('- none -')],
        ];

        $fieldDefs = $this->entityFieldManager->getFieldDefinitions($entityTypeId, $entityTypeId);
        foreach ($bundles[$entityTypeId] as $fieldName) {
          $options['options'][$fieldName] = isset($fieldDefs[$fieldName])
            ? $fieldDefs[$fieldName]->getLabel()
            : $fieldName;
        }
        $locationOpts[$entityTypeId][$entityTypeId] = $options;
        continue;
      }

      // Field options by entity type bundles.
      $bundleInfo = $this->entityBundleInfo->getBundleInfo($entityTypeId);
      foreach ($bundles as $bundleId => $bundle) {
        $label = new FormattableMarkup('@entity_type: @bundle', [
          '@entity_type' => $entityType->getLabel(),
          '@bundle' => isset($bundleInfo[$bundleId]) ? $bundleInfo[$bundleId]['label'] : $bundleId,
        ]);
        $options = [
          'label' => $label,
          'options' => ['' => $this->t('- none -')],
        ];

        $fieldDefs = $this->entityFieldManager->getFieldDefinitions($entityTypeId, $bundleId);
        foreach ($bundle as $fieldName) {
          $options['options'][$fieldName] = isset($fieldDefs[$fieldName])
            ? $fieldDefs[$fieldName]->getLabel()
            : $fieldName;
        }

        $locationOpts[$entityTypeId][$bundleId] = $options;
      }
    }

    return $locationOpts;
  }

}
