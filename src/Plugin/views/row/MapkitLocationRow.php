<?php

namespace Drupal\mapkit\Plugin\views\Row;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\row\Fields;
use Drupal\views\ResultRow;

/**
 * Display a mapkit location view result row plugin.
 *
 * @ingroup views_row_plugins
 *
 * @ViewsRow(
 *   id = "mapkit_location_row",
 *   title = @Translation("Mapkit location"),
 *   help = @Translation("Displays the fields with location and map display options."),
 *   theme = "views_view_mapkit_location_row",
 *   theme_file = "mapkit.views.theme",
 *   display_types = {
 *     "normal",
 *   }
 * )
 */
class MapkitLocationRow extends Fields {

  /**
   * Fetch the entity that this views row is rendering.
   *
   * @param \Drupal\views\ResultRow $row
   *   A result row object to extract an ID from.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The entity associated with this views row, or NULL if there is no entity.
   */
  public function getEntity(ResultRow $row): ?EntityInterface {
    $entity = NULL;

    if (!empty($row->_object)) {
      $entity = $row->_object->getValue();
    }
    elseif (!empty($row->_entity)) {
      $entity = $row->_entity;
    }

    if ($entity instanceof EntityInterface) {
      return $entity;
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    // Options for determine which fields to display location information.
    $options['location_fields'] = [
      'default' => [],
    ];

    // Display computed distances.
    $options['display_distance'] = [
      'default' => TRUE,
    ];

    // Display the map marker with each result row.
    $options['display_marker'] = [
      'default' => TRUE,
    ];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['location_fields'] = [
      '#type' => 'select',
      '#title' => $this->t('Select fields to use for display information'),
      '#options' => $this->displayHandler->getFieldLabels(),
      '#multiple' => TRUE,
      '#default_value' => $this->options['location_fields'],
    ];

    $form['display_distance'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show calculated distances'),
      '#default_value' => $this->options['display_distance'],
    ];

    $form['display_marker'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display map marker with result row'),
      '#default_value' => $this->options['display_marker'],
    ];
  }

}
