<?php

namespace Drupal\mapkit\Plugin\views\filter;

use Drupal\Component\Plugin\Exception\ContextException;
use Drupal\Component\Plugin\Exception\MissingValueContextException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\Context\ContextHandlerInterface;
use Drupal\Core\Plugin\Context\ContextRepositoryInterface;
use Drupal\Core\Plugin\ContextAwarePluginInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\mapkit\Exception\InvalidLocationException;
use Drupal\mapkit\Plugin\LocationInputInterface;
use Drupal\mapkit\Plugin\LocationInputPluginManager;
use Drupal\mapkit\Plugin\LocationResolverInterface;
use Drupal\mapkit\Plugin\LocationResolverPluginManager;
use Drupal\mapkit\Plugin\views\SearchApiProximityTrait;
use Drupal\search_api\Plugin\views\filter\SearchApiFilterTrait;
use Drupal\views\Plugin\views\filter\FilterPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The location proximity views plugin filter Search API handler.
 *
 * @ViewsFilter("mapkit_location_filter")
 */
class MapkitLocationFilter extends FilterPluginBase implements ContainerFactoryPluginInterface {

  use SearchApiFilterTrait;
  use SearchApiProximityTrait;

  /**
   * Location resolution plugin manager.
   *
   * @var \Drupal\mapkit\Plugin\LocationResolverPluginManager
   */
  protected LocationResolverPluginManager $resolverManager;

  /**
   * Location input plugin manager.
   *
   * @var \Drupal\mapkit\Plugin\LocationInputPluginManager
   */
  protected LocationInputPluginManager $inputManager;

  /**
   * The context repository.
   *
   * @var \Drupal\Core\Plugin\Context\ContextRepositoryInterface
   */
  protected ContextRepositoryInterface $contextRepo;

  /**
   * The context handler.
   *
   * @var \Drupal\Core\Plugin\Context\ContextHandlerInterface
   */
  protected ContextHandlerInterface $contextHandler;

  /**
   * The location input plugin configured for this filter.
   *
   * @var \Drupal\mapkit\Plugin\LocationInputInterface|null
   */
  protected ?LocationInputInterface $inputPlugin;

  /**
   * The location resolver plugin configured for this filter.
   *
   * @var \Drupal\mapkit\Plugin\LocationResolverInterface|null
   */
  protected ?LocationResolverInterface $resolverPlugin;

  /**
   * Create a new instance of the MapkitLocationFilter Views filter plugin.
   *
   * @param array $configuration
   *   The views filter plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition from the Views data.
   * @param \Drupal\mapkit\Plugin\LocationResolverPluginManager $location_resolver_manager
   *   The location resolver plugin manager.
   * @param \Drupal\mapkit\Plugin\LocationInputPluginManager $location_input_manager
   *   The location input plugin manager.
   * @param \Drupal\Core\Plugin\Context\ContextRepositoryInterface $context_repository
   *   The context repository.
   * @param \Drupal\Core\Plugin\Context\ContextHandlerInterface $context_handler
   *   The context handler.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LocationResolverPluginManager $location_resolver_manager, LocationInputPluginManager $location_input_manager, ContextRepositoryInterface $context_repository, ContextHandlerInterface $context_handler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->resolverManager = $location_resolver_manager;
    $this->inputManager = $location_input_manager;
    $this->contextHandler = $context_handler;
    $this->contextRepo = $context_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.mapkit.location_resolver'),
      $container->get('plugin.manager.mapkit.location_input'),
      $container->get('context.repository'),
      $container->get('context.handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function adminSummary() {
    return $this->operator . ' ' . $this->options['input']['plugin'];
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['operator'] = ['default' => '<'];
    $options['require_valid'] = ['default' => FALSE];

    // By default use the NULL location resolver.
    $options['resolver'] = [
      'default' => [
        'plugin' => 'null_resolver',
        'config' => [],
      ],
    ];

    // By default setup the basic text entry location input plugin.
    $options['input'] = [
      'default' => [
        'plugin' => 'textfield',
        'config' => [],
      ],
    ];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function hasExtraOptions() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildExtraOptionsForm(&$form, FormStateInterface $form_state) {
    $values = $form_state->getValues()['options'] ?? $this->options;
    $contexts = $this->contextRepo->getAvailableContexts();
    $form_state->setTemporaryValue('gathered_contexts', $contexts);

    $form['require_valid'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Is a valid location required to execute a search'),
      '#default_value' => $this->options['require_valid'],
      '#description' => $this->t('Empty location values are allowed, but when provided, if the locations are invalid, should that prevent the query from executing?'),
    ];

    // Create a wrapper prefix for this view filter plugin instance.
    $wrapperPrefix = implode('-', [$this->view->id(), $this->options['id']]);
    $wrapperPrefix = strtr($wrapperPrefix, '_', '-');

    $plugins = [
      'input' => $this->inputManager,
      'resolver' => $this->resolverManager,
    ];

    foreach ($plugins as $pluginType => $pluginManager) {
      $wrapperId = "{$wrapperPrefix}-{$pluginType}-wrapper";
      $form[$pluginType] = [
        '#type' => 'fieldset',
        '#tree' => TRUE,

        'plugin' => [
          '#type' => 'select',
          '#required' => TRUE,
          '#default_value' => $values[$pluginType]['plugin'],
          '#ajax' => [
            'callback' => static::class . '::updatePluginConfigAjax',
            'wrapper' => $wrapperId,
          ],
        ],
        'config' => [
          '#prefix' => '<div id="' . $wrapperId . '">',
          '#suffix' => '</div>',
        ],
      ];

      $pluginId = $values[$pluginType]['plugin'] ?? NULL;
      if ($pluginId) {
        $config = $values[$pluginType]['config'] ?? [];

        $plugin = $pluginManager->createInstance($pluginId, $config);
        if ($plugin instanceof PluginFormInterface) {
          $subForm = &$form[$pluginType]['config'] ?? [];
          $subForm['#parents'] = ['options', 'resolver', 'config'];
          $subForm['#array_parents'] = $subForm['#parents'];

          $subFormState = SubformState::createForSubform($subForm, $form, $form_state);
          $form[$pluginType]['config'] = $plugin->buildConfigurationForm($subForm, $subFormState);
        }
      }
      else {
        $form[$pluginType]['config'] += [
          '#type' => 'value',
          '#value' => [],
        ];
      }
    }

    // Set labels and options for input plugin.
    $form['input']['#title'] = $this->t('Geolocation input');
    $form['input']['plugin']['#title'] = $this->t('Input type');
    $form['input']['plugin']['#options'] = $this->inputManager->getInputOptions();

    // Set labels and options for resolver plugin.
    $form['resolver']['#title'] = $this->t('Provide default location values');
    $form['resolver']['plugin']['#title'] = $this->t('Resolution type');
    $form['resolver']['plugin']['#options'] = $this->resolverManager->getResolverOptions();
  }

  /**
   * {@inheritdoc}
   */
  public function validateExtraOptionsForm($form, FormStateInterface $form_state) {
    $plugins = [
      'input' => $this->inputManager,
      'resolver' => $this->resolverManager,
    ];

    foreach ($plugins as $pluginType => $pluginManager) {
      $values = $form_state->getValue(['options', $pluginType]);

      $plugin = $pluginManager->createInstance($values['plugin'], $values['config'] ?? []);
      if ($plugin instanceof PluginFormInterface) {
        $subform = &$form[$pluginType]['config'];
        $subFormState = SubformState::createForSubform($subform, $form_state->getCompleteForm(), $form_state);
        $plugin->validateConfigurationForm($subform, $subFormState);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitExtraOptionsForm($form, FormStateInterface $form_state) {
    $plugins = [
      'input' => $this->inputManager,
      'resolver' => $this->resolverManager,
    ];

    foreach ($plugins as $pluginType => $pluginManager) {
      $values = $form_state->getValue(['options', $pluginType]);

      /** @var \Drupal\Component\Plugin\PluginInspectionInterface&\Drupal\Component\Plugin\ConfigurableInterface $plugin */
      $plugin = $pluginManager->createInstance($values['plugin'], $values['config'] ?? []);
      if ($plugin instanceof PluginFormInterface) {
        $subform = &$form[$pluginType]['config'];
        $subFormState = SubformState::createForSubform($subform, $form_state->getCompleteForm(), $form_state);
        $plugin->submitConfigurationForm($subform, $subFormState);
      }

      // Ensure that the plugin values are set based on the processed plugin
      // values and configurations. Some plugins could have massaged the raw
      // form values in the submitConfigurationForm() method and needs these
      // values to get applied.
      $form_state->setValue($form[$pluginType]['#parents'], [
        'plugin' => $plugin->getPluginId(),
        'config' => $plugin->getConfiguration(),
      ]);
    }

    // Generally these should not be initialized during a settings change,
    // but it doesn't hurt to ensure they are reset after options are updated.
    $this->inputPlugin = NULL;
    $this->resolverPlugin = NULL;
  }

  /**
   * {@inheritdoc}
   */
  protected function valueForm(&$form, FormStateInterface $form_state) {
    try {
      if ($inputPlugin = $this->getInputPlugin()) {
        $resolver = $this->getResolverPlugin();

        $form['value'] = [];
        $inputPlugin->buildInputElements($this->value, $form['value'], $form_state, $resolver);
      }
    }
    catch (PluginNotFoundException $e) {
    }
  }

  /**
   * {@inheritdoc}
   */
  public function acceptExposedInput($input) {
    if ($this->options['exposed'] && !empty($this->options['expose']['identifier'])) {
      $idKey = $this->options['expose']['identifier'];

      if (!empty($input[$idKey])) {
        $inputPlugin = $this->getInputPlugin();

        if ($inputPlugin && $inputPlugin->acceptInput($input[$idKey])) {
          $this->value = $input[$idKey];
          return TRUE;
        }
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $inputPlugin = $this->getInputPlugin();
    if (!$inputPlugin || !$inputPlugin->acceptInput($this->value)) {
      return;
    }

    try {
      // If there is valid lat/lng search location? Execute search if there was.
      if ($loc = $inputPlugin->processInput($this->value)) {
        $query = $this->getQuery();
        $this->addProximitySearch($query, $this->realField, $loc, $loc['radius'] ?? 80);
      }
    }
    catch (InvalidLocationException $e) {
      if (!empty($this->options['require_valid'])) {
        $this
          ->getQuery()
          ->abort($this->t('The location "@input" could not be resolved.', [
            '@input' => $e->getInvalidLocation(),
          ]));
      }
      else {
        $this
          ->messenger()
          ->addWarning($this->t('The location "@input" could not be resolved and was ignored.', [
            '@input' => $e->getInvalidLocation(),
          ]));
      }
    }
  }

  /**
   * The plugin AJAX form callback to refresh plugin configuration elements.
   *
   * @param array $form
   *   A reference to the full form elements.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state and build information.
   *
   * @return array
   *   Render elements to replace the plugin configuration elements.
   */
  public static function updatePluginConfigAjax(array &$form, FormStateInterface $form_state): array {
    $trigger = $form_state->getTriggeringElement();
    $parents = $trigger['#array_parents'];
    array_pop($parents);
    $parents[] = 'config';

    return NestedArray::getValue($form, $parents);
  }

  /**
   * Get the currently configure location plugin.
   *
   * @return \Drupal\mapkit\Plugin\LocationInputInterface|null
   *   Get the loaded location input plugin.
   */
  protected function getInputPlugin(): ?LocationInputInterface {
    if (!isset($this->inputPlugin) && !empty($this->options['input']['plugin'])) {
      $inputDef = $this->options['input'];
      $this->inputPlugin = $this->inputManager->createInstance($inputDef['plugin'], $inputDef['config'] ?? []);
    }

    return $this->inputPlugin;
  }

  /**
   * Get the currently configured location resolver plugin.
   *
   * @return \Drupal\mapkit\Plugin\LocationResolverInterface|null
   *   The location resolver plugin configured if the plugin could be
   *   loaded and has all th required contexts.
   */
  protected function getResolverPlugin(): ?LocationResolverInterface {
    if (!isset($this->resolverPlugin) && !empty($this->options['resolver']['plugin'])) {
      try {
        $resolverDef = $this->options['resolver'];
        $plugin = $this->resolverManager->createInstance($resolverDef['plugin'], $resolverDef['config'] ?? []);

        // If this resolver has contexts, apply the context values.
        if ($plugin instanceof ContextAwarePluginInterface) {
          $contexts = $this->contextRepo->getRuntimeContexts($plugin->getContextMapping());
          $this->contextHandler->applyContextMapping($plugin, $contexts);
        }
        $this->resolverPlugin = $plugin;
      }
      catch (MissingValueContextException $e) {
        // We're missing a required context, we can treat this resolver like
        // it doesn't apply and just not ignore it.
        $this->resolverPlugin = NULL;
      }
      catch (ContextException $e) {
        watchdog_exception('mapkit', $e);
        $this->resolverPlugin = NULL;
      }
    }

    return $this->resolverPlugin ?: NULL;
  }

}
