<?php

namespace Drupal\mapkit\Plugin\views;

use Drupal\search_api\Plugin\views\query\SearchApiQuery;

/**
 * Trait for views handlers which apply a geolocation filter to a Search index.
 *
 * Provides common methods used by filters and arguments that want to add
 * a geolocation filter to a Search API query. Will do the work of consolidating
 * the filters if multiple are attempted to the same index field.
 */
trait SearchApiProximityTrait {

  /**
   * Get the list of operators that pertain to proximity searches.
   *
   * The array keys are the "operators" and the array values are the operator
   * labels. This return value is compatible for use with a "select" or "radio"
   * form elements options.
   *
   * @return array
   *   The available operators for proximity searches.
   */
  public function operatorOptions() {
    return [
      '<' => $this->t('less than'),
      'between' => $this->t('between'),
      '>' => $this->t('more than'),
    ];
  }

  /**
   * Add a spatial field search criteria to the Search API query.
   *
   * @param \Drupal\search_api\Plugin\views\query\SearchApiQuery $query
   *   Current spatial search condition to add the field proximity values to.
   * @param string $field
   *   The ID of the field to search on.
   * @param array $value
   *   The lat/lng value to run the spatial search from.
   * @param float $radius
   *   The radius to limit the proximity search range to.
   */
  protected function addProximitySearch(SearchApiQuery $query, string $field, array $value, $radius): void {
    $cond = [
      'field' => $field,
      'lat' => $value['lat'],
      'lon' => $value['lng'],
      // Round the radius value because some backends will warn about truncating
      // the value to an integer and this precision is reasonable.
      // Search API Solr is an example of this, and this avoids the warning.
      'radius' => intval($radius + 0.5),
    ];

    $filters = $query->getOption('search_api_location', []);
    // Look for existing proximity filter on this field.
    foreach ($filters as $key => $search) {
      if ($search['field'] === $field) {
        $filters[$key] = $cond + $search;
        $query->setOption('search_api_location', $filters);
        return;
      }
    }

    // Create a new location field condition with the proximity info.
    $filters[] = $cond;
    $query->setOption('search_api_location', $filters);
  }

}
