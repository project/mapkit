<?php

namespace Drupal\mapkit\Plugin\views\style;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Form\SubformStateInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\mapkit\GeoParser\GeoParserManagerInterface;
use Drupal\mapkit\Plugin\MapProviderManagerInterface;
use Drupal\mapkit\Plugin\MarkerPluginManagerInterface;
use Drupal\search_api\Plugin\views\query\SearchApiQuery;
use Drupal\views\Plugin\views\style\StylePluginBase;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Style plugin to render locations with proximity and optional map.
 *
 * Renders a list of views location rows with the ability to add and configure
 * an interactive map.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "mapkit_location_style",
 *   title = @Translation("Mapkit geolocation results"),
 *   help = @Translation("Display geolocation results with proximity and optionally a map"),
 *   theme = "views_view_mapkit_location_style",
 *   theme_file = "mapkit.views.theme",
 *   display_types = {
 *     "normal",
 *   }
 * )
 */
class MapkitLocationStyle extends StylePluginBase {

  use LoggerChannelTrait;

  /**
   * {@inheritdoc}
   */
  protected $usesRowPlugin = TRUE;

  /**
   * {@inheritdoc}
   */
  protected $usesRowClass = TRUE;

  /**
   * {@inheritdoc}
   */
  protected $usesGrouping = FALSE;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The map provider plugin manager.
   *
   * @var \Drupal\mapkit\Plugin\MapProviderManagerInterface
   */
  protected $mapProviderManager;

  /**
   * The map marker plugin manager.
   *
   * @var \Drupal\mapkit\Plugin\MarkerPluginInterface
   */
  protected $markerPluginManager;

  /**
   * The geo-parser plugin manager.
   *
   * @var \Drupal\mapkit\GeoParser\GeoParserManagerInterface
   */
  protected $geoParserManager;

  /**
   * Creates a new instance of the MapkitStyle view style plugin.
   *
   * @param array $configuration
   *   The view style pluign configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\mapkit\Plugin\MapProviderManagerInterface $map_provider_manager
   *   The map provider plugin manager.
   * @param \Drupal\mapkit\Plugin\MarkerPluginManagerInterface $marker_plugin_manager
   *   The marker plugin manager.
   * @param \Drupal\mapkit\GeoParser\GeoParserManagerInterface $geo_parser_manager
   *   The geo-parser plugin manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, MapProviderManagerInterface $map_provider_manager, MarkerPluginManagerInterface $marker_plugin_manager, GeoParserManagerInterface $geo_parser_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
    $this->mapProviderManager = $map_provider_manager;
    $this->markerPluginManager = $marker_plugin_manager;
    $this->geoParserManager = $geo_parser_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.mapkit.map_provider'),
      $container->get('plugin.manager.mapkit.marker'),
      $container->get('strategy.manager.mapkit.geo_parser')
    );
  }

  /**
   * Gets the drupalSettings datakey to place the JS settings.
   *
   * @return string
   *   The datakey to use for placing the plugin's JS settings data.
   */
  public function getJsSettingsKey() {
    return !empty($this->view->dom_id)
      ? 'js-view-dom-id-' . $this->view->dom_id
      : 'view-id-' . Html::cleanCssIdentifier($this->view->id());
  }

  /**
   * Set the array key to place the location JS data.
   *
   * @param \Drupal\views\ResultRow $row
   *   A result row object to extract an ID from.
   *
   * @return string|null
   *   A string representing the row identifier.
   */
  public function getRowId(ResultRow $row) {
    $entity = $this->getEntity($row);

    if ($entity instanceof EntityInterface) {
      return $entity->getEntityTypeId() . '/' . $entity->id();
    }
    elseif (!empty($row->_item)) {
      return $row->_item->getId();
    }

    return $row->index;
  }

  /**
   * Fetch the entity that this views row is rendering.
   *
   * @param \Drupal\views\ResultRow $row
   *   A result row object to extract an ID from.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The entity associated with this views row, or NULL if there is no entity.
   */
  public function getEntity(ResultRow $row): ?EntityInterface {
    $entity = NULL;

    if (!empty($row->_object)) {
      $entity = $row->_object->getValue();

      if ($entity instanceof EntityInterface) {
        return $entity;
      }
    }
    elseif (!empty($row->_entity)) {
      return $row->_entity;
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    // Options for proximity calculations.
    $options['locations'] = [
      'default' => [
        'latlng_fields' => [],
        'units' => 'mi',
      ],
    ];

    // Enable map display toggle.
    $options['map_enabled'] = [
      'default' => TRUE,
    ];

    // Default to using "gmap" map plugin.
    $options['map'] = [
      'default' => [
        'width' => '100%',
        'height' => '500px',
        'plugin' => 'gmap',
        'config' => [],
      ],
    ];

    // Use the default marker set which is compatible with "gmap".
    $options['markers'] = [
      'default' => [
        'set' => 'default',
        'config' => [],
      ],
    ];

    return $options;
  }

  /**
   * Get labels for field views which can be used for the lat/long fields.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup[]|string[]
   *   Labels for views fields which can be used as the geo coordinate of the
   *   the views result row. Labels are keyed by the field view ID.
   */
  protected function getLatLngFieldLabels() {
    foreach ($this->displayHandler->getHandlers('relationship') as $relationship => $handler) {
      $relationships[$relationship] = $handler->adminLabel();
    }

    $options = [];
    /** @var \Drupal\views\Plugin\views\field\FieldHandlerInterface $handler */
    foreach ($this->displayHandler->getHandlers('field') as $id => $handler) {
      if ($this->geoParserManager->getViewFieldParser($handler)) {
        // @todo create filters fields are valid for storing lat/long data.
        $options[$id] = $handler->label() ?: $handler->adminLabel();

        if (!empty($handler->options['relationship']) && !empty($relationships[$handler->options['relationship']])) {
          $options[$id] = '(' . $relationships[$handler->options['relationship']] . ') ' . $options[$id];
        }
      }
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $options = $form_state->getValue(['style_options']) ?: $this->options;
    $mapPluginId = $options['map']['plugin'];
    $mapConfig = $options['map']['config'] ?? [];

    // Determine which fields are used for location displays and lat/long.
    $form['locations'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Location settings'),

      'latlng_fields' => [
        '#type' => 'select',
        '#title' => $this->t('Select Latitude/Longitude fields to use for location data'),
        '#options' => $this->getLatLngFieldLabels(),
        '#required' => TRUE,
        '#multiple' => TRUE,
        '#default_value' => $options['locations']['latlng_fields'],
      ],
      'units' => [
        '#type' => 'radios',
        '#title' => $this->t('Distance units'),
        '#options' => [
          'km' => $this->t('kilometers'),
          'mi' => $this->t('miles'),
        ],
        '#default_value' => $this->options['locations']['units'],
      ],
    ];

    // Map settings.
    $form['map_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include a map display of locations'),
      '#default_value' => $options['map_enabled'],
    ];

    $form['map'] = [
      '#type' => 'details',
      '#title' => $this->t('Map Settings'),
      '#tree' => TRUE,

      'plugin' => [
        '#type' => 'select',
        '#title' => $this->t('Map Type'),
        '#options' => [
          'gmap' => $this->t('Google Maps'),
        ],
        '#disabled' => TRUE,
        '#value' => $mapPluginId,
      ],
      'width' => [
        '#type' => 'textfield',
        '#title' => $this->t('Map width'),
        '#required' => TRUE,
        '#default_value' => $options['map']['width'],
        '#pattern' => '\d+\s*(px|%|em|rem)',
      ],
      'height' => [
        '#type' => 'textfield',
        '#title' => $this->t('Map height'),
        '#required' => TRUE,
        '#default_value' => $options['map']['height'],
        '#pattern' => '\d+\s*(px|em|rem)',
      ],
      'config' => [
        '#plugin' => $this->mapProviderManager->createInstance($mapPluginId, $mapConfig),
        '#process' => [static::class . '::buildPluginFormProcess'],
      ],
    ];

    $markerSets = $this->entityTypeManager
      ->getStorage('mapkit_marker_set')
      ->loadByProperties(['map_type' => $mapPluginId]);

    $markerOpts = [];
    foreach ($markerSets as $set) {
      $markerOpts[$set->id()] = $set->label();
    }

    $form['markers'] = [
      '#type' => 'details',
      '#title' => $this->t('Map Marker Settings'),
      '#tree' => TRUE,

      'set' => [
        '#type' => 'select',
        '#title' => $this->t('Marker set'),
        '#options' => $markerOpts,
        '#default_value' => $this->options['markers']['set'],
      ],
    ];
  }

  /**
   * Form element process callback to expand plugin configuration elements.
   *
   * Build the configuration form after 'array_parents' and 'parents' element
   * properties have been created. This way the subform can find the correct
   * form state values to work with.
   *
   * @param array $elements
   *   The config form elements being processed.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param array $complete_form
   *   The complete form structure and elements.
   */
  public static function buildPluginFormProcess(array $elements, FormStateInterface $form_state, array &$complete_form) {
    $plugin = $elements['#plugin'] ?? NULL;

    if ($plugin && $plugin instanceof PluginFormInterface) {
      $subformState = SubformState::createForSubform($elements, $complete_form, $form_state);
      $elements += $plugin->buildConfigurationForm($elements, $subformState);
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function validateOptionsForm(&$form, FormStateInterface $form_state) {
    parent::validateOptionsForm($form, $form_state);

    $completeForm = &$form_state->getCompleteForm();
    $completeState = $form_state instanceof SubformStateInterface
      ? $form_state->getCompleteFormState() : $form_state;

    // Validate map settings through the plugin.
    $elements = &$form['map']['config'];
    $plugin = $elements['#plugin'] ?? NULL;

    if ($plugin && $plugin instanceof PluginFormInterface) {
      $subformState = SubformState::createForSubform($elements, $completeForm, $completeState);
      $plugin->validateConfigurationForm($elements, $subformState);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitOptionsForm(&$form, FormStateInterface $form_state) {
    parent::submitOptionsForm($form, $form_state);

    $completeForm = &$form_state->getCompleteForm();
    $completeState = $form_state instanceof SubformStateInterface
      ? $form_state->getCompleteFormState() : $form_state;

    // Allow the map and marker plugin to process configuration form values.
    $elements = &$form['map']['config'];
    /** @var \Drupal\mapkit\Plugin\MapProviderInterface $plugin */
    $plugin = $elements['#plugin'] ?? NULL;

    if ($plugin && $plugin instanceof PluginFormInterface) {
      $subformState = SubformState::createForSubform($elements, $completeForm, $completeState);
      $plugin->submitConfigurationForm($elements, $subformState);
      $form_state->setValue($elements['#parents'], $plugin->getConfiguration());
    }
  }

  /**
   * AJAX form callback to update the plugin configuration form.
   *
   * @param array $form
   *   The complete configuration for, which contains this plugin subform.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The complete form state including the information about the AJAX form
   *   triggering element.
   *
   * @return array
   *   Render element replacement for the plugin configuration subform.
   */
  public function updatePluginConfigAjax(array &$form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    $parents = $trigger['#array_parents'];
    array_splice($parents, -1, 1, 'config');

    return NestedArray::getValue($form, $parents);
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = parent::render();

    // Ensure JS settings reference for map instance and locations to be added.
    $jsDataKey = $this->getJsSettingsKey();
    if (!isset($build['#attached']['drupalSettings']['mapkitViews']['instance'][$jsDataKey])) {
      $build['#attached']['drupalSettings']['mapkitViews']['instance'][$jsDataKey] = [];
    }
    $jsSettings = &$build['#attached']['drupalSettings']['mapkitViews']['instance'][$jsDataKey];

    // The views ID and display are used on the clientside to search for any
    // event listeners to apply. See module README.md on instructions on adding
    // customizing initialization and map listeners.
    $jsSettings['viewId'] = $this->view->id();
    $jsSettings['displayId'] = $this->view->current_display;

    // If map is not enabled, render() has nothing to add and can exit.
    if ($this->options['map_enabled']) {
      try {
        // Get the map provider plugin.
        $mapPluginId = $this->options['map']['plugin'];
        $mapConfig = $this->options['map']['config'] ?? [];
        $mapPlugin = $this->mapProviderManager->createInstance($mapPluginId, $mapConfig);

        // Ensure the mapping libraries and settings are included.
        $build['#attached']['library'][] = 'mapkit/views';
        $build['#attached']['library'] = array_merge(
          $build['#attached']['library'],
          $mapPlugin->getLibraries(),
        );

        $jsSettings['mapWidth'] = $this->options['map']['width'] ?? '100%';
        $jsSettings['mapHeight'] = $this->options['map']['height'] ?? '500px';
        $jsSettings['map'] = $mapPlugin->getJsSettings();
        $jsSettings['map']['type'] = $mapPlugin->getPluginId();

        /** @var \Drupal\mapkit\Entity\MapkitMarkerSet $markerset */
        $markerset = $this->entityTypeManager
          ->getStorage('mapkit_marker_set')
          ->load($this->options['markers']['set']);

        if ($markerset && ($markerPlugin = $markerset->getPlugin())) {
          $build['#attached']['library'] = array_merge(
            $build['#attached']['library'],
            $markerPlugin->getLibraries($mapPlugin)
          );
          $jsSettings['markers'] = $markerPlugin->getJsSettings($mapPlugin);
        }
      }
      catch (PluginNotFoundException $e) {
        // Missing marker or map handlers, skip rendering the markers.
        $this->getLogger('mapkit')->error('Failed to load mapkit resource for @name @display with: @message', [
          '@name' => $this->view->id(),
          '@display' => $this->view->current_display,
          '@message' => $e->getMessage(),
        ]);
      }
    }

    // If a Search API proximity search was performed, get the location search
    // query center and radius information. Allows us to center the map on the
    // original search location if relevant.
    $query = $this->view->query;
    if ($query instanceof SearchApiQuery && ($location = $query->getOption('search_api_location'))) {
      $location = array_pop($location);
      $jsSettings['refLocation'] = [
        'lat' => floatval($location['lat']),
        'lng' => floatval($location['lon']),
        'radius' => floatval($location['radius']),
      ];
      $jsSettings['units'] = $this->options['locations']['units'] ?? 'mi';
    }

    // Add the lat/long data for all locations of the result providers.
    $geoParsers = [];
    $fields = array_intersect_key($this->displayHandler->getHandlers('field'), $this->options['locations']['latlng_fields']);
    foreach ($this->view->result as $delta => $row) {
      $dataId = $this->getRowId($row);
      $coordinates = [];

      foreach ($fields as $fieldId => $field) {
        if (!isset($geoParsers[$fieldId])) {
          $geoParsers[$fieldId] = $this->geoParserManager->getViewFieldParser($field) ?: FALSE;
        }

        if ($geoParsers[$fieldId]) {
          if ($values = $geoParsers[$fieldId]->parseViewField($row, $field)) {
            // Coordinates (latlng values) can come from multiple fields, so
            // if there are already values, we need to merge in new values.
            $coordinates = empty($coordinates) ? $values : array_merge($coordinates, $values);
          }
        }

      }

      $build[0]['#rows'][$delta]['#data_id'] = $dataId;
      $jsSettings['locations'][$dataId] = $coordinates;
    }

    return $build;
  }

}
