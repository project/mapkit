<?php

namespace Drupal\mapkit\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines annotation for Mapkit Marker Sets.
 *
 * Plugin Namespace: Plugin\Mapkit\Marker.
 *
 * @see \Drupal\Component\Annotation\Plugin
 * @see \Drupal\mapkit\MarkerPluginInterface
 * @see \Drupal\mapkit\MarkerPluginManager
 *
 * @ingroup mapkit_marker
 *
 * @Annotation
 */
class MapkitMarker extends Plugin {

  /**
   * The plugin ID for the marker set.
   *
   * @var string
   */
  public $id;

  /**
   * Human friendly name the plugin description.
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $label;

  /**
   * Map types that are supported by this marker set.
   *
   * If empty then this marker set is available for all map providers.
   *
   * @var string[]
   */
  public $map_types = [];

}
