<?php

namespace Drupal\mapkit\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines annotation for Mapkit location input plugins.
 *
 * Plugin Namespace: Plugin\Mapkit.
 *
 * @see \Drupal\Component\Annotation\Plugin
 * @see \Drupal\mapkit\LocationInputInterface
 * @see \Drupal\mapkit\LocationInputPluginManager
 *
 * @ingroup mapkit_location_input
 *
 * @Annotation
 */
class MapkitLocationInput extends Plugin {

  /**
   * The plugin ID for the location input.
   *
   * @var string
   */
  public $id;

  /**
   * Human friendly name the location input plugin label.
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $label;

}
