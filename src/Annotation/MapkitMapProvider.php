<?php

namespace Drupal\mapkit\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines annotation for Mapkit Map provider plugins.
 *
 * Plugin Namespace: Plugin\Mapkit\MapProvider.
 *
 * @see \Drupal\Component\Annotation\Plugin
 * @see \Drupal\mapkit\MapProviderInterface
 * @see \Drupal\mapkit\MapProviderManager
 *
 * @ingroup mapkit_map_provider
 *
 * @Annotation
 */
class MapkitMapProvider extends Plugin {

  /**
   * The plugin ID for the map handler plugin.
   *
   * @var string
   */
  public $id;

  /**
   * Human friendly name the plugin description.
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $label;

  /**
   * Route name and parameters for the map provider configuration forms.
   *
   * If the map provider has a configuration form route provide the route info
   * here so the local task deriver can add this configuration page to the
   * local tasks for mapkit configurations.
   *
   * @var array
   */
  public $config_route = [];

}
