<?php

namespace Drupal\mapkit\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines annotation for Mapkit location resolver plugin.
 *
 * Plugin Namespace: Plugin\Mapkit.
 *
 * @see \Drupal\Component\Annotation\Plugin
 * @see \Drupal\mapkit\LocationResolverInterface
 * @see \Drupal\mapkit\LocationResolverPluginManager
 *
 * @ingroup mapkit_location_resolver
 *
 * @Annotation
 */
class MapkitLocationResolver extends Plugin {

  /**
   * The plugin ID for the location resolver.
   *
   * @var string
   */
  public $id;

  /**
   * Human friendly name the plugin description.
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $label;

}
