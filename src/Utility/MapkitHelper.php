<?php

namespace Drupal\mapkit\Utility;

/**
 * A class with some helper functions for geographical bounds and points.
 */
final class MapkitHelper {

  /**
   * REGEX pattern for extracting latitude and longitude values from a string.
   */
  const LATLNG_REGEX = '/^([+-]?[0-9]+(?:\.[0-9]+)?)\s*,\s*([+-]?[0-9]+(?:\.[0-9]+)?)$/';

  /**
   * The information about available measurement units relative to kilometers.
   *
   * @var array
   */
  protected static array $unitInfo = [
    'km' => [
      'label' => 'Kilometers',
      'multiplier' => 1,
      'abbreviation' => 'km',
    ],
    'mi' => [
      'label' => 'Miles',
      'multiplier' => 0.621371,
      'abbreviation' => 'mi',
    ],
  ];

  /**
   * Get information about the supported measurement units for distances.
   *
   * @param string $property
   *   The array key to fetch from the unit information.
   *
   * @return array
   *   If not property key is provided, return all the measurement unit
   *   information. If property was provided, return an array of just those
   *   measurement unit property values.
   */
  public static function getDistanceUnits(string $property = NULL): array {
    if ($property) {
      $values = [];
      foreach (static::$unitInfo as $key => $props) {
        $values[$key] = $props[$property];
      }

      return $values;
    }

    return static::$unitInfo;
  }

  /**
   * Convert a value from a different unit of measurement to kilometers.
   *
   * @param float $value
   *   The value to convert to kilometers.
   * @param string $units
   *   The measurement units of the current value.
   *
   * @return float
   *   The value converted to kilometers.
   *
   * @throws \InvalidArgumentException
   *   If the incoming unit of measure isn't known.
   */
  public static function toKilometers(float $value, string $units = 'mi'): float {
    if ('km' !== $units) {
      $convData = self::$unitInfo;

      if (!isset($convData[$units])) {
        throw new \InvalidArgumentException("Unsupported units of measurement '{$units}'");
      }

      // All the multiplier values are relative to kilometers.
      $value *= $convData[$units]['multiplier'];
    }

    return $value;
  }

  /**
   * Convert a value from a different unit of measurement to miles.
   *
   * @param float $value
   *   The value to convert to miles.
   * @param string $units
   *   The measurement units of the current value.
   *
   * @return float
   *   The value converted to miles.
   *
   * @throws \InvalidArgumentException
   *   If the incoming unit of measure isn't known.
   */
  public static function toMiles(float $value, string $units = 'km'): float {
    if ('mi' !== $units) {
      $convData = self::$unitInfo;

      if (!isset($convData[$units])) {
        throw new \InvalidArgumentException("Unsupported units of measurement '{$units}'");
      }

      $value *= $convData[$units]['multiplier'] / $convData['mi']['multiplier'];
    }
    return $value;
  }

  /**
   * Parse a lat/long string into float values array.
   *
   * The string format is a latitude and longitude separated by a comma. If the
   * value doesn't match this format "NULL" is returned. If this string is
   * 2 numbers which are comma separated but out of bounds for latitude and
   * longitude values, an exception is thrown.
   *
   * @param string $value
   *   The string value to parse into latitude and longitude values.
   *
   * @return array|null
   *   An array the latitude and longitude values from the string if they match
   *   the string format.
   *
   * @throws \InvalidArgumentException
   *   If the provided lat/long values are out of range, or invalid.
   */
  public static function parseLatLng(string $value): ?array {
    if (preg_match(self::LATLNG_REGEX, $value, $matches)) {
      $lat = floatval($matches[1]);
      $lng = floatval($matches[2]);

      // Ensure that the value is inbounds.
      if (90 < $lat || -90 > $lat) {
        throw new \InvalidArgumentException("Latitude value needs to be between -90 and 90 degrees. The value of {$matches[1]} is out of bounds.");
      }
      if (180 < $lng || -180 > $lng) {
        throw new \InvalidArgumentException("Longitude value needs to be between -180 and 180 degrees. The value of {$matches[2]} is out of bounds.");
      }

      return ['lat' => $lat, 'lng' => $lng];
    }

    return NULL;
  }

  /**
   * Checks if a latitude/longitude is within a bounds rectangle.
   *
   * @param array $pt
   *   An array with a 'lat' and 'lng' set of values.
   * @param array $bounds
   *   An array with a 'topLeft' and 'bottomRight' points which are
   *   both points (have a lat and lng), which outline a rectangular bounds.
   *
   * @return bool
   *   If the $pt is inside of the bounding rectangle coordinates.
   */
  public static function ptInBounds(array $pt, array $bounds) {
    return $pt['lat'] >= $bounds['topLeft']['lat']
      && $pt['lng'] >= $bounds['topLeft']['lng']
      && $pt['lat'] <= $bounds['bottomRight']['lat']
      && $pt['lng'] <= $bounds['bottomRight']['lng'];
  }

}
