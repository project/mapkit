<?php

namespace Drupal\mapkit\Controller;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\mapkit\Plugin\MapProviderManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The route controller for mapkit provider management routes.
 */
class MapProviderController implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The Mapkit map provider plugin manager.
   *
   * @var \Drupal\mapkit\Plugin\MapProviderManagerInterface
   */
  protected MapProviderManagerInterface $providerManager;

  /**
   * Create a new instance of the MapProviderController class.
   *
   * @param \Drupal\mapkit\Plugin\MapProviderManagerInterface $map_provider_manager
   *   The map provider plugin manager.
   */
  public function __construct(MapProviderManagerInterface $map_provider_manager) {
    $this->providerManager = $map_provider_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.mapkit.map_provider')
    );
  }

  /**
   * Generate the Mapkit map provider listing page.
   *
   * Page provides an overview of installed providers and allows provides
   * configurations to the provider configurations and libraries.
   *
   * @return array
   *   The renderable array of the map provider links.
   */
  public function providerList(): array {
    $build = [
      '#type' => 'table',
      '#header' => [
        $this->t('Map provider'),
        $this->t('Actions'),
      ],
      '#empty' => $this->t('No map providers are currently available. Map providers are made available by installing modules that implement a provider plugin.'),
    ];

    // Build table of the map providers and link to their configurations.
    foreach ($this->providerManager->getDefinitions() as $pid => $providerDef) {
      $build[$pid]['provider']['#markup'] = $providerDef['label'];
      $build[$pid]['actions'] = [];

      // Is there a configuration for this map provider.
      if (!empty($providerDef['config_route']['route_name'])) {
        $route = $providerDef['config_route'];

        $build[$pid]['actions']['#type'] = 'operations';
        $build[$pid]['actions']['#links']['configure'] = [
          'title' => $this->t('Configure'),
          'url' => Url::fromRoute($route['route_name'], $route['route_parameters'] ?? []),
        ];
      }
    }

    return $build;
  }

}
