"use strict";

(() => {
  /**
   *
   */
  class LoadQueue {
    constructor() {
      this.queue = [];
    }

    /**
     * Called when library dependencies are loaded and ready for use.
     *
     * This calls all queued initializers and changes the queue into add()
     * method so the methods immediately execute.
     */
    init() {
      // Run all queued functions, ensuring to remove them as we do, so there
      // are no references are kept after we're done with them here. This may
      // help with the JS garbage collector.
      let cb = this.queue.pop();
      while (cb) {
        const func = cb.shift();
        func(...cb);
        cb = this.queue.pop();
      }

      // Replaces the array.prototype.push function so that all future
      // push calls just execute the callback immediately.
      this.queue.push = ([func, ...args]) => func(...args);
    }

    /**
     * Add or execute a callback. If the provider dependencies are loaded, the
     * callback is called immediately (array.push), otherwise it is queue until
     * the this.init() method is called.
     *
     * This allows functionality that is dependent on provider libraries to
     * be used without having to worry about the load state of the libraries.
     *
     * @param {array|function} data
     *   Either a function to add to the callbacks queue or an array
     *   with the callback as the first item, and the function parameters
     *   in the array following the callback.
     */
    add(data) {
      if (typeof data === 'function') {
        this.queue.push([data]);
      } else if (Array.isArray(data) && typeof data[0] === 'function') {
        this.queue.push(data);
      } else {
        throw new Error('Method expects either a function or an array with a callback.');
      }
    }

    /**
     * Search for a callback init function to remove. This can will only remove
     * callbacks which are still pending. After they are run they are no longer
     * tracked and don't need to be removed anymore.
     *
     * @param {function} handler
     *   The callback handler to remove from initialization.
     */
    remove(handler) {
      // Go hunting for the callback to remove.
      for (let i = 0; i < this.queue.length; ++i) {
        const data = this.queue[i];
        if (data[0] === handler) {
          this.queue.splice(i, 1);
        }
      }
    }
  }

  /**
   * Maintains a queue of initialization functions for when the Mapkit
   * dependent libraries are loaded.
   *
   * Add initializer functions to the queue by calling "addInit()" and providing
   * the callback. The value pushed should be an array, with the first value
   * being a callable function, followed by each of the arguments for that
   * function, if there are any.
   *
   * EX: window.Mapkit.addInit('gmap', [func, arg1, arg2, arg3]);
   */
  window.Mapkit = {
    loader: {},
    /**
     * Creates an loader queue for a Mapkit provider.
     *
     * @param {string} id
     *   Idenitifier for the loader queue to create.
     */
    createQueue(id) {
      if (!this.loader[id]) {
        this.loader[id] = new LoadQueue();
      }
    },
    /**
     * Add or execute a callback. To the specified loader queue.
     *
     * Ensure that the queue has been created before adding the initializers
     * by calling "Mapkit.createQueue(id)".
     *
     * @param {string} id
     *   The loader queue to add the callback to.
     * @param {array|function} data
     *   Either a function to add to the callbacks queue or an array
     *   with the callback as the first item, and the function parameters
     *   in the array following the callback.
     */
    addInit(id, data) {
      this.loader[id].add(data);
    },
    /**
     * Search for an init function to remove from the specified loader queue.
     *
     * @param {string} id
     *   The loader queue to search for and remove this handler.
     * @param {function} handler
     *   The callback handler to remove from initialization.
     */
    removeInit(id, handler) {
      if (this.loader[id]) {
        this.laoder[id].remove(handler);
      }
    }
  };
})();
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9hZGVyLmpzIiwibmFtZXMiOlsiTG9hZFF1ZXVlIiwiY29uc3RydWN0b3IiLCJxdWV1ZSIsImluaXQiLCJjYiIsInBvcCIsImZ1bmMiLCJzaGlmdCIsInB1c2giLCJhcmdzIiwiYWRkIiwiZGF0YSIsIkFycmF5IiwiaXNBcnJheSIsIkVycm9yIiwicmVtb3ZlIiwiaGFuZGxlciIsImkiLCJsZW5ndGgiLCJzcGxpY2UiLCJ3aW5kb3ciLCJNYXBraXQiLCJsb2FkZXIiLCJjcmVhdGVRdWV1ZSIsImlkIiwiYWRkSW5pdCIsInJlbW92ZUluaXQiLCJsYW9kZXIiXSwic291cmNlcyI6WyJsb2FkZXIuZXM2LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIigoKSA9PiB7XG4gIC8qKlxuICAgKlxuICAgKi9cbiAgY2xhc3MgTG9hZFF1ZXVlIHtcbiAgICBjb25zdHJ1Y3RvcigpIHtcbiAgICAgIHRoaXMucXVldWUgPSBbXTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBDYWxsZWQgd2hlbiBsaWJyYXJ5IGRlcGVuZGVuY2llcyBhcmUgbG9hZGVkIGFuZCByZWFkeSBmb3IgdXNlLlxuICAgICAqXG4gICAgICogVGhpcyBjYWxscyBhbGwgcXVldWVkIGluaXRpYWxpemVycyBhbmQgY2hhbmdlcyB0aGUgcXVldWUgaW50byBhZGQoKVxuICAgICAqIG1ldGhvZCBzbyB0aGUgbWV0aG9kcyBpbW1lZGlhdGVseSBleGVjdXRlLlxuICAgICAqL1xuICAgIGluaXQoKSB7XG4gICAgICAvLyBSdW4gYWxsIHF1ZXVlZCBmdW5jdGlvbnMsIGVuc3VyaW5nIHRvIHJlbW92ZSB0aGVtIGFzIHdlIGRvLCBzbyB0aGVyZVxuICAgICAgLy8gYXJlIG5vIHJlZmVyZW5jZXMgYXJlIGtlcHQgYWZ0ZXIgd2UncmUgZG9uZSB3aXRoIHRoZW0gaGVyZS4gVGhpcyBtYXlcbiAgICAgIC8vIGhlbHAgd2l0aCB0aGUgSlMgZ2FyYmFnZSBjb2xsZWN0b3IuXG4gICAgICBsZXQgY2IgPSB0aGlzLnF1ZXVlLnBvcCgpO1xuICAgICAgd2hpbGUgKGNiKSB7XG4gICAgICAgIGNvbnN0IGZ1bmMgPSBjYi5zaGlmdCgpO1xuICAgICAgICBmdW5jKC4uLmNiKTtcbiAgICAgICAgY2IgPSB0aGlzLnF1ZXVlLnBvcCgpO1xuICAgICAgfVxuXG4gICAgICAvLyBSZXBsYWNlcyB0aGUgYXJyYXkucHJvdG90eXBlLnB1c2ggZnVuY3Rpb24gc28gdGhhdCBhbGwgZnV0dXJlXG4gICAgICAvLyBwdXNoIGNhbGxzIGp1c3QgZXhlY3V0ZSB0aGUgY2FsbGJhY2sgaW1tZWRpYXRlbHkuXG4gICAgICB0aGlzLnF1ZXVlLnB1c2ggPSAoW2Z1bmMsIC4uLmFyZ3NdKSA9PiBmdW5jKC4uLmFyZ3MpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEFkZCBvciBleGVjdXRlIGEgY2FsbGJhY2suIElmIHRoZSBwcm92aWRlciBkZXBlbmRlbmNpZXMgYXJlIGxvYWRlZCwgdGhlXG4gICAgICogY2FsbGJhY2sgaXMgY2FsbGVkIGltbWVkaWF0ZWx5IChhcnJheS5wdXNoKSwgb3RoZXJ3aXNlIGl0IGlzIHF1ZXVlIHVudGlsXG4gICAgICogdGhlIHRoaXMuaW5pdCgpIG1ldGhvZCBpcyBjYWxsZWQuXG4gICAgICpcbiAgICAgKiBUaGlzIGFsbG93cyBmdW5jdGlvbmFsaXR5IHRoYXQgaXMgZGVwZW5kZW50IG9uIHByb3ZpZGVyIGxpYnJhcmllcyB0b1xuICAgICAqIGJlIHVzZWQgd2l0aG91dCBoYXZpbmcgdG8gd29ycnkgYWJvdXQgdGhlIGxvYWQgc3RhdGUgb2YgdGhlIGxpYnJhcmllcy5cbiAgICAgKlxuICAgICAqIEBwYXJhbSB7YXJyYXl8ZnVuY3Rpb259IGRhdGFcbiAgICAgKiAgIEVpdGhlciBhIGZ1bmN0aW9uIHRvIGFkZCB0byB0aGUgY2FsbGJhY2tzIHF1ZXVlIG9yIGFuIGFycmF5XG4gICAgICogICB3aXRoIHRoZSBjYWxsYmFjayBhcyB0aGUgZmlyc3QgaXRlbSwgYW5kIHRoZSBmdW5jdGlvbiBwYXJhbWV0ZXJzXG4gICAgICogICBpbiB0aGUgYXJyYXkgZm9sbG93aW5nIHRoZSBjYWxsYmFjay5cbiAgICAgKi9cbiAgICBhZGQoZGF0YSkge1xuICAgICAgaWYgKHR5cGVvZiBkYXRhID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgIHRoaXMucXVldWUucHVzaChbZGF0YV0pO1xuICAgICAgfVxuICAgICAgZWxzZSBpZiAoQXJyYXkuaXNBcnJheShkYXRhKSAmJiB0eXBlb2YgZGF0YVswXSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICB0aGlzLnF1ZXVlLnB1c2goZGF0YSk7XG4gICAgICB9XG4gICAgICBlbHNlIHtcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdNZXRob2QgZXhwZWN0cyBlaXRoZXIgYSBmdW5jdGlvbiBvciBhbiBhcnJheSB3aXRoIGEgY2FsbGJhY2suJyk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogU2VhcmNoIGZvciBhIGNhbGxiYWNrIGluaXQgZnVuY3Rpb24gdG8gcmVtb3ZlLiBUaGlzIGNhbiB3aWxsIG9ubHkgcmVtb3ZlXG4gICAgICogY2FsbGJhY2tzIHdoaWNoIGFyZSBzdGlsbCBwZW5kaW5nLiBBZnRlciB0aGV5IGFyZSBydW4gdGhleSBhcmUgbm8gbG9uZ2VyXG4gICAgICogdHJhY2tlZCBhbmQgZG9uJ3QgbmVlZCB0byBiZSByZW1vdmVkIGFueW1vcmUuXG4gICAgICpcbiAgICAgKiBAcGFyYW0ge2Z1bmN0aW9ufSBoYW5kbGVyXG4gICAgICogICBUaGUgY2FsbGJhY2sgaGFuZGxlciB0byByZW1vdmUgZnJvbSBpbml0aWFsaXphdGlvbi5cbiAgICAgKi9cbiAgICByZW1vdmUoaGFuZGxlcikge1xuICAgICAgLy8gR28gaHVudGluZyBmb3IgdGhlIGNhbGxiYWNrIHRvIHJlbW92ZS5cbiAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5xdWV1ZS5sZW5ndGg7ICsraSkge1xuICAgICAgICBjb25zdCBkYXRhID0gdGhpcy5xdWV1ZVtpXTtcblxuICAgICAgICBpZiAoZGF0YVswXSA9PT0gaGFuZGxlcikge1xuICAgICAgICAgIHRoaXMucXVldWUuc3BsaWNlKGksIDEpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIE1haW50YWlucyBhIHF1ZXVlIG9mIGluaXRpYWxpemF0aW9uIGZ1bmN0aW9ucyBmb3Igd2hlbiB0aGUgTWFwa2l0XG4gICAqIGRlcGVuZGVudCBsaWJyYXJpZXMgYXJlIGxvYWRlZC5cbiAgICpcbiAgICogQWRkIGluaXRpYWxpemVyIGZ1bmN0aW9ucyB0byB0aGUgcXVldWUgYnkgY2FsbGluZyBcImFkZEluaXQoKVwiIGFuZCBwcm92aWRpbmdcbiAgICogdGhlIGNhbGxiYWNrLiBUaGUgdmFsdWUgcHVzaGVkIHNob3VsZCBiZSBhbiBhcnJheSwgd2l0aCB0aGUgZmlyc3QgdmFsdWVcbiAgICogYmVpbmcgYSBjYWxsYWJsZSBmdW5jdGlvbiwgZm9sbG93ZWQgYnkgZWFjaCBvZiB0aGUgYXJndW1lbnRzIGZvciB0aGF0XG4gICAqIGZ1bmN0aW9uLCBpZiB0aGVyZSBhcmUgYW55LlxuICAgKlxuICAgKiBFWDogd2luZG93Lk1hcGtpdC5hZGRJbml0KCdnbWFwJywgW2Z1bmMsIGFyZzEsIGFyZzIsIGFyZzNdKTtcbiAgICovXG4gIHdpbmRvdy5NYXBraXQgPSB7XG4gICAgbG9hZGVyOiB7fSxcblxuICAgIC8qKlxuICAgICAqIENyZWF0ZXMgYW4gbG9hZGVyIHF1ZXVlIGZvciBhIE1hcGtpdCBwcm92aWRlci5cbiAgICAgKlxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBpZFxuICAgICAqICAgSWRlbml0aWZpZXIgZm9yIHRoZSBsb2FkZXIgcXVldWUgdG8gY3JlYXRlLlxuICAgICAqL1xuICAgIGNyZWF0ZVF1ZXVlKGlkKSB7XG4gICAgICBpZiAoIXRoaXMubG9hZGVyW2lkXSkge1xuICAgICAgICB0aGlzLmxvYWRlcltpZF0gPSBuZXcgTG9hZFF1ZXVlKCk7XG4gICAgICB9XG4gICAgfSxcblxuICAgIC8qKlxuICAgICAqIEFkZCBvciBleGVjdXRlIGEgY2FsbGJhY2suIFRvIHRoZSBzcGVjaWZpZWQgbG9hZGVyIHF1ZXVlLlxuICAgICAqXG4gICAgICogRW5zdXJlIHRoYXQgdGhlIHF1ZXVlIGhhcyBiZWVuIGNyZWF0ZWQgYmVmb3JlIGFkZGluZyB0aGUgaW5pdGlhbGl6ZXJzXG4gICAgICogYnkgY2FsbGluZyBcIk1hcGtpdC5jcmVhdGVRdWV1ZShpZClcIi5cbiAgICAgKlxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBpZFxuICAgICAqICAgVGhlIGxvYWRlciBxdWV1ZSB0byBhZGQgdGhlIGNhbGxiYWNrIHRvLlxuICAgICAqIEBwYXJhbSB7YXJyYXl8ZnVuY3Rpb259IGRhdGFcbiAgICAgKiAgIEVpdGhlciBhIGZ1bmN0aW9uIHRvIGFkZCB0byB0aGUgY2FsbGJhY2tzIHF1ZXVlIG9yIGFuIGFycmF5XG4gICAgICogICB3aXRoIHRoZSBjYWxsYmFjayBhcyB0aGUgZmlyc3QgaXRlbSwgYW5kIHRoZSBmdW5jdGlvbiBwYXJhbWV0ZXJzXG4gICAgICogICBpbiB0aGUgYXJyYXkgZm9sbG93aW5nIHRoZSBjYWxsYmFjay5cbiAgICAgKi9cbiAgICBhZGRJbml0KGlkLCBkYXRhKSB7XG4gICAgICB0aGlzLmxvYWRlcltpZF0uYWRkKGRhdGEpO1xuICAgIH0sXG5cbiAgICAvKipcbiAgICAgKiBTZWFyY2ggZm9yIGFuIGluaXQgZnVuY3Rpb24gdG8gcmVtb3ZlIGZyb20gdGhlIHNwZWNpZmllZCBsb2FkZXIgcXVldWUuXG4gICAgICpcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gaWRcbiAgICAgKiAgIFRoZSBsb2FkZXIgcXVldWUgdG8gc2VhcmNoIGZvciBhbmQgcmVtb3ZlIHRoaXMgaGFuZGxlci5cbiAgICAgKiBAcGFyYW0ge2Z1bmN0aW9ufSBoYW5kbGVyXG4gICAgICogICBUaGUgY2FsbGJhY2sgaGFuZGxlciB0byByZW1vdmUgZnJvbSBpbml0aWFsaXphdGlvbi5cbiAgICAgKi9cbiAgICByZW1vdmVJbml0KGlkLCBoYW5kbGVyKSB7XG4gICAgICBpZiAodGhpcy5sb2FkZXJbaWRdKSB7XG4gICAgICAgIHRoaXMubGFvZGVyW2lkXS5yZW1vdmUoaGFuZGxlcik7XG4gICAgICB9XG4gICAgfSxcbiAgfTtcbn0pKCk7XG4iXSwibWFwcGluZ3MiOiI7O0FBQUEsQ0FBQyxNQUFNO0VBQ0w7QUFDRjtBQUNBO0VBQ0UsTUFBTUEsU0FBUyxDQUFDO0lBQ2RDLFdBQVcsR0FBRztNQUNaLElBQUksQ0FBQ0MsS0FBSyxHQUFHLEVBQUU7SUFDakI7O0lBRUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0lBQ0lDLElBQUksR0FBRztNQUNMO01BQ0E7TUFDQTtNQUNBLElBQUlDLEVBQUUsR0FBRyxJQUFJLENBQUNGLEtBQUssQ0FBQ0csR0FBRyxFQUFFO01BQ3pCLE9BQU9ELEVBQUUsRUFBRTtRQUNULE1BQU1FLElBQUksR0FBR0YsRUFBRSxDQUFDRyxLQUFLLEVBQUU7UUFDdkJELElBQUksQ0FBQyxHQUFHRixFQUFFLENBQUM7UUFDWEEsRUFBRSxHQUFHLElBQUksQ0FBQ0YsS0FBSyxDQUFDRyxHQUFHLEVBQUU7TUFDdkI7O01BRUE7TUFDQTtNQUNBLElBQUksQ0FBQ0gsS0FBSyxDQUFDTSxJQUFJLEdBQUcsQ0FBQyxDQUFDRixJQUFJLEVBQUUsR0FBR0csSUFBSSxDQUFDLEtBQUtILElBQUksQ0FBQyxHQUFHRyxJQUFJLENBQUM7SUFDdEQ7O0lBRUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7SUFDSUMsR0FBRyxDQUFDQyxJQUFJLEVBQUU7TUFDUixJQUFJLE9BQU9BLElBQUksS0FBSyxVQUFVLEVBQUU7UUFDOUIsSUFBSSxDQUFDVCxLQUFLLENBQUNNLElBQUksQ0FBQyxDQUFDRyxJQUFJLENBQUMsQ0FBQztNQUN6QixDQUFDLE1BQ0ksSUFBSUMsS0FBSyxDQUFDQyxPQUFPLENBQUNGLElBQUksQ0FBQyxJQUFJLE9BQU9BLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxVQUFVLEVBQUU7UUFDN0QsSUFBSSxDQUFDVCxLQUFLLENBQUNNLElBQUksQ0FBQ0csSUFBSSxDQUFDO01BQ3ZCLENBQUMsTUFDSTtRQUNILE1BQU0sSUFBSUcsS0FBSyxDQUFDLCtEQUErRCxDQUFDO01BQ2xGO0lBQ0Y7O0lBRUE7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtJQUNJQyxNQUFNLENBQUNDLE9BQU8sRUFBRTtNQUNkO01BQ0EsS0FBSyxJQUFJQyxDQUFDLEdBQUcsQ0FBQyxFQUFFQSxDQUFDLEdBQUcsSUFBSSxDQUFDZixLQUFLLENBQUNnQixNQUFNLEVBQUUsRUFBRUQsQ0FBQyxFQUFFO1FBQzFDLE1BQU1OLElBQUksR0FBRyxJQUFJLENBQUNULEtBQUssQ0FBQ2UsQ0FBQyxDQUFDO1FBRTFCLElBQUlOLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBS0ssT0FBTyxFQUFFO1VBQ3ZCLElBQUksQ0FBQ2QsS0FBSyxDQUFDaUIsTUFBTSxDQUFDRixDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ3pCO01BQ0Y7SUFDRjtFQUNGOztFQUVBO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7RUFDRUcsTUFBTSxDQUFDQyxNQUFNLEdBQUc7SUFDZEMsTUFBTSxFQUFFLENBQUMsQ0FBQztJQUVWO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtJQUNJQyxXQUFXLENBQUNDLEVBQUUsRUFBRTtNQUNkLElBQUksQ0FBQyxJQUFJLENBQUNGLE1BQU0sQ0FBQ0UsRUFBRSxDQUFDLEVBQUU7UUFDcEIsSUFBSSxDQUFDRixNQUFNLENBQUNFLEVBQUUsQ0FBQyxHQUFHLElBQUl4QixTQUFTLEVBQUU7TUFDbkM7SUFDRixDQUFDO0lBRUQ7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7SUFDSXlCLE9BQU8sQ0FBQ0QsRUFBRSxFQUFFYixJQUFJLEVBQUU7TUFDaEIsSUFBSSxDQUFDVyxNQUFNLENBQUNFLEVBQUUsQ0FBQyxDQUFDZCxHQUFHLENBQUNDLElBQUksQ0FBQztJQUMzQixDQUFDO0lBRUQ7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtJQUNJZSxVQUFVLENBQUNGLEVBQUUsRUFBRVIsT0FBTyxFQUFFO01BQ3RCLElBQUksSUFBSSxDQUFDTSxNQUFNLENBQUNFLEVBQUUsQ0FBQyxFQUFFO1FBQ25CLElBQUksQ0FBQ0csTUFBTSxDQUFDSCxFQUFFLENBQUMsQ0FBQ1QsTUFBTSxDQUFDQyxPQUFPLENBQUM7TUFDakM7SUFDRjtFQUNGLENBQUM7QUFDSCxDQUFDLEdBQUcifQ==
