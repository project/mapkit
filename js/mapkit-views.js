"use strict";

(({
  Toolshed: ts,
  Mapkit
}) => {
  /**
   * A single location entry, which connects the result row to a map marker. A
   * location can have several lat/lng coordinates but only one may be active
   * at a time.
   */
  class Location {
    constructor(el, data, view) {
      this.el = el;
      this.active = 0;
      this.marker = null;
      this.data = [];
      this.list = el.getElementsByClassName('js-mapkit-location-list').item(0);
      const displays = [];
      if (this.list && data.length > 1) {
        ts.walkByClass(this.list, 'location-item', item => {
          displays[item.dataset.index] = item;
        });
      }

      // Add the geolocations for this location.
      let minDist = null;
      let closest = 0;
      data.forEach((pt, key) => {
        this.data[key] = {
          display: displays[key],
          latLng: pt,
          distance: null
        };

        // If not the currently active display, hide the location result.
        if (this.active !== key && displays[key]) {
          displays[key].style.display = 'none';
        }

        // If there is a reference point, we can calculate distances. With
        // distances we set the closest geo-point as the active point.
        if (view.refPt) {
          const dist = Mapkit.calcDist(view.refPt, pt, view.units);
          this.data[key].distance = dist;
          if (minDist === null || minDist > dist) {
            closest = key;
            minDist = dist;
          }
        }
      });
      if (this.active !== closest) {
        this.setActive(closest);
      }
    }

    /**
     * Getter to get the currently active lat/lng for this location.
     *
     * @return {object|null}
     *   The active lat/lng coordinate for this location entry.
     */
    get latLng() {
      return (this.data[this.active] || {}).latLng;
    }

    /**
     * The gets the active location display.
     */
    get display() {
      return (this.data[this.active] || {}).display;
    }

    /**
     * Change the active location based on index. This updates the displayed
     * marker position if marker was already rendered.
     *
     * @param {int} index
     *   The index of the location to make active.
     */
    setActive(index) {
      // Hide the previously selected item.
      const prev = this.display;
      if (prev) {
        prev.style.display = 'none';
        prev.classList.remove('location-item--active');
      }
      this.active = index;

      // Make newly selected location visible.
      const curr = this.display;
      if (curr) {
        curr.style.display = '';
        curr.classList.add('location-item--active');
      }
      if (this.marker) {
        this.marker.position = this.latLng;
      }
    }
  }

  /**
   * Mapkit views style plugin object, which maintains the JS state, map
   * and location information for each of the views results.
   */
  class MapkitLocationView {
    constructor(el, settings, data) {
      this.el = el;

      // Apply location settings data settings to this instance.
      this.units = settings.units || 'km';
      this.precision = settings.percision || 2;
      this.refPt = settings.refLocation;
      this.locations = new Map();

      // Configure the map and popup event handlers.
      this.markerPopupContent = settings.markerPopupContent;
      this.events = settings.events || {};

      // Find and build each of the rows of data into coordinate map data.
      // These data locations will later be used to place map markers during
      // populateMarkers() if a map has been attached.
      ts.walkBySelector(el, settings.rowSelector || '.views-row', rowEl => {
        const {
          dataId
        } = rowEl.dataset;
        if (data[dataId]) {
          this.locations.set(dataId, new Location(rowEl, data[dataId], this));
        }
      });
      if (settings.map) {
        const mapEl = el.getElementsByClassName('js-view-mapkit-style-map')[0];
        if (mapEl) {
          mapEl.style.width = settings.mapWidth || '100%';
          mapEl.style.height = settings.mapHeight || '500px';
          this.map = Mapkit.createMap(mapEl, settings.map, settings.markers);
          if (this.events.onMapCreate) {
            this.events.onMapCreate.forEach(cb => cb(this, this.map));
          }
          this.populateMarkers();
        }
      }
    }

    /**
     * Render all the map markers for each of the current map locations.
     */
    populateMarkers() {
      if (!this.map) return;
      this.locations.forEach(loc => {
        if (loc.latLng && !loc.marker) {
          loc.marker = this.map.createMarker({
            latLng: loc.latLng
          }, false);

          // If there are registered "onCreateMarker" listeners, apply them to
          // each of the newly created markers.
          if (this.events.onMarkerCreate) {
            this.events.onMarkerCreate.forEach(cb => cb(this, loc, loc.marker));
          }

          // If there is a callback function to fetch contents for a marker
          // popup window, attach an "onClick" handler to trigger the popup.
          if (this.markerPopupContent instanceof Function) {
            loc.marker.on('click', () => {
              const content = this.markerPopupContent(this, loc);
              if (content) this.map.openMarkerPopup(loc.marker, content);
            });
          }
        }
      });
    }

    /**
     * Clean up map resources and event listeners.
     */
    destroy() {
      if (this.map) {
        if (this.events.onMapDestroy) {
          this.events.onMapDestroy.forEach(cb => cb(this, this.map));
        }
        this.map.destroy();
      }
    }
  }

  /**
   * Load behaviors for mapkit location views style.
   */
  Drupal.behaviors.mapkitViewStyle = {
    dataKeyRegex: /(?:\s|^)((?:js-view-dom-id-)[^\s]+)/,
    views: new Map(),
    attach(context, settings) {
      const instances = (settings.mapkitViews || {}).instance;

      // Find each of the locations views and build locations and maps.
      ts.walkByClass(context, 'view-content', el => {
        const view = el.closest('.view');
        const match = view ? this.dataKeyRegex.exec(view.getAttribute('class')) : '';
        if (match && instances[match[1]]) {
          const data = instances[match[1]].locations;
          const config = Mapkit.config.applyOverrides(instances[match[1]]);
          this.views.set(match[1], new MapkitLocationView(el, config, data));
        }
      }, 'mapkit-view-processed');
    },
    detach(context, settings, trigger) {
      if (trigger !== 'unload') return;

      // Remove behaviors and clean up map / views resources. Important for AJAX
      // loading and unloading of generated views and map resources.
      ts.walkByClass(context, 'view-content', el => {
        const view = el.closest('.view');
        const match = view ? this.dataKeyRegex.exec(view.getAttribute('class')) : null;
        const instance = match ? this.views.get(match[1]) : null;
        if (instance) {
          this.views.delete(match[1]);
          instance.destroy();
        }
      });
    }
  };
})(Drupal, drupalSettings);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFwa2l0LXZpZXdzLmpzIiwibmFtZXMiOlsiVG9vbHNoZWQiLCJ0cyIsIk1hcGtpdCIsIkxvY2F0aW9uIiwiY29uc3RydWN0b3IiLCJlbCIsImRhdGEiLCJ2aWV3IiwiYWN0aXZlIiwibWFya2VyIiwibGlzdCIsImdldEVsZW1lbnRzQnlDbGFzc05hbWUiLCJpdGVtIiwiZGlzcGxheXMiLCJsZW5ndGgiLCJ3YWxrQnlDbGFzcyIsImRhdGFzZXQiLCJpbmRleCIsIm1pbkRpc3QiLCJjbG9zZXN0IiwiZm9yRWFjaCIsInB0Iiwia2V5IiwiZGlzcGxheSIsImxhdExuZyIsImRpc3RhbmNlIiwic3R5bGUiLCJyZWZQdCIsImRpc3QiLCJjYWxjRGlzdCIsInVuaXRzIiwic2V0QWN0aXZlIiwicHJldiIsImNsYXNzTGlzdCIsInJlbW92ZSIsImN1cnIiLCJhZGQiLCJwb3NpdGlvbiIsIk1hcGtpdExvY2F0aW9uVmlldyIsInNldHRpbmdzIiwicHJlY2lzaW9uIiwicGVyY2lzaW9uIiwicmVmTG9jYXRpb24iLCJsb2NhdGlvbnMiLCJNYXAiLCJtYXJrZXJQb3B1cENvbnRlbnQiLCJldmVudHMiLCJ3YWxrQnlTZWxlY3RvciIsInJvd1NlbGVjdG9yIiwicm93RWwiLCJkYXRhSWQiLCJzZXQiLCJtYXAiLCJtYXBFbCIsIndpZHRoIiwibWFwV2lkdGgiLCJoZWlnaHQiLCJtYXBIZWlnaHQiLCJjcmVhdGVNYXAiLCJtYXJrZXJzIiwib25NYXBDcmVhdGUiLCJjYiIsInBvcHVsYXRlTWFya2VycyIsImxvYyIsImNyZWF0ZU1hcmtlciIsIm9uTWFya2VyQ3JlYXRlIiwiRnVuY3Rpb24iLCJvbiIsImNvbnRlbnQiLCJvcGVuTWFya2VyUG9wdXAiLCJkZXN0cm95Iiwib25NYXBEZXN0cm95IiwiRHJ1cGFsIiwiYmVoYXZpb3JzIiwibWFwa2l0Vmlld1N0eWxlIiwiZGF0YUtleVJlZ2V4Iiwidmlld3MiLCJhdHRhY2giLCJjb250ZXh0IiwiaW5zdGFuY2VzIiwibWFwa2l0Vmlld3MiLCJpbnN0YW5jZSIsIm1hdGNoIiwiZXhlYyIsImdldEF0dHJpYnV0ZSIsImNvbmZpZyIsImFwcGx5T3ZlcnJpZGVzIiwiZGV0YWNoIiwidHJpZ2dlciIsImdldCIsImRlbGV0ZSIsImRydXBhbFNldHRpbmdzIl0sInNvdXJjZXMiOlsibWFwa2l0LXZpZXdzLmVzNi5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIoKHsgVG9vbHNoZWQ6IHRzLCBNYXBraXQgfSkgPT4ge1xuICAvKipcbiAgICogQSBzaW5nbGUgbG9jYXRpb24gZW50cnksIHdoaWNoIGNvbm5lY3RzIHRoZSByZXN1bHQgcm93IHRvIGEgbWFwIG1hcmtlci4gQVxuICAgKiBsb2NhdGlvbiBjYW4gaGF2ZSBzZXZlcmFsIGxhdC9sbmcgY29vcmRpbmF0ZXMgYnV0IG9ubHkgb25lIG1heSBiZSBhY3RpdmVcbiAgICogYXQgYSB0aW1lLlxuICAgKi9cbiAgY2xhc3MgTG9jYXRpb24ge1xuICAgIGNvbnN0cnVjdG9yKGVsLCBkYXRhLCB2aWV3KSB7XG4gICAgICB0aGlzLmVsID0gZWw7XG4gICAgICB0aGlzLmFjdGl2ZSA9IDA7XG4gICAgICB0aGlzLm1hcmtlciA9IG51bGw7XG4gICAgICB0aGlzLmRhdGEgPSBbXTtcbiAgICAgIHRoaXMubGlzdCA9IGVsLmdldEVsZW1lbnRzQnlDbGFzc05hbWUoJ2pzLW1hcGtpdC1sb2NhdGlvbi1saXN0JykuaXRlbSgwKTtcblxuICAgICAgY29uc3QgZGlzcGxheXMgPSBbXTtcbiAgICAgIGlmICh0aGlzLmxpc3QgJiYgZGF0YS5sZW5ndGggPiAxKSB7XG4gICAgICAgIHRzLndhbGtCeUNsYXNzKHRoaXMubGlzdCwgJ2xvY2F0aW9uLWl0ZW0nLCAoaXRlbSkgPT4ge1xuICAgICAgICAgIGRpc3BsYXlzW2l0ZW0uZGF0YXNldC5pbmRleF0gPSBpdGVtO1xuICAgICAgICB9KTtcbiAgICAgIH1cblxuICAgICAgLy8gQWRkIHRoZSBnZW9sb2NhdGlvbnMgZm9yIHRoaXMgbG9jYXRpb24uXG4gICAgICBsZXQgbWluRGlzdCA9IG51bGw7XG4gICAgICBsZXQgY2xvc2VzdCA9IDA7XG4gICAgICBkYXRhLmZvckVhY2goKHB0LCBrZXkpID0+IHtcbiAgICAgICAgdGhpcy5kYXRhW2tleV0gPSB7XG4gICAgICAgICAgZGlzcGxheTogZGlzcGxheXNba2V5XSxcbiAgICAgICAgICBsYXRMbmc6IHB0LFxuICAgICAgICAgIGRpc3RhbmNlOiBudWxsLFxuICAgICAgICB9O1xuXG4gICAgICAgIC8vIElmIG5vdCB0aGUgY3VycmVudGx5IGFjdGl2ZSBkaXNwbGF5LCBoaWRlIHRoZSBsb2NhdGlvbiByZXN1bHQuXG4gICAgICAgIGlmICh0aGlzLmFjdGl2ZSAhPT0ga2V5ICYmIGRpc3BsYXlzW2tleV0pIHtcbiAgICAgICAgICBkaXNwbGF5c1trZXldLnN0eWxlLmRpc3BsYXkgPSAnbm9uZSc7XG4gICAgICAgIH1cblxuICAgICAgICAvLyBJZiB0aGVyZSBpcyBhIHJlZmVyZW5jZSBwb2ludCwgd2UgY2FuIGNhbGN1bGF0ZSBkaXN0YW5jZXMuIFdpdGhcbiAgICAgICAgLy8gZGlzdGFuY2VzIHdlIHNldCB0aGUgY2xvc2VzdCBnZW8tcG9pbnQgYXMgdGhlIGFjdGl2ZSBwb2ludC5cbiAgICAgICAgaWYgKHZpZXcucmVmUHQpIHtcbiAgICAgICAgICBjb25zdCBkaXN0ID0gTWFwa2l0LmNhbGNEaXN0KHZpZXcucmVmUHQsIHB0LCB2aWV3LnVuaXRzKTtcbiAgICAgICAgICB0aGlzLmRhdGFba2V5XS5kaXN0YW5jZSA9IGRpc3Q7XG5cbiAgICAgICAgICBpZiAobWluRGlzdCA9PT0gbnVsbCB8fCBtaW5EaXN0ID4gZGlzdCkge1xuICAgICAgICAgICAgY2xvc2VzdCA9IGtleTtcbiAgICAgICAgICAgIG1pbkRpc3QgPSBkaXN0O1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfSk7XG5cbiAgICAgIGlmICh0aGlzLmFjdGl2ZSAhPT0gY2xvc2VzdCkge1xuICAgICAgICB0aGlzLnNldEFjdGl2ZShjbG9zZXN0KTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBHZXR0ZXIgdG8gZ2V0IHRoZSBjdXJyZW50bHkgYWN0aXZlIGxhdC9sbmcgZm9yIHRoaXMgbG9jYXRpb24uXG4gICAgICpcbiAgICAgKiBAcmV0dXJuIHtvYmplY3R8bnVsbH1cbiAgICAgKiAgIFRoZSBhY3RpdmUgbGF0L2xuZyBjb29yZGluYXRlIGZvciB0aGlzIGxvY2F0aW9uIGVudHJ5LlxuICAgICAqL1xuICAgIGdldCBsYXRMbmcoKSB7XG4gICAgICByZXR1cm4gKHRoaXMuZGF0YVt0aGlzLmFjdGl2ZV0gfHwge30pLmxhdExuZztcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBUaGUgZ2V0cyB0aGUgYWN0aXZlIGxvY2F0aW9uIGRpc3BsYXkuXG4gICAgICovXG4gICAgZ2V0IGRpc3BsYXkoKSB7XG4gICAgICByZXR1cm4gKHRoaXMuZGF0YVt0aGlzLmFjdGl2ZV0gfHwge30pLmRpc3BsYXk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogQ2hhbmdlIHRoZSBhY3RpdmUgbG9jYXRpb24gYmFzZWQgb24gaW5kZXguIFRoaXMgdXBkYXRlcyB0aGUgZGlzcGxheWVkXG4gICAgICogbWFya2VyIHBvc2l0aW9uIGlmIG1hcmtlciB3YXMgYWxyZWFkeSByZW5kZXJlZC5cbiAgICAgKlxuICAgICAqIEBwYXJhbSB7aW50fSBpbmRleFxuICAgICAqICAgVGhlIGluZGV4IG9mIHRoZSBsb2NhdGlvbiB0byBtYWtlIGFjdGl2ZS5cbiAgICAgKi9cbiAgICBzZXRBY3RpdmUoaW5kZXgpIHtcbiAgICAgIC8vIEhpZGUgdGhlIHByZXZpb3VzbHkgc2VsZWN0ZWQgaXRlbS5cbiAgICAgIGNvbnN0IHByZXYgPSB0aGlzLmRpc3BsYXk7XG4gICAgICBpZiAocHJldikge1xuICAgICAgICBwcmV2LnN0eWxlLmRpc3BsYXkgPSAnbm9uZSc7XG4gICAgICAgIHByZXYuY2xhc3NMaXN0LnJlbW92ZSgnbG9jYXRpb24taXRlbS0tYWN0aXZlJyk7XG4gICAgICB9XG5cbiAgICAgIHRoaXMuYWN0aXZlID0gaW5kZXg7XG5cbiAgICAgIC8vIE1ha2UgbmV3bHkgc2VsZWN0ZWQgbG9jYXRpb24gdmlzaWJsZS5cbiAgICAgIGNvbnN0IGN1cnIgPSB0aGlzLmRpc3BsYXk7XG4gICAgICBpZiAoY3Vycikge1xuICAgICAgICBjdXJyLnN0eWxlLmRpc3BsYXkgPSAnJztcbiAgICAgICAgY3Vyci5jbGFzc0xpc3QuYWRkKCdsb2NhdGlvbi1pdGVtLS1hY3RpdmUnKTtcbiAgICAgIH1cblxuICAgICAgaWYgKHRoaXMubWFya2VyKSB7XG4gICAgICAgIHRoaXMubWFya2VyLnBvc2l0aW9uID0gdGhpcy5sYXRMbmc7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIE1hcGtpdCB2aWV3cyBzdHlsZSBwbHVnaW4gb2JqZWN0LCB3aGljaCBtYWludGFpbnMgdGhlIEpTIHN0YXRlLCBtYXBcbiAgICogYW5kIGxvY2F0aW9uIGluZm9ybWF0aW9uIGZvciBlYWNoIG9mIHRoZSB2aWV3cyByZXN1bHRzLlxuICAgKi9cbiAgY2xhc3MgTWFwa2l0TG9jYXRpb25WaWV3IHtcbiAgICBjb25zdHJ1Y3RvcihlbCwgc2V0dGluZ3MsIGRhdGEpIHtcbiAgICAgIHRoaXMuZWwgPSBlbDtcblxuICAgICAgLy8gQXBwbHkgbG9jYXRpb24gc2V0dGluZ3MgZGF0YSBzZXR0aW5ncyB0byB0aGlzIGluc3RhbmNlLlxuICAgICAgdGhpcy51bml0cyA9IHNldHRpbmdzLnVuaXRzIHx8ICdrbSc7XG4gICAgICB0aGlzLnByZWNpc2lvbiA9IHNldHRpbmdzLnBlcmNpc2lvbiB8fCAyO1xuICAgICAgdGhpcy5yZWZQdCA9IHNldHRpbmdzLnJlZkxvY2F0aW9uO1xuICAgICAgdGhpcy5sb2NhdGlvbnMgPSBuZXcgTWFwKCk7XG5cbiAgICAgIC8vIENvbmZpZ3VyZSB0aGUgbWFwIGFuZCBwb3B1cCBldmVudCBoYW5kbGVycy5cbiAgICAgIHRoaXMubWFya2VyUG9wdXBDb250ZW50ID0gc2V0dGluZ3MubWFya2VyUG9wdXBDb250ZW50O1xuICAgICAgdGhpcy5ldmVudHMgPSBzZXR0aW5ncy5ldmVudHMgfHwge307XG5cbiAgICAgIC8vIEZpbmQgYW5kIGJ1aWxkIGVhY2ggb2YgdGhlIHJvd3Mgb2YgZGF0YSBpbnRvIGNvb3JkaW5hdGUgbWFwIGRhdGEuXG4gICAgICAvLyBUaGVzZSBkYXRhIGxvY2F0aW9ucyB3aWxsIGxhdGVyIGJlIHVzZWQgdG8gcGxhY2UgbWFwIG1hcmtlcnMgZHVyaW5nXG4gICAgICAvLyBwb3B1bGF0ZU1hcmtlcnMoKSBpZiBhIG1hcCBoYXMgYmVlbiBhdHRhY2hlZC5cbiAgICAgIHRzLndhbGtCeVNlbGVjdG9yKGVsLCBzZXR0aW5ncy5yb3dTZWxlY3RvciB8fCAnLnZpZXdzLXJvdycsIChyb3dFbCkgPT4ge1xuICAgICAgICBjb25zdCB7IGRhdGFJZCB9ID0gcm93RWwuZGF0YXNldDtcblxuICAgICAgICBpZiAoZGF0YVtkYXRhSWRdKSB7XG4gICAgICAgICAgdGhpcy5sb2NhdGlvbnMuc2V0KGRhdGFJZCwgbmV3IExvY2F0aW9uKHJvd0VsLCBkYXRhW2RhdGFJZF0sIHRoaXMpKTtcbiAgICAgICAgfVxuICAgICAgfSk7XG5cbiAgICAgIGlmIChzZXR0aW5ncy5tYXApIHtcbiAgICAgICAgY29uc3QgbWFwRWwgPSBlbC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKCdqcy12aWV3LW1hcGtpdC1zdHlsZS1tYXAnKVswXTtcblxuICAgICAgICBpZiAobWFwRWwpIHtcbiAgICAgICAgICBtYXBFbC5zdHlsZS53aWR0aCA9IHNldHRpbmdzLm1hcFdpZHRoIHx8ICcxMDAlJztcbiAgICAgICAgICBtYXBFbC5zdHlsZS5oZWlnaHQgPSBzZXR0aW5ncy5tYXBIZWlnaHQgfHwgJzUwMHB4JztcbiAgICAgICAgICB0aGlzLm1hcCA9IE1hcGtpdC5jcmVhdGVNYXAobWFwRWwsIHNldHRpbmdzLm1hcCwgc2V0dGluZ3MubWFya2Vycyk7XG5cbiAgICAgICAgICBpZiAodGhpcy5ldmVudHMub25NYXBDcmVhdGUpIHtcbiAgICAgICAgICAgIHRoaXMuZXZlbnRzLm9uTWFwQ3JlYXRlLmZvckVhY2goKGNiKSA9PiBjYih0aGlzLCB0aGlzLm1hcCkpO1xuICAgICAgICAgIH1cblxuICAgICAgICAgIHRoaXMucG9wdWxhdGVNYXJrZXJzKCk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBSZW5kZXIgYWxsIHRoZSBtYXAgbWFya2VycyBmb3IgZWFjaCBvZiB0aGUgY3VycmVudCBtYXAgbG9jYXRpb25zLlxuICAgICAqL1xuICAgIHBvcHVsYXRlTWFya2VycygpIHtcbiAgICAgIGlmICghdGhpcy5tYXApIHJldHVybjtcblxuICAgICAgdGhpcy5sb2NhdGlvbnMuZm9yRWFjaCgobG9jKSA9PiB7XG4gICAgICAgIGlmIChsb2MubGF0TG5nICYmICFsb2MubWFya2VyKSB7XG4gICAgICAgICAgbG9jLm1hcmtlciA9IHRoaXMubWFwLmNyZWF0ZU1hcmtlcih7IGxhdExuZzogbG9jLmxhdExuZyB9LCBmYWxzZSk7XG5cbiAgICAgICAgICAvLyBJZiB0aGVyZSBhcmUgcmVnaXN0ZXJlZCBcIm9uQ3JlYXRlTWFya2VyXCIgbGlzdGVuZXJzLCBhcHBseSB0aGVtIHRvXG4gICAgICAgICAgLy8gZWFjaCBvZiB0aGUgbmV3bHkgY3JlYXRlZCBtYXJrZXJzLlxuICAgICAgICAgIGlmICh0aGlzLmV2ZW50cy5vbk1hcmtlckNyZWF0ZSkge1xuICAgICAgICAgICAgdGhpcy5ldmVudHMub25NYXJrZXJDcmVhdGUuZm9yRWFjaCgoY2IpID0+IGNiKHRoaXMsIGxvYywgbG9jLm1hcmtlcikpO1xuICAgICAgICAgIH1cblxuICAgICAgICAgIC8vIElmIHRoZXJlIGlzIGEgY2FsbGJhY2sgZnVuY3Rpb24gdG8gZmV0Y2ggY29udGVudHMgZm9yIGEgbWFya2VyXG4gICAgICAgICAgLy8gcG9wdXAgd2luZG93LCBhdHRhY2ggYW4gXCJvbkNsaWNrXCIgaGFuZGxlciB0byB0cmlnZ2VyIHRoZSBwb3B1cC5cbiAgICAgICAgICBpZiAodGhpcy5tYXJrZXJQb3B1cENvbnRlbnQgaW5zdGFuY2VvZiBGdW5jdGlvbikge1xuICAgICAgICAgICAgbG9jLm1hcmtlci5vbignY2xpY2snLCAoKSA9PiB7XG4gICAgICAgICAgICAgIGNvbnN0IGNvbnRlbnQgPSB0aGlzLm1hcmtlclBvcHVwQ29udGVudCh0aGlzLCBsb2MpO1xuICAgICAgICAgICAgICBpZiAoY29udGVudCkgdGhpcy5tYXAub3Blbk1hcmtlclBvcHVwKGxvYy5tYXJrZXIsIGNvbnRlbnQpO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9KTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBDbGVhbiB1cCBtYXAgcmVzb3VyY2VzIGFuZCBldmVudCBsaXN0ZW5lcnMuXG4gICAgICovXG4gICAgZGVzdHJveSgpIHtcbiAgICAgIGlmICh0aGlzLm1hcCkge1xuICAgICAgICBpZiAodGhpcy5ldmVudHMub25NYXBEZXN0cm95KSB7XG4gICAgICAgICAgdGhpcy5ldmVudHMub25NYXBEZXN0cm95LmZvckVhY2goKGNiKSA9PiBjYih0aGlzLCB0aGlzLm1hcCkpO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5tYXAuZGVzdHJveSgpO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBMb2FkIGJlaGF2aW9ycyBmb3IgbWFwa2l0IGxvY2F0aW9uIHZpZXdzIHN0eWxlLlxuICAgKi9cbiAgRHJ1cGFsLmJlaGF2aW9ycy5tYXBraXRWaWV3U3R5bGUgPSB7XG4gICAgZGF0YUtleVJlZ2V4OiAvKD86XFxzfF4pKCg/OmpzLXZpZXctZG9tLWlkLSlbXlxcc10rKS8sXG4gICAgdmlld3M6IG5ldyBNYXAoKSxcblxuICAgIGF0dGFjaChjb250ZXh0LCBzZXR0aW5ncykge1xuICAgICAgY29uc3QgaW5zdGFuY2VzID0gKHNldHRpbmdzLm1hcGtpdFZpZXdzIHx8IHt9KS5pbnN0YW5jZTtcblxuICAgICAgLy8gRmluZCBlYWNoIG9mIHRoZSBsb2NhdGlvbnMgdmlld3MgYW5kIGJ1aWxkIGxvY2F0aW9ucyBhbmQgbWFwcy5cbiAgICAgIHRzLndhbGtCeUNsYXNzKGNvbnRleHQsICd2aWV3LWNvbnRlbnQnLCAoZWwpID0+IHtcbiAgICAgICAgY29uc3QgdmlldyA9IGVsLmNsb3Nlc3QoJy52aWV3Jyk7XG4gICAgICAgIGNvbnN0IG1hdGNoID0gdmlldyA/IHRoaXMuZGF0YUtleVJlZ2V4LmV4ZWModmlldy5nZXRBdHRyaWJ1dGUoJ2NsYXNzJykpIDogJyc7XG5cbiAgICAgICAgaWYgKG1hdGNoICYmIGluc3RhbmNlc1ttYXRjaFsxXV0pIHtcbiAgICAgICAgICBjb25zdCBkYXRhID0gaW5zdGFuY2VzW21hdGNoWzFdXS5sb2NhdGlvbnM7XG4gICAgICAgICAgY29uc3QgY29uZmlnID0gTWFwa2l0LmNvbmZpZy5hcHBseU92ZXJyaWRlcyhpbnN0YW5jZXNbbWF0Y2hbMV1dKTtcbiAgICAgICAgICB0aGlzLnZpZXdzLnNldChtYXRjaFsxXSwgbmV3IE1hcGtpdExvY2F0aW9uVmlldyhlbCwgY29uZmlnLCBkYXRhKSk7XG4gICAgICAgIH1cbiAgICAgIH0sICdtYXBraXQtdmlldy1wcm9jZXNzZWQnKTtcbiAgICB9LFxuXG4gICAgZGV0YWNoKGNvbnRleHQsIHNldHRpbmdzLCB0cmlnZ2VyKSB7XG4gICAgICBpZiAodHJpZ2dlciAhPT0gJ3VubG9hZCcpIHJldHVybjtcblxuICAgICAgLy8gUmVtb3ZlIGJlaGF2aW9ycyBhbmQgY2xlYW4gdXAgbWFwIC8gdmlld3MgcmVzb3VyY2VzLiBJbXBvcnRhbnQgZm9yIEFKQVhcbiAgICAgIC8vIGxvYWRpbmcgYW5kIHVubG9hZGluZyBvZiBnZW5lcmF0ZWQgdmlld3MgYW5kIG1hcCByZXNvdXJjZXMuXG4gICAgICB0cy53YWxrQnlDbGFzcyhjb250ZXh0LCAndmlldy1jb250ZW50JywgKGVsKSA9PiB7XG4gICAgICAgIGNvbnN0IHZpZXcgPSBlbC5jbG9zZXN0KCcudmlldycpO1xuICAgICAgICBjb25zdCBtYXRjaCA9IHZpZXcgPyB0aGlzLmRhdGFLZXlSZWdleC5leGVjKHZpZXcuZ2V0QXR0cmlidXRlKCdjbGFzcycpKSA6IG51bGw7XG4gICAgICAgIGNvbnN0IGluc3RhbmNlID0gbWF0Y2ggPyB0aGlzLnZpZXdzLmdldChtYXRjaFsxXSkgOiBudWxsO1xuXG4gICAgICAgIGlmIChpbnN0YW5jZSkge1xuICAgICAgICAgIHRoaXMudmlld3MuZGVsZXRlKG1hdGNoWzFdKTtcbiAgICAgICAgICBpbnN0YW5jZS5kZXN0cm95KCk7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgIH0sXG4gIH07XG59KShEcnVwYWwsIGRydXBhbFNldHRpbmdzKTtcbiJdLCJtYXBwaW5ncyI6Ijs7QUFBQSxDQUFDLENBQUM7RUFBRUEsUUFBUSxFQUFFQyxFQUFFO0VBQUVDO0FBQU8sQ0FBQyxLQUFLO0VBQzdCO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7RUFDRSxNQUFNQyxRQUFRLENBQUM7SUFDYkMsV0FBVyxDQUFDQyxFQUFFLEVBQUVDLElBQUksRUFBRUMsSUFBSSxFQUFFO01BQzFCLElBQUksQ0FBQ0YsRUFBRSxHQUFHQSxFQUFFO01BQ1osSUFBSSxDQUFDRyxNQUFNLEdBQUcsQ0FBQztNQUNmLElBQUksQ0FBQ0MsTUFBTSxHQUFHLElBQUk7TUFDbEIsSUFBSSxDQUFDSCxJQUFJLEdBQUcsRUFBRTtNQUNkLElBQUksQ0FBQ0ksSUFBSSxHQUFHTCxFQUFFLENBQUNNLHNCQUFzQixDQUFDLHlCQUF5QixDQUFDLENBQUNDLElBQUksQ0FBQyxDQUFDLENBQUM7TUFFeEUsTUFBTUMsUUFBUSxHQUFHLEVBQUU7TUFDbkIsSUFBSSxJQUFJLENBQUNILElBQUksSUFBSUosSUFBSSxDQUFDUSxNQUFNLEdBQUcsQ0FBQyxFQUFFO1FBQ2hDYixFQUFFLENBQUNjLFdBQVcsQ0FBQyxJQUFJLENBQUNMLElBQUksRUFBRSxlQUFlLEVBQUdFLElBQUksSUFBSztVQUNuREMsUUFBUSxDQUFDRCxJQUFJLENBQUNJLE9BQU8sQ0FBQ0MsS0FBSyxDQUFDLEdBQUdMLElBQUk7UUFDckMsQ0FBQyxDQUFDO01BQ0o7O01BRUE7TUFDQSxJQUFJTSxPQUFPLEdBQUcsSUFBSTtNQUNsQixJQUFJQyxPQUFPLEdBQUcsQ0FBQztNQUNmYixJQUFJLENBQUNjLE9BQU8sQ0FBQyxDQUFDQyxFQUFFLEVBQUVDLEdBQUcsS0FBSztRQUN4QixJQUFJLENBQUNoQixJQUFJLENBQUNnQixHQUFHLENBQUMsR0FBRztVQUNmQyxPQUFPLEVBQUVWLFFBQVEsQ0FBQ1MsR0FBRyxDQUFDO1VBQ3RCRSxNQUFNLEVBQUVILEVBQUU7VUFDVkksUUFBUSxFQUFFO1FBQ1osQ0FBQzs7UUFFRDtRQUNBLElBQUksSUFBSSxDQUFDakIsTUFBTSxLQUFLYyxHQUFHLElBQUlULFFBQVEsQ0FBQ1MsR0FBRyxDQUFDLEVBQUU7VUFDeENULFFBQVEsQ0FBQ1MsR0FBRyxDQUFDLENBQUNJLEtBQUssQ0FBQ0gsT0FBTyxHQUFHLE1BQU07UUFDdEM7O1FBRUE7UUFDQTtRQUNBLElBQUloQixJQUFJLENBQUNvQixLQUFLLEVBQUU7VUFDZCxNQUFNQyxJQUFJLEdBQUcxQixNQUFNLENBQUMyQixRQUFRLENBQUN0QixJQUFJLENBQUNvQixLQUFLLEVBQUVOLEVBQUUsRUFBRWQsSUFBSSxDQUFDdUIsS0FBSyxDQUFDO1VBQ3hELElBQUksQ0FBQ3hCLElBQUksQ0FBQ2dCLEdBQUcsQ0FBQyxDQUFDRyxRQUFRLEdBQUdHLElBQUk7VUFFOUIsSUFBSVYsT0FBTyxLQUFLLElBQUksSUFBSUEsT0FBTyxHQUFHVSxJQUFJLEVBQUU7WUFDdENULE9BQU8sR0FBR0csR0FBRztZQUNiSixPQUFPLEdBQUdVLElBQUk7VUFDaEI7UUFDRjtNQUNGLENBQUMsQ0FBQztNQUVGLElBQUksSUFBSSxDQUFDcEIsTUFBTSxLQUFLVyxPQUFPLEVBQUU7UUFDM0IsSUFBSSxDQUFDWSxTQUFTLENBQUNaLE9BQU8sQ0FBQztNQUN6QjtJQUNGOztJQUVBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtJQUNJLElBQUlLLE1BQU0sR0FBRztNQUNYLE9BQU8sQ0FBQyxJQUFJLENBQUNsQixJQUFJLENBQUMsSUFBSSxDQUFDRSxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRWdCLE1BQU07SUFDOUM7O0lBRUE7QUFDSjtBQUNBO0lBQ0ksSUFBSUQsT0FBTyxHQUFHO01BQ1osT0FBTyxDQUFDLElBQUksQ0FBQ2pCLElBQUksQ0FBQyxJQUFJLENBQUNFLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFZSxPQUFPO0lBQy9DOztJQUVBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0lBQ0lRLFNBQVMsQ0FBQ2QsS0FBSyxFQUFFO01BQ2Y7TUFDQSxNQUFNZSxJQUFJLEdBQUcsSUFBSSxDQUFDVCxPQUFPO01BQ3pCLElBQUlTLElBQUksRUFBRTtRQUNSQSxJQUFJLENBQUNOLEtBQUssQ0FBQ0gsT0FBTyxHQUFHLE1BQU07UUFDM0JTLElBQUksQ0FBQ0MsU0FBUyxDQUFDQyxNQUFNLENBQUMsdUJBQXVCLENBQUM7TUFDaEQ7TUFFQSxJQUFJLENBQUMxQixNQUFNLEdBQUdTLEtBQUs7O01BRW5CO01BQ0EsTUFBTWtCLElBQUksR0FBRyxJQUFJLENBQUNaLE9BQU87TUFDekIsSUFBSVksSUFBSSxFQUFFO1FBQ1JBLElBQUksQ0FBQ1QsS0FBSyxDQUFDSCxPQUFPLEdBQUcsRUFBRTtRQUN2QlksSUFBSSxDQUFDRixTQUFTLENBQUNHLEdBQUcsQ0FBQyx1QkFBdUIsQ0FBQztNQUM3QztNQUVBLElBQUksSUFBSSxDQUFDM0IsTUFBTSxFQUFFO1FBQ2YsSUFBSSxDQUFDQSxNQUFNLENBQUM0QixRQUFRLEdBQUcsSUFBSSxDQUFDYixNQUFNO01BQ3BDO0lBQ0Y7RUFDRjs7RUFFQTtBQUNGO0FBQ0E7QUFDQTtFQUNFLE1BQU1jLGtCQUFrQixDQUFDO0lBQ3ZCbEMsV0FBVyxDQUFDQyxFQUFFLEVBQUVrQyxRQUFRLEVBQUVqQyxJQUFJLEVBQUU7TUFDOUIsSUFBSSxDQUFDRCxFQUFFLEdBQUdBLEVBQUU7O01BRVo7TUFDQSxJQUFJLENBQUN5QixLQUFLLEdBQUdTLFFBQVEsQ0FBQ1QsS0FBSyxJQUFJLElBQUk7TUFDbkMsSUFBSSxDQUFDVSxTQUFTLEdBQUdELFFBQVEsQ0FBQ0UsU0FBUyxJQUFJLENBQUM7TUFDeEMsSUFBSSxDQUFDZCxLQUFLLEdBQUdZLFFBQVEsQ0FBQ0csV0FBVztNQUNqQyxJQUFJLENBQUNDLFNBQVMsR0FBRyxJQUFJQyxHQUFHLEVBQUU7O01BRTFCO01BQ0EsSUFBSSxDQUFDQyxrQkFBa0IsR0FBR04sUUFBUSxDQUFDTSxrQkFBa0I7TUFDckQsSUFBSSxDQUFDQyxNQUFNLEdBQUdQLFFBQVEsQ0FBQ08sTUFBTSxJQUFJLENBQUMsQ0FBQzs7TUFFbkM7TUFDQTtNQUNBO01BQ0E3QyxFQUFFLENBQUM4QyxjQUFjLENBQUMxQyxFQUFFLEVBQUVrQyxRQUFRLENBQUNTLFdBQVcsSUFBSSxZQUFZLEVBQUdDLEtBQUssSUFBSztRQUNyRSxNQUFNO1VBQUVDO1FBQU8sQ0FBQyxHQUFHRCxLQUFLLENBQUNqQyxPQUFPO1FBRWhDLElBQUlWLElBQUksQ0FBQzRDLE1BQU0sQ0FBQyxFQUFFO1VBQ2hCLElBQUksQ0FBQ1AsU0FBUyxDQUFDUSxHQUFHLENBQUNELE1BQU0sRUFBRSxJQUFJL0MsUUFBUSxDQUFDOEMsS0FBSyxFQUFFM0MsSUFBSSxDQUFDNEMsTUFBTSxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDckU7TUFDRixDQUFDLENBQUM7TUFFRixJQUFJWCxRQUFRLENBQUNhLEdBQUcsRUFBRTtRQUNoQixNQUFNQyxLQUFLLEdBQUdoRCxFQUFFLENBQUNNLHNCQUFzQixDQUFDLDBCQUEwQixDQUFDLENBQUMsQ0FBQyxDQUFDO1FBRXRFLElBQUkwQyxLQUFLLEVBQUU7VUFDVEEsS0FBSyxDQUFDM0IsS0FBSyxDQUFDNEIsS0FBSyxHQUFHZixRQUFRLENBQUNnQixRQUFRLElBQUksTUFBTTtVQUMvQ0YsS0FBSyxDQUFDM0IsS0FBSyxDQUFDOEIsTUFBTSxHQUFHakIsUUFBUSxDQUFDa0IsU0FBUyxJQUFJLE9BQU87VUFDbEQsSUFBSSxDQUFDTCxHQUFHLEdBQUdsRCxNQUFNLENBQUN3RCxTQUFTLENBQUNMLEtBQUssRUFBRWQsUUFBUSxDQUFDYSxHQUFHLEVBQUViLFFBQVEsQ0FBQ29CLE9BQU8sQ0FBQztVQUVsRSxJQUFJLElBQUksQ0FBQ2IsTUFBTSxDQUFDYyxXQUFXLEVBQUU7WUFDM0IsSUFBSSxDQUFDZCxNQUFNLENBQUNjLFdBQVcsQ0FBQ3hDLE9BQU8sQ0FBRXlDLEVBQUUsSUFBS0EsRUFBRSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUNULEdBQUcsQ0FBQyxDQUFDO1VBQzdEO1VBRUEsSUFBSSxDQUFDVSxlQUFlLEVBQUU7UUFDeEI7TUFDRjtJQUNGOztJQUVBO0FBQ0o7QUFDQTtJQUNJQSxlQUFlLEdBQUc7TUFDaEIsSUFBSSxDQUFDLElBQUksQ0FBQ1YsR0FBRyxFQUFFO01BRWYsSUFBSSxDQUFDVCxTQUFTLENBQUN2QixPQUFPLENBQUUyQyxHQUFHLElBQUs7UUFDOUIsSUFBSUEsR0FBRyxDQUFDdkMsTUFBTSxJQUFJLENBQUN1QyxHQUFHLENBQUN0RCxNQUFNLEVBQUU7VUFDN0JzRCxHQUFHLENBQUN0RCxNQUFNLEdBQUcsSUFBSSxDQUFDMkMsR0FBRyxDQUFDWSxZQUFZLENBQUM7WUFBRXhDLE1BQU0sRUFBRXVDLEdBQUcsQ0FBQ3ZDO1VBQU8sQ0FBQyxFQUFFLEtBQUssQ0FBQzs7VUFFakU7VUFDQTtVQUNBLElBQUksSUFBSSxDQUFDc0IsTUFBTSxDQUFDbUIsY0FBYyxFQUFFO1lBQzlCLElBQUksQ0FBQ25CLE1BQU0sQ0FBQ21CLGNBQWMsQ0FBQzdDLE9BQU8sQ0FBRXlDLEVBQUUsSUFBS0EsRUFBRSxDQUFDLElBQUksRUFBRUUsR0FBRyxFQUFFQSxHQUFHLENBQUN0RCxNQUFNLENBQUMsQ0FBQztVQUN2RTs7VUFFQTtVQUNBO1VBQ0EsSUFBSSxJQUFJLENBQUNvQyxrQkFBa0IsWUFBWXFCLFFBQVEsRUFBRTtZQUMvQ0gsR0FBRyxDQUFDdEQsTUFBTSxDQUFDMEQsRUFBRSxDQUFDLE9BQU8sRUFBRSxNQUFNO2NBQzNCLE1BQU1DLE9BQU8sR0FBRyxJQUFJLENBQUN2QixrQkFBa0IsQ0FBQyxJQUFJLEVBQUVrQixHQUFHLENBQUM7Y0FDbEQsSUFBSUssT0FBTyxFQUFFLElBQUksQ0FBQ2hCLEdBQUcsQ0FBQ2lCLGVBQWUsQ0FBQ04sR0FBRyxDQUFDdEQsTUFBTSxFQUFFMkQsT0FBTyxDQUFDO1lBQzVELENBQUMsQ0FBQztVQUNKO1FBQ0Y7TUFDRixDQUFDLENBQUM7SUFDSjs7SUFFQTtBQUNKO0FBQ0E7SUFDSUUsT0FBTyxHQUFHO01BQ1IsSUFBSSxJQUFJLENBQUNsQixHQUFHLEVBQUU7UUFDWixJQUFJLElBQUksQ0FBQ04sTUFBTSxDQUFDeUIsWUFBWSxFQUFFO1VBQzVCLElBQUksQ0FBQ3pCLE1BQU0sQ0FBQ3lCLFlBQVksQ0FBQ25ELE9BQU8sQ0FBRXlDLEVBQUUsSUFBS0EsRUFBRSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUNULEdBQUcsQ0FBQyxDQUFDO1FBQzlEO1FBRUEsSUFBSSxDQUFDQSxHQUFHLENBQUNrQixPQUFPLEVBQUU7TUFDcEI7SUFDRjtFQUNGOztFQUVBO0FBQ0Y7QUFDQTtFQUNFRSxNQUFNLENBQUNDLFNBQVMsQ0FBQ0MsZUFBZSxHQUFHO0lBQ2pDQyxZQUFZLEVBQUUscUNBQXFDO0lBQ25EQyxLQUFLLEVBQUUsSUFBSWhDLEdBQUcsRUFBRTtJQUVoQmlDLE1BQU0sQ0FBQ0MsT0FBTyxFQUFFdkMsUUFBUSxFQUFFO01BQ3hCLE1BQU13QyxTQUFTLEdBQUcsQ0FBQ3hDLFFBQVEsQ0FBQ3lDLFdBQVcsSUFBSSxDQUFDLENBQUMsRUFBRUMsUUFBUTs7TUFFdkQ7TUFDQWhGLEVBQUUsQ0FBQ2MsV0FBVyxDQUFDK0QsT0FBTyxFQUFFLGNBQWMsRUFBR3pFLEVBQUUsSUFBSztRQUM5QyxNQUFNRSxJQUFJLEdBQUdGLEVBQUUsQ0FBQ2MsT0FBTyxDQUFDLE9BQU8sQ0FBQztRQUNoQyxNQUFNK0QsS0FBSyxHQUFHM0UsSUFBSSxHQUFHLElBQUksQ0FBQ29FLFlBQVksQ0FBQ1EsSUFBSSxDQUFDNUUsSUFBSSxDQUFDNkUsWUFBWSxDQUFDLE9BQU8sQ0FBQyxDQUFDLEdBQUcsRUFBRTtRQUU1RSxJQUFJRixLQUFLLElBQUlILFNBQVMsQ0FBQ0csS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUU7VUFDaEMsTUFBTTVFLElBQUksR0FBR3lFLFNBQVMsQ0FBQ0csS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUN2QyxTQUFTO1VBQzFDLE1BQU0wQyxNQUFNLEdBQUduRixNQUFNLENBQUNtRixNQUFNLENBQUNDLGNBQWMsQ0FBQ1AsU0FBUyxDQUFDRyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztVQUNoRSxJQUFJLENBQUNOLEtBQUssQ0FBQ3pCLEdBQUcsQ0FBQytCLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBRSxJQUFJNUMsa0JBQWtCLENBQUNqQyxFQUFFLEVBQUVnRixNQUFNLEVBQUUvRSxJQUFJLENBQUMsQ0FBQztRQUNwRTtNQUNGLENBQUMsRUFBRSx1QkFBdUIsQ0FBQztJQUM3QixDQUFDO0lBRURpRixNQUFNLENBQUNULE9BQU8sRUFBRXZDLFFBQVEsRUFBRWlELE9BQU8sRUFBRTtNQUNqQyxJQUFJQSxPQUFPLEtBQUssUUFBUSxFQUFFOztNQUUxQjtNQUNBO01BQ0F2RixFQUFFLENBQUNjLFdBQVcsQ0FBQytELE9BQU8sRUFBRSxjQUFjLEVBQUd6RSxFQUFFLElBQUs7UUFDOUMsTUFBTUUsSUFBSSxHQUFHRixFQUFFLENBQUNjLE9BQU8sQ0FBQyxPQUFPLENBQUM7UUFDaEMsTUFBTStELEtBQUssR0FBRzNFLElBQUksR0FBRyxJQUFJLENBQUNvRSxZQUFZLENBQUNRLElBQUksQ0FBQzVFLElBQUksQ0FBQzZFLFlBQVksQ0FBQyxPQUFPLENBQUMsQ0FBQyxHQUFHLElBQUk7UUFDOUUsTUFBTUgsUUFBUSxHQUFHQyxLQUFLLEdBQUcsSUFBSSxDQUFDTixLQUFLLENBQUNhLEdBQUcsQ0FBQ1AsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSTtRQUV4RCxJQUFJRCxRQUFRLEVBQUU7VUFDWixJQUFJLENBQUNMLEtBQUssQ0FBQ2MsTUFBTSxDQUFDUixLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7VUFDM0JELFFBQVEsQ0FBQ1gsT0FBTyxFQUFFO1FBQ3BCO01BQ0YsQ0FBQyxDQUFDO0lBQ0o7RUFDRixDQUFDO0FBQ0gsQ0FBQyxFQUFFRSxNQUFNLEVBQUVtQixjQUFjLENBQUMifQ==
