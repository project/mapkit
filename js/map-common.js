"use strict";

Drupal.Mapkit = {} || Drupal.Mapkit;
(({
  Mapkit
}) => {
  /**
   * Map generator functions, keyed by the map plugin ID.
   *
   * The generator function should create a Map object that implements the
   * following prototype (still in progress). The map parameters should accept
   * common Mapkit marker settings, and allow the marker builder (Mapkit.marker)
   * handle the implementation and API specifics.
   *
   * See Gmap in mapkit_gmap/assets/gmap.es6.js from the mapkit_gmap module.
   *
   * Map.prototype = {
   *   getCenter(),
   *   setCenter({ lat, lng }),
   *   addMarker(markerObj),
   *   removeMarker(markerObj),
   * }
   *
   * @todo Write better documentation on map class prototypes.
   */
  Mapkit.handler = {};

  /**
   * Creates a Marker factory function, which creates the markers based on the
   * marker set configurations. The MarkerFactory is a function which generates
   * Marker objects that implement the following prototype.
   *
   * Marker.prototype = {
   *   get position(),
   *   set position({ lat, lng }),
   *   getHtml(),
   *   attach(map),
   *   detach(),
   *   on(event, callback),
   *   off(event, callback=),
   * }
   */
  Mapkit.marker = {};

  /**
   * Creates a map with map definition settings. Settings should contain info
   * about the Map type and options, and information about what map marker sets
   * are allowed for the map.
   *
   * @param {HTMLElement} el
   *   The HTML element to use place the generate map instance into.
   * @param {Object} mapDef
   *   The map definition settings, which must include a valid Mapkit handler
   *   map type. The map determines what kind of map get generated and what
   *   configurations are needed.
   * @param {Array|Object} markers
   *   Marker configurations that are used with this map.
   *
   * @return {Object}
   *   The generated map instance, build from the provided configurations.
   */
  Mapkit.createMap = function createMap(el, mapDef, markers) {
    const plugin = Mapkit.handler[mapDef.type] || false;
    if (plugin) {
      const mb = {};

      // Create the marker builders that are configured with this map.
      if (markers) {
        if (!Array.isArray(markers)) {
          markers = [markers];
        }
        markers.forEach(set => {
          if (Mapkit.marker[set.type]) {
            if (!mapDef.defaultMarkerSet) mapDef.defaultMarkerSet = set.type;
            mb[set.type] = Mapkit.marker[set.type](set);
          }
        });
      }
      return plugin(el, mapDef, mb);
    }
  };

  /**
   * Manage mapkit configurations and support overriding of map functionality.
   */
  Mapkit.config = {
    events: ['onMapCreate', 'onMapDestory', 'onMarkerCreate'],
    mergeConfig(base, overrides) {
      // Split the configurations into different settings based on how they
      // should be merged together.
      //  - Events are each arrays of callbacks, which should be additive.
      //  - Map settings are merge together.
      //  - All other configs are directly overridden.
      const {
        events,
        map,
        ...rest
      } = overrides;
      const config = {
        ...base,
        ...rest
      };

      // Merge the map settings.
      if (map) config.map = {
        ...base.map,
        ...map
      };

      // Build a entirely new events object so a shallow copy of event handlers
      // doesn't overwrite the original configuration object unintentionally.
      if (events) {
        config.events = {};
        const currEvents = base.events || {};
        this.events.forEach(eName => {
          if (events[eName]) {
            config.events[eName] = [...(currEvents[eName] || []), ...events[eName]];
          }
        });
      }
      return config;
    },
    /**
     * Apply any settings overrides and additional event listeners from global
     * settings or instance specific overrides.
     *
     * @see MapkitLocationView class for settings and events.
     *
     * @param {object} config
     *   The original configuration from the instance being build.
     *
     * @return {object}
     *   A new configuration object, with the overrides applied.
     */
    applyOverrides(config) {
      const settings = drupalSettings.mapkitViews || {};
      let result = config;

      // Global settings for all mapkit location views style.
      if (settings.globalSettings) {
        result = this.mergeConfig(config, settings.globalSettings);
      }

      // If this configuration is for a view.
      const {
        viewId,
        displayId
      } = config;
      const overrides = viewId && settings.view ? settings.view[viewId] : null;
      if (overrides) {
        // Apply any settings override for the view, if any are available.
        if (overrides.settings) {
          result = this.mergeConfig(result, overrides.settings);
        }

        // If there are display specific overrides, apply them after any of the
        // view specific overrides. This allows display specific settings to
        // take precedence.
        if (displayId && (overrides.display || {})[displayId]) {
          result = this.mergeCOnfig(result, overrides.display[displayId]);
        }
      }
      return result;
    },
    addOverrides(overrides, scope = 'global', id = '') {
      drupalSettings.mapkitViews = drupalSettings.mapkitViews || {};
      const settings = drupalSettings.mapkitViews;
      switch (scope) {
        case 'global':
          settings.globalSettings = this.mergeConfig(settings.globalSettings || {}, overrides);
          break;
        case 'view':
          if (id) {
            const [view, display] = id.split(':');
            settings.view = settings.view || {};
            settings.view[view] = settings.view[view] || {};
            const viewConf = settings.view[view];
            if (display) {
              viewConf.display = viewConf.display || {};
              viewConf.display[display] = this.mergeConfig(viewConf.display[display], overrides);
            } else {
              viewConf.settings = this.mergeConfig(viewConf.settings, overrides);
            }
          }
          break;
        default:
          throw new Error('Override scope must be either "view" or "global".');
      }
    }
  };

  /**
   * Convert degrees to radians.
   *
   * @param {Number} deg
   *   The value in degrees.
   *
   * @return {Number}
   *   The value converted to radians.
   */
  Mapkit.deg2Rad = function deg2Rad(deg) {
    return deg * Math.PI / 180;
  };

  /**
   * Haversine formula to calculate distance in of two points on Earth.
   *
   * @param {Object} from
   *   A location object with lat and lng.
   * @param {Object} to
   *   A location object with lat and lng.
   * @param {string} unit
   *   The distance unit to use in the result. Use 'mi' or 'mile(s)' to get
   *   the results in miles, otherwise, the distance will be in kilometers.
   *
   * @return {Number}
   *   The calculated distance between two points on Earth. NaN is returned
   *   when a coordinate is invalid.
   */
  Mapkit.calcDist = function calcDist(from, to, unit = 'km') {
    // Mean radius of Earth in miles or km.
    const r = /^mi(les?)?/i.test(unit) ? 3963.1676 : 6378.1;

    // If any coordinates are missing a component, then we won't be able to
    // calculate distances, so just return "NaN".
    if (!(from.lat && from.lng && to.lat && to.lng)) {
      return NaN;
    }
    const lat1 = this.deg2Rad(from.lat);
    const lat2 = this.deg2Rad(to.lat);
    const b = Math.sin((lat1 - lat2) / 2);
    const c = Math.sin(this.deg2Rad(from.lng - to.lng) / 2);

    // Compute the arc length between the Earth coordinates.
    const a = b * b + Math.cos(lat1) * Math.cos(lat2) * (c * c);
    const arc = 2 * Math.asin(Math.sqrt(a));

    // Arc length times the radius of the Earth is the distance.
    return arc * r;
  };
})(Drupal);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFwLWNvbW1vbi5qcyIsIm5hbWVzIjpbIkRydXBhbCIsIk1hcGtpdCIsImhhbmRsZXIiLCJtYXJrZXIiLCJjcmVhdGVNYXAiLCJlbCIsIm1hcERlZiIsIm1hcmtlcnMiLCJwbHVnaW4iLCJ0eXBlIiwibWIiLCJBcnJheSIsImlzQXJyYXkiLCJmb3JFYWNoIiwic2V0IiwiZGVmYXVsdE1hcmtlclNldCIsImNvbmZpZyIsImV2ZW50cyIsIm1lcmdlQ29uZmlnIiwiYmFzZSIsIm92ZXJyaWRlcyIsIm1hcCIsInJlc3QiLCJjdXJyRXZlbnRzIiwiZU5hbWUiLCJhcHBseU92ZXJyaWRlcyIsInNldHRpbmdzIiwiZHJ1cGFsU2V0dGluZ3MiLCJtYXBraXRWaWV3cyIsInJlc3VsdCIsImdsb2JhbFNldHRpbmdzIiwidmlld0lkIiwiZGlzcGxheUlkIiwidmlldyIsImRpc3BsYXkiLCJtZXJnZUNPbmZpZyIsImFkZE92ZXJyaWRlcyIsInNjb3BlIiwiaWQiLCJzcGxpdCIsInZpZXdDb25mIiwiRXJyb3IiLCJkZWcyUmFkIiwiZGVnIiwiTWF0aCIsIlBJIiwiY2FsY0Rpc3QiLCJmcm9tIiwidG8iLCJ1bml0IiwiciIsInRlc3QiLCJsYXQiLCJsbmciLCJOYU4iLCJsYXQxIiwibGF0MiIsImIiLCJzaW4iLCJjIiwiYSIsImNvcyIsImFyYyIsImFzaW4iLCJzcXJ0Il0sInNvdXJjZXMiOlsibWFwLWNvbW1vbi5lczYuanMiXSwic291cmNlc0NvbnRlbnQiOlsiRHJ1cGFsLk1hcGtpdCA9IHt9IHx8IERydXBhbC5NYXBraXQ7XG5cbigoeyBNYXBraXQgfSkgPT4ge1xuICAvKipcbiAgICogTWFwIGdlbmVyYXRvciBmdW5jdGlvbnMsIGtleWVkIGJ5IHRoZSBtYXAgcGx1Z2luIElELlxuICAgKlxuICAgKiBUaGUgZ2VuZXJhdG9yIGZ1bmN0aW9uIHNob3VsZCBjcmVhdGUgYSBNYXAgb2JqZWN0IHRoYXQgaW1wbGVtZW50cyB0aGVcbiAgICogZm9sbG93aW5nIHByb3RvdHlwZSAoc3RpbGwgaW4gcHJvZ3Jlc3MpLiBUaGUgbWFwIHBhcmFtZXRlcnMgc2hvdWxkIGFjY2VwdFxuICAgKiBjb21tb24gTWFwa2l0IG1hcmtlciBzZXR0aW5ncywgYW5kIGFsbG93IHRoZSBtYXJrZXIgYnVpbGRlciAoTWFwa2l0Lm1hcmtlcilcbiAgICogaGFuZGxlIHRoZSBpbXBsZW1lbnRhdGlvbiBhbmQgQVBJIHNwZWNpZmljcy5cbiAgICpcbiAgICogU2VlIEdtYXAgaW4gbWFwa2l0X2dtYXAvYXNzZXRzL2dtYXAuZXM2LmpzIGZyb20gdGhlIG1hcGtpdF9nbWFwIG1vZHVsZS5cbiAgICpcbiAgICogTWFwLnByb3RvdHlwZSA9IHtcbiAgICogICBnZXRDZW50ZXIoKSxcbiAgICogICBzZXRDZW50ZXIoeyBsYXQsIGxuZyB9KSxcbiAgICogICBhZGRNYXJrZXIobWFya2VyT2JqKSxcbiAgICogICByZW1vdmVNYXJrZXIobWFya2VyT2JqKSxcbiAgICogfVxuICAgKlxuICAgKiBAdG9kbyBXcml0ZSBiZXR0ZXIgZG9jdW1lbnRhdGlvbiBvbiBtYXAgY2xhc3MgcHJvdG90eXBlcy5cbiAgICovXG4gIE1hcGtpdC5oYW5kbGVyID0ge307XG5cbiAgLyoqXG4gICAqIENyZWF0ZXMgYSBNYXJrZXIgZmFjdG9yeSBmdW5jdGlvbiwgd2hpY2ggY3JlYXRlcyB0aGUgbWFya2VycyBiYXNlZCBvbiB0aGVcbiAgICogbWFya2VyIHNldCBjb25maWd1cmF0aW9ucy4gVGhlIE1hcmtlckZhY3RvcnkgaXMgYSBmdW5jdGlvbiB3aGljaCBnZW5lcmF0ZXNcbiAgICogTWFya2VyIG9iamVjdHMgdGhhdCBpbXBsZW1lbnQgdGhlIGZvbGxvd2luZyBwcm90b3R5cGUuXG4gICAqXG4gICAqIE1hcmtlci5wcm90b3R5cGUgPSB7XG4gICAqICAgZ2V0IHBvc2l0aW9uKCksXG4gICAqICAgc2V0IHBvc2l0aW9uKHsgbGF0LCBsbmcgfSksXG4gICAqICAgZ2V0SHRtbCgpLFxuICAgKiAgIGF0dGFjaChtYXApLFxuICAgKiAgIGRldGFjaCgpLFxuICAgKiAgIG9uKGV2ZW50LCBjYWxsYmFjayksXG4gICAqICAgb2ZmKGV2ZW50LCBjYWxsYmFjaz0pLFxuICAgKiB9XG4gICAqL1xuICBNYXBraXQubWFya2VyID0ge307XG5cbiAgLyoqXG4gICAqIENyZWF0ZXMgYSBtYXAgd2l0aCBtYXAgZGVmaW5pdGlvbiBzZXR0aW5ncy4gU2V0dGluZ3Mgc2hvdWxkIGNvbnRhaW4gaW5mb1xuICAgKiBhYm91dCB0aGUgTWFwIHR5cGUgYW5kIG9wdGlvbnMsIGFuZCBpbmZvcm1hdGlvbiBhYm91dCB3aGF0IG1hcCBtYXJrZXIgc2V0c1xuICAgKiBhcmUgYWxsb3dlZCBmb3IgdGhlIG1hcC5cbiAgICpcbiAgICogQHBhcmFtIHtIVE1MRWxlbWVudH0gZWxcbiAgICogICBUaGUgSFRNTCBlbGVtZW50IHRvIHVzZSBwbGFjZSB0aGUgZ2VuZXJhdGUgbWFwIGluc3RhbmNlIGludG8uXG4gICAqIEBwYXJhbSB7T2JqZWN0fSBtYXBEZWZcbiAgICogICBUaGUgbWFwIGRlZmluaXRpb24gc2V0dGluZ3MsIHdoaWNoIG11c3QgaW5jbHVkZSBhIHZhbGlkIE1hcGtpdCBoYW5kbGVyXG4gICAqICAgbWFwIHR5cGUuIFRoZSBtYXAgZGV0ZXJtaW5lcyB3aGF0IGtpbmQgb2YgbWFwIGdldCBnZW5lcmF0ZWQgYW5kIHdoYXRcbiAgICogICBjb25maWd1cmF0aW9ucyBhcmUgbmVlZGVkLlxuICAgKiBAcGFyYW0ge0FycmF5fE9iamVjdH0gbWFya2Vyc1xuICAgKiAgIE1hcmtlciBjb25maWd1cmF0aW9ucyB0aGF0IGFyZSB1c2VkIHdpdGggdGhpcyBtYXAuXG4gICAqXG4gICAqIEByZXR1cm4ge09iamVjdH1cbiAgICogICBUaGUgZ2VuZXJhdGVkIG1hcCBpbnN0YW5jZSwgYnVpbGQgZnJvbSB0aGUgcHJvdmlkZWQgY29uZmlndXJhdGlvbnMuXG4gICAqL1xuICBNYXBraXQuY3JlYXRlTWFwID0gZnVuY3Rpb24gY3JlYXRlTWFwKGVsLCBtYXBEZWYsIG1hcmtlcnMpIHtcbiAgICBjb25zdCBwbHVnaW4gPSBNYXBraXQuaGFuZGxlclttYXBEZWYudHlwZV0gfHwgZmFsc2U7XG5cbiAgICBpZiAocGx1Z2luKSB7XG4gICAgICBjb25zdCBtYiA9IHt9O1xuXG4gICAgICAvLyBDcmVhdGUgdGhlIG1hcmtlciBidWlsZGVycyB0aGF0IGFyZSBjb25maWd1cmVkIHdpdGggdGhpcyBtYXAuXG4gICAgICBpZiAobWFya2Vycykge1xuICAgICAgICBpZiAoIUFycmF5LmlzQXJyYXkobWFya2VycykpIHtcbiAgICAgICAgICBtYXJrZXJzID0gW21hcmtlcnNdO1xuICAgICAgICB9XG5cbiAgICAgICAgbWFya2Vycy5mb3JFYWNoKChzZXQpID0+IHtcbiAgICAgICAgICBpZiAoTWFwa2l0Lm1hcmtlcltzZXQudHlwZV0pIHtcbiAgICAgICAgICAgIGlmICghbWFwRGVmLmRlZmF1bHRNYXJrZXJTZXQpIG1hcERlZi5kZWZhdWx0TWFya2VyU2V0ID0gc2V0LnR5cGU7XG5cbiAgICAgICAgICAgIG1iW3NldC50eXBlXSA9IE1hcGtpdC5tYXJrZXJbc2V0LnR5cGVdKHNldCk7XG4gICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHBsdWdpbihlbCwgbWFwRGVmLCBtYik7XG4gICAgfVxuICB9O1xuXG4gIC8qKlxuICAgKiBNYW5hZ2UgbWFwa2l0IGNvbmZpZ3VyYXRpb25zIGFuZCBzdXBwb3J0IG92ZXJyaWRpbmcgb2YgbWFwIGZ1bmN0aW9uYWxpdHkuXG4gICAqL1xuICBNYXBraXQuY29uZmlnID0ge1xuICAgIGV2ZW50czogW1xuICAgICAgJ29uTWFwQ3JlYXRlJyxcbiAgICAgICdvbk1hcERlc3RvcnknLFxuICAgICAgJ29uTWFya2VyQ3JlYXRlJyxcbiAgICBdLFxuXG4gICAgbWVyZ2VDb25maWcoYmFzZSwgb3ZlcnJpZGVzKSB7XG4gICAgICAvLyBTcGxpdCB0aGUgY29uZmlndXJhdGlvbnMgaW50byBkaWZmZXJlbnQgc2V0dGluZ3MgYmFzZWQgb24gaG93IHRoZXlcbiAgICAgIC8vIHNob3VsZCBiZSBtZXJnZWQgdG9nZXRoZXIuXG4gICAgICAvLyAgLSBFdmVudHMgYXJlIGVhY2ggYXJyYXlzIG9mIGNhbGxiYWNrcywgd2hpY2ggc2hvdWxkIGJlIGFkZGl0aXZlLlxuICAgICAgLy8gIC0gTWFwIHNldHRpbmdzIGFyZSBtZXJnZSB0b2dldGhlci5cbiAgICAgIC8vICAtIEFsbCBvdGhlciBjb25maWdzIGFyZSBkaXJlY3RseSBvdmVycmlkZGVuLlxuICAgICAgY29uc3QgeyBldmVudHMsIG1hcCwgLi4ucmVzdCB9ID0gb3ZlcnJpZGVzO1xuICAgICAgY29uc3QgY29uZmlnID0geyAuLi5iYXNlLCAuLi5yZXN0IH07XG5cbiAgICAgIC8vIE1lcmdlIHRoZSBtYXAgc2V0dGluZ3MuXG4gICAgICBpZiAobWFwKSBjb25maWcubWFwID0geyAuLi5iYXNlLm1hcCwgLi4ubWFwIH07XG5cbiAgICAgIC8vIEJ1aWxkIGEgZW50aXJlbHkgbmV3IGV2ZW50cyBvYmplY3Qgc28gYSBzaGFsbG93IGNvcHkgb2YgZXZlbnQgaGFuZGxlcnNcbiAgICAgIC8vIGRvZXNuJ3Qgb3ZlcndyaXRlIHRoZSBvcmlnaW5hbCBjb25maWd1cmF0aW9uIG9iamVjdCB1bmludGVudGlvbmFsbHkuXG4gICAgICBpZiAoZXZlbnRzKSB7XG4gICAgICAgIGNvbmZpZy5ldmVudHMgPSB7fTtcbiAgICAgICAgY29uc3QgY3VyckV2ZW50cyA9IGJhc2UuZXZlbnRzIHx8IHt9O1xuICAgICAgICB0aGlzLmV2ZW50cy5mb3JFYWNoKChlTmFtZSkgPT4ge1xuICAgICAgICAgIGlmIChldmVudHNbZU5hbWVdKSB7XG4gICAgICAgICAgICBjb25maWcuZXZlbnRzW2VOYW1lXSA9IFtcbiAgICAgICAgICAgICAgLi4uKGN1cnJFdmVudHNbZU5hbWVdIHx8IFtdKSxcbiAgICAgICAgICAgICAgLi4uZXZlbnRzW2VOYW1lXSxcbiAgICAgICAgICAgIF07XG4gICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIGNvbmZpZztcbiAgICB9LFxuXG4gICAgLyoqXG4gICAgICogQXBwbHkgYW55IHNldHRpbmdzIG92ZXJyaWRlcyBhbmQgYWRkaXRpb25hbCBldmVudCBsaXN0ZW5lcnMgZnJvbSBnbG9iYWxcbiAgICAgKiBzZXR0aW5ncyBvciBpbnN0YW5jZSBzcGVjaWZpYyBvdmVycmlkZXMuXG4gICAgICpcbiAgICAgKiBAc2VlIE1hcGtpdExvY2F0aW9uVmlldyBjbGFzcyBmb3Igc2V0dGluZ3MgYW5kIGV2ZW50cy5cbiAgICAgKlxuICAgICAqIEBwYXJhbSB7b2JqZWN0fSBjb25maWdcbiAgICAgKiAgIFRoZSBvcmlnaW5hbCBjb25maWd1cmF0aW9uIGZyb20gdGhlIGluc3RhbmNlIGJlaW5nIGJ1aWxkLlxuICAgICAqXG4gICAgICogQHJldHVybiB7b2JqZWN0fVxuICAgICAqICAgQSBuZXcgY29uZmlndXJhdGlvbiBvYmplY3QsIHdpdGggdGhlIG92ZXJyaWRlcyBhcHBsaWVkLlxuICAgICAqL1xuICAgIGFwcGx5T3ZlcnJpZGVzKGNvbmZpZykge1xuICAgICAgY29uc3Qgc2V0dGluZ3MgPSBkcnVwYWxTZXR0aW5ncy5tYXBraXRWaWV3cyB8fCB7fTtcbiAgICAgIGxldCByZXN1bHQgPSBjb25maWc7XG5cbiAgICAgIC8vIEdsb2JhbCBzZXR0aW5ncyBmb3IgYWxsIG1hcGtpdCBsb2NhdGlvbiB2aWV3cyBzdHlsZS5cbiAgICAgIGlmIChzZXR0aW5ncy5nbG9iYWxTZXR0aW5ncykge1xuICAgICAgICByZXN1bHQgPSB0aGlzLm1lcmdlQ29uZmlnKGNvbmZpZywgc2V0dGluZ3MuZ2xvYmFsU2V0dGluZ3MpO1xuICAgICAgfVxuXG4gICAgICAvLyBJZiB0aGlzIGNvbmZpZ3VyYXRpb24gaXMgZm9yIGEgdmlldy5cbiAgICAgIGNvbnN0IHsgdmlld0lkLCBkaXNwbGF5SWQgfSA9IGNvbmZpZztcbiAgICAgIGNvbnN0IG92ZXJyaWRlcyA9IHZpZXdJZCAmJiBzZXR0aW5ncy52aWV3ID8gc2V0dGluZ3Mudmlld1t2aWV3SWRdIDogbnVsbDtcbiAgICAgIGlmIChvdmVycmlkZXMpIHtcbiAgICAgICAgLy8gQXBwbHkgYW55IHNldHRpbmdzIG92ZXJyaWRlIGZvciB0aGUgdmlldywgaWYgYW55IGFyZSBhdmFpbGFibGUuXG4gICAgICAgIGlmIChvdmVycmlkZXMuc2V0dGluZ3MpIHtcbiAgICAgICAgICByZXN1bHQgPSB0aGlzLm1lcmdlQ29uZmlnKHJlc3VsdCwgb3ZlcnJpZGVzLnNldHRpbmdzKTtcbiAgICAgICAgfVxuXG4gICAgICAgIC8vIElmIHRoZXJlIGFyZSBkaXNwbGF5IHNwZWNpZmljIG92ZXJyaWRlcywgYXBwbHkgdGhlbSBhZnRlciBhbnkgb2YgdGhlXG4gICAgICAgIC8vIHZpZXcgc3BlY2lmaWMgb3ZlcnJpZGVzLiBUaGlzIGFsbG93cyBkaXNwbGF5IHNwZWNpZmljIHNldHRpbmdzIHRvXG4gICAgICAgIC8vIHRha2UgcHJlY2VkZW5jZS5cbiAgICAgICAgaWYgKGRpc3BsYXlJZCAmJiAob3ZlcnJpZGVzLmRpc3BsYXkgfHwge30pW2Rpc3BsYXlJZF0pIHtcbiAgICAgICAgICByZXN1bHQgPSB0aGlzLm1lcmdlQ09uZmlnKHJlc3VsdCwgb3ZlcnJpZGVzLmRpc3BsYXlbZGlzcGxheUlkXSk7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICB9LFxuXG4gICAgYWRkT3ZlcnJpZGVzKG92ZXJyaWRlcywgc2NvcGUgPSAnZ2xvYmFsJywgaWQgPSAnJykge1xuICAgICAgZHJ1cGFsU2V0dGluZ3MubWFwa2l0Vmlld3MgPSBkcnVwYWxTZXR0aW5ncy5tYXBraXRWaWV3cyB8fCB7fTtcbiAgICAgIGNvbnN0IHNldHRpbmdzID0gZHJ1cGFsU2V0dGluZ3MubWFwa2l0Vmlld3M7XG5cbiAgICAgIHN3aXRjaCAoc2NvcGUpIHtcbiAgICAgICAgY2FzZSAnZ2xvYmFsJzpcbiAgICAgICAgICBzZXR0aW5ncy5nbG9iYWxTZXR0aW5ncyA9IHRoaXMubWVyZ2VDb25maWcoc2V0dGluZ3MuZ2xvYmFsU2V0dGluZ3MgfHwge30sIG92ZXJyaWRlcyk7XG4gICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgY2FzZSAndmlldyc6XG4gICAgICAgICAgaWYgKGlkKSB7XG4gICAgICAgICAgICBjb25zdCBbdmlldywgZGlzcGxheV0gPSBpZC5zcGxpdCgnOicpO1xuICAgICAgICAgICAgc2V0dGluZ3MudmlldyA9IHNldHRpbmdzLnZpZXcgfHwge307XG4gICAgICAgICAgICBzZXR0aW5ncy52aWV3W3ZpZXddID0gc2V0dGluZ3Mudmlld1t2aWV3XSB8fCB7fTtcbiAgICAgICAgICAgIGNvbnN0IHZpZXdDb25mID0gc2V0dGluZ3Mudmlld1t2aWV3XTtcblxuICAgICAgICAgICAgaWYgKGRpc3BsYXkpIHtcbiAgICAgICAgICAgICAgdmlld0NvbmYuZGlzcGxheSA9IHZpZXdDb25mLmRpc3BsYXkgfHwge307XG4gICAgICAgICAgICAgIHZpZXdDb25mLmRpc3BsYXlbZGlzcGxheV0gPSB0aGlzLm1lcmdlQ29uZmlnKHZpZXdDb25mLmRpc3BsYXlbZGlzcGxheV0sIG92ZXJyaWRlcyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgdmlld0NvbmYuc2V0dGluZ3MgPSB0aGlzLm1lcmdlQ29uZmlnKHZpZXdDb25mLnNldHRpbmdzLCBvdmVycmlkZXMpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgICBicmVhaztcblxuICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgIHRocm93IG5ldyBFcnJvcignT3ZlcnJpZGUgc2NvcGUgbXVzdCBiZSBlaXRoZXIgXCJ2aWV3XCIgb3IgXCJnbG9iYWxcIi4nKTtcbiAgICAgIH1cbiAgICB9LFxuICB9O1xuXG4gIC8qKlxuICAgKiBDb252ZXJ0IGRlZ3JlZXMgdG8gcmFkaWFucy5cbiAgICpcbiAgICogQHBhcmFtIHtOdW1iZXJ9IGRlZ1xuICAgKiAgIFRoZSB2YWx1ZSBpbiBkZWdyZWVzLlxuICAgKlxuICAgKiBAcmV0dXJuIHtOdW1iZXJ9XG4gICAqICAgVGhlIHZhbHVlIGNvbnZlcnRlZCB0byByYWRpYW5zLlxuICAgKi9cbiAgTWFwa2l0LmRlZzJSYWQgPSBmdW5jdGlvbiBkZWcyUmFkKGRlZykge1xuICAgIHJldHVybiAoZGVnICogTWF0aC5QSSkgLyAxODA7XG4gIH07XG5cbiAgLyoqXG4gICAqIEhhdmVyc2luZSBmb3JtdWxhIHRvIGNhbGN1bGF0ZSBkaXN0YW5jZSBpbiBvZiB0d28gcG9pbnRzIG9uIEVhcnRoLlxuICAgKlxuICAgKiBAcGFyYW0ge09iamVjdH0gZnJvbVxuICAgKiAgIEEgbG9jYXRpb24gb2JqZWN0IHdpdGggbGF0IGFuZCBsbmcuXG4gICAqIEBwYXJhbSB7T2JqZWN0fSB0b1xuICAgKiAgIEEgbG9jYXRpb24gb2JqZWN0IHdpdGggbGF0IGFuZCBsbmcuXG4gICAqIEBwYXJhbSB7c3RyaW5nfSB1bml0XG4gICAqICAgVGhlIGRpc3RhbmNlIHVuaXQgdG8gdXNlIGluIHRoZSByZXN1bHQuIFVzZSAnbWknIG9yICdtaWxlKHMpJyB0byBnZXRcbiAgICogICB0aGUgcmVzdWx0cyBpbiBtaWxlcywgb3RoZXJ3aXNlLCB0aGUgZGlzdGFuY2Ugd2lsbCBiZSBpbiBraWxvbWV0ZXJzLlxuICAgKlxuICAgKiBAcmV0dXJuIHtOdW1iZXJ9XG4gICAqICAgVGhlIGNhbGN1bGF0ZWQgZGlzdGFuY2UgYmV0d2VlbiB0d28gcG9pbnRzIG9uIEVhcnRoLiBOYU4gaXMgcmV0dXJuZWRcbiAgICogICB3aGVuIGEgY29vcmRpbmF0ZSBpcyBpbnZhbGlkLlxuICAgKi9cbiAgTWFwa2l0LmNhbGNEaXN0ID0gZnVuY3Rpb24gY2FsY0Rpc3QoZnJvbSwgdG8sIHVuaXQgPSAna20nKSB7XG4gICAgLy8gTWVhbiByYWRpdXMgb2YgRWFydGggaW4gbWlsZXMgb3Iga20uXG4gICAgY29uc3QgciA9IC9ebWkobGVzPyk/L2kudGVzdCh1bml0KSA/IDM5NjMuMTY3NiA6IDYzNzguMTtcblxuICAgIC8vIElmIGFueSBjb29yZGluYXRlcyBhcmUgbWlzc2luZyBhIGNvbXBvbmVudCwgdGhlbiB3ZSB3b24ndCBiZSBhYmxlIHRvXG4gICAgLy8gY2FsY3VsYXRlIGRpc3RhbmNlcywgc28ganVzdCByZXR1cm4gXCJOYU5cIi5cbiAgICBpZiAoIShmcm9tLmxhdCAmJiBmcm9tLmxuZyAmJiB0by5sYXQgJiYgdG8ubG5nKSkge1xuICAgICAgcmV0dXJuIE5hTjtcbiAgICB9XG5cbiAgICBjb25zdCBsYXQxID0gdGhpcy5kZWcyUmFkKGZyb20ubGF0KTtcbiAgICBjb25zdCBsYXQyID0gdGhpcy5kZWcyUmFkKHRvLmxhdCk7XG4gICAgY29uc3QgYiA9IE1hdGguc2luKChsYXQxIC0gbGF0MikgLyAyKTtcbiAgICBjb25zdCBjID0gTWF0aC5zaW4odGhpcy5kZWcyUmFkKGZyb20ubG5nIC0gdG8ubG5nKSAvIDIpO1xuXG4gICAgLy8gQ29tcHV0ZSB0aGUgYXJjIGxlbmd0aCBiZXR3ZWVuIHRoZSBFYXJ0aCBjb29yZGluYXRlcy5cbiAgICBjb25zdCBhID0gKGIgKiBiKSArIE1hdGguY29zKGxhdDEpICogTWF0aC5jb3MobGF0MikgKiAoYyAqIGMpO1xuICAgIGNvbnN0IGFyYyA9IDIgKiBNYXRoLmFzaW4oTWF0aC5zcXJ0KGEpKTtcblxuICAgIC8vIEFyYyBsZW5ndGggdGltZXMgdGhlIHJhZGl1cyBvZiB0aGUgRWFydGggaXMgdGhlIGRpc3RhbmNlLlxuICAgIHJldHVybiBhcmMgKiByO1xuICB9O1xufSkoRHJ1cGFsKTtcbiJdLCJtYXBwaW5ncyI6Ijs7QUFBQUEsTUFBTSxDQUFDQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLElBQUlELE1BQU0sQ0FBQ0MsTUFBTTtBQUVuQyxDQUFDLENBQUM7RUFBRUE7QUFBTyxDQUFDLEtBQUs7RUFDZjtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtFQUNFQSxNQUFNLENBQUNDLE9BQU8sR0FBRyxDQUFDLENBQUM7O0VBRW5CO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtFQUNFRCxNQUFNLENBQUNFLE1BQU0sR0FBRyxDQUFDLENBQUM7O0VBRWxCO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7RUFDRUYsTUFBTSxDQUFDRyxTQUFTLEdBQUcsU0FBU0EsU0FBUyxDQUFDQyxFQUFFLEVBQUVDLE1BQU0sRUFBRUMsT0FBTyxFQUFFO0lBQ3pELE1BQU1DLE1BQU0sR0FBR1AsTUFBTSxDQUFDQyxPQUFPLENBQUNJLE1BQU0sQ0FBQ0csSUFBSSxDQUFDLElBQUksS0FBSztJQUVuRCxJQUFJRCxNQUFNLEVBQUU7TUFDVixNQUFNRSxFQUFFLEdBQUcsQ0FBQyxDQUFDOztNQUViO01BQ0EsSUFBSUgsT0FBTyxFQUFFO1FBQ1gsSUFBSSxDQUFDSSxLQUFLLENBQUNDLE9BQU8sQ0FBQ0wsT0FBTyxDQUFDLEVBQUU7VUFDM0JBLE9BQU8sR0FBRyxDQUFDQSxPQUFPLENBQUM7UUFDckI7UUFFQUEsT0FBTyxDQUFDTSxPQUFPLENBQUVDLEdBQUcsSUFBSztVQUN2QixJQUFJYixNQUFNLENBQUNFLE1BQU0sQ0FBQ1csR0FBRyxDQUFDTCxJQUFJLENBQUMsRUFBRTtZQUMzQixJQUFJLENBQUNILE1BQU0sQ0FBQ1MsZ0JBQWdCLEVBQUVULE1BQU0sQ0FBQ1MsZ0JBQWdCLEdBQUdELEdBQUcsQ0FBQ0wsSUFBSTtZQUVoRUMsRUFBRSxDQUFDSSxHQUFHLENBQUNMLElBQUksQ0FBQyxHQUFHUixNQUFNLENBQUNFLE1BQU0sQ0FBQ1csR0FBRyxDQUFDTCxJQUFJLENBQUMsQ0FBQ0ssR0FBRyxDQUFDO1VBQzdDO1FBQ0YsQ0FBQyxDQUFDO01BQ0o7TUFFQSxPQUFPTixNQUFNLENBQUNILEVBQUUsRUFBRUMsTUFBTSxFQUFFSSxFQUFFLENBQUM7SUFDL0I7RUFDRixDQUFDOztFQUVEO0FBQ0Y7QUFDQTtFQUNFVCxNQUFNLENBQUNlLE1BQU0sR0FBRztJQUNkQyxNQUFNLEVBQUUsQ0FDTixhQUFhLEVBQ2IsY0FBYyxFQUNkLGdCQUFnQixDQUNqQjtJQUVEQyxXQUFXLENBQUNDLElBQUksRUFBRUMsU0FBUyxFQUFFO01BQzNCO01BQ0E7TUFDQTtNQUNBO01BQ0E7TUFDQSxNQUFNO1FBQUVILE1BQU07UUFBRUksR0FBRztRQUFFLEdBQUdDO01BQUssQ0FBQyxHQUFHRixTQUFTO01BQzFDLE1BQU1KLE1BQU0sR0FBRztRQUFFLEdBQUdHLElBQUk7UUFBRSxHQUFHRztNQUFLLENBQUM7O01BRW5DO01BQ0EsSUFBSUQsR0FBRyxFQUFFTCxNQUFNLENBQUNLLEdBQUcsR0FBRztRQUFFLEdBQUdGLElBQUksQ0FBQ0UsR0FBRztRQUFFLEdBQUdBO01BQUksQ0FBQzs7TUFFN0M7TUFDQTtNQUNBLElBQUlKLE1BQU0sRUFBRTtRQUNWRCxNQUFNLENBQUNDLE1BQU0sR0FBRyxDQUFDLENBQUM7UUFDbEIsTUFBTU0sVUFBVSxHQUFHSixJQUFJLENBQUNGLE1BQU0sSUFBSSxDQUFDLENBQUM7UUFDcEMsSUFBSSxDQUFDQSxNQUFNLENBQUNKLE9BQU8sQ0FBRVcsS0FBSyxJQUFLO1VBQzdCLElBQUlQLE1BQU0sQ0FBQ08sS0FBSyxDQUFDLEVBQUU7WUFDakJSLE1BQU0sQ0FBQ0MsTUFBTSxDQUFDTyxLQUFLLENBQUMsR0FBRyxDQUNyQixJQUFJRCxVQUFVLENBQUNDLEtBQUssQ0FBQyxJQUFJLEVBQUUsQ0FBQyxFQUM1QixHQUFHUCxNQUFNLENBQUNPLEtBQUssQ0FBQyxDQUNqQjtVQUNIO1FBQ0YsQ0FBQyxDQUFDO01BQ0o7TUFFQSxPQUFPUixNQUFNO0lBQ2YsQ0FBQztJQUVEO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtJQUNJUyxjQUFjLENBQUNULE1BQU0sRUFBRTtNQUNyQixNQUFNVSxRQUFRLEdBQUdDLGNBQWMsQ0FBQ0MsV0FBVyxJQUFJLENBQUMsQ0FBQztNQUNqRCxJQUFJQyxNQUFNLEdBQUdiLE1BQU07O01BRW5CO01BQ0EsSUFBSVUsUUFBUSxDQUFDSSxjQUFjLEVBQUU7UUFDM0JELE1BQU0sR0FBRyxJQUFJLENBQUNYLFdBQVcsQ0FBQ0YsTUFBTSxFQUFFVSxRQUFRLENBQUNJLGNBQWMsQ0FBQztNQUM1RDs7TUFFQTtNQUNBLE1BQU07UUFBRUMsTUFBTTtRQUFFQztNQUFVLENBQUMsR0FBR2hCLE1BQU07TUFDcEMsTUFBTUksU0FBUyxHQUFHVyxNQUFNLElBQUlMLFFBQVEsQ0FBQ08sSUFBSSxHQUFHUCxRQUFRLENBQUNPLElBQUksQ0FBQ0YsTUFBTSxDQUFDLEdBQUcsSUFBSTtNQUN4RSxJQUFJWCxTQUFTLEVBQUU7UUFDYjtRQUNBLElBQUlBLFNBQVMsQ0FBQ00sUUFBUSxFQUFFO1VBQ3RCRyxNQUFNLEdBQUcsSUFBSSxDQUFDWCxXQUFXLENBQUNXLE1BQU0sRUFBRVQsU0FBUyxDQUFDTSxRQUFRLENBQUM7UUFDdkQ7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsSUFBSU0sU0FBUyxJQUFJLENBQUNaLFNBQVMsQ0FBQ2MsT0FBTyxJQUFJLENBQUMsQ0FBQyxFQUFFRixTQUFTLENBQUMsRUFBRTtVQUNyREgsTUFBTSxHQUFHLElBQUksQ0FBQ00sV0FBVyxDQUFDTixNQUFNLEVBQUVULFNBQVMsQ0FBQ2MsT0FBTyxDQUFDRixTQUFTLENBQUMsQ0FBQztRQUNqRTtNQUNGO01BRUEsT0FBT0gsTUFBTTtJQUNmLENBQUM7SUFFRE8sWUFBWSxDQUFDaEIsU0FBUyxFQUFFaUIsS0FBSyxHQUFHLFFBQVEsRUFBRUMsRUFBRSxHQUFHLEVBQUUsRUFBRTtNQUNqRFgsY0FBYyxDQUFDQyxXQUFXLEdBQUdELGNBQWMsQ0FBQ0MsV0FBVyxJQUFJLENBQUMsQ0FBQztNQUM3RCxNQUFNRixRQUFRLEdBQUdDLGNBQWMsQ0FBQ0MsV0FBVztNQUUzQyxRQUFRUyxLQUFLO1FBQ1gsS0FBSyxRQUFRO1VBQ1hYLFFBQVEsQ0FBQ0ksY0FBYyxHQUFHLElBQUksQ0FBQ1osV0FBVyxDQUFDUSxRQUFRLENBQUNJLGNBQWMsSUFBSSxDQUFDLENBQUMsRUFBRVYsU0FBUyxDQUFDO1VBQ3BGO1FBRUYsS0FBSyxNQUFNO1VBQ1QsSUFBSWtCLEVBQUUsRUFBRTtZQUNOLE1BQU0sQ0FBQ0wsSUFBSSxFQUFFQyxPQUFPLENBQUMsR0FBR0ksRUFBRSxDQUFDQyxLQUFLLENBQUMsR0FBRyxDQUFDO1lBQ3JDYixRQUFRLENBQUNPLElBQUksR0FBR1AsUUFBUSxDQUFDTyxJQUFJLElBQUksQ0FBQyxDQUFDO1lBQ25DUCxRQUFRLENBQUNPLElBQUksQ0FBQ0EsSUFBSSxDQUFDLEdBQUdQLFFBQVEsQ0FBQ08sSUFBSSxDQUFDQSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDL0MsTUFBTU8sUUFBUSxHQUFHZCxRQUFRLENBQUNPLElBQUksQ0FBQ0EsSUFBSSxDQUFDO1lBRXBDLElBQUlDLE9BQU8sRUFBRTtjQUNYTSxRQUFRLENBQUNOLE9BQU8sR0FBR00sUUFBUSxDQUFDTixPQUFPLElBQUksQ0FBQyxDQUFDO2NBQ3pDTSxRQUFRLENBQUNOLE9BQU8sQ0FBQ0EsT0FBTyxDQUFDLEdBQUcsSUFBSSxDQUFDaEIsV0FBVyxDQUFDc0IsUUFBUSxDQUFDTixPQUFPLENBQUNBLE9BQU8sQ0FBQyxFQUFFZCxTQUFTLENBQUM7WUFDcEYsQ0FBQyxNQUNJO2NBQ0hvQixRQUFRLENBQUNkLFFBQVEsR0FBRyxJQUFJLENBQUNSLFdBQVcsQ0FBQ3NCLFFBQVEsQ0FBQ2QsUUFBUSxFQUFFTixTQUFTLENBQUM7WUFDcEU7VUFDRjtVQUNBO1FBRUY7VUFDRSxNQUFNLElBQUlxQixLQUFLLENBQUMsbURBQW1ELENBQUM7TUFBQztJQUUzRTtFQUNGLENBQUM7O0VBRUQ7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0VBQ0V4QyxNQUFNLENBQUN5QyxPQUFPLEdBQUcsU0FBU0EsT0FBTyxDQUFDQyxHQUFHLEVBQUU7SUFDckMsT0FBUUEsR0FBRyxHQUFHQyxJQUFJLENBQUNDLEVBQUUsR0FBSSxHQUFHO0VBQzlCLENBQUM7O0VBRUQ7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0VBQ0U1QyxNQUFNLENBQUM2QyxRQUFRLEdBQUcsU0FBU0EsUUFBUSxDQUFDQyxJQUFJLEVBQUVDLEVBQUUsRUFBRUMsSUFBSSxHQUFHLElBQUksRUFBRTtJQUN6RDtJQUNBLE1BQU1DLENBQUMsR0FBRyxhQUFhLENBQUNDLElBQUksQ0FBQ0YsSUFBSSxDQUFDLEdBQUcsU0FBUyxHQUFHLE1BQU07O0lBRXZEO0lBQ0E7SUFDQSxJQUFJLEVBQUVGLElBQUksQ0FBQ0ssR0FBRyxJQUFJTCxJQUFJLENBQUNNLEdBQUcsSUFBSUwsRUFBRSxDQUFDSSxHQUFHLElBQUlKLEVBQUUsQ0FBQ0ssR0FBRyxDQUFDLEVBQUU7TUFDL0MsT0FBT0MsR0FBRztJQUNaO0lBRUEsTUFBTUMsSUFBSSxHQUFHLElBQUksQ0FBQ2IsT0FBTyxDQUFDSyxJQUFJLENBQUNLLEdBQUcsQ0FBQztJQUNuQyxNQUFNSSxJQUFJLEdBQUcsSUFBSSxDQUFDZCxPQUFPLENBQUNNLEVBQUUsQ0FBQ0ksR0FBRyxDQUFDO0lBQ2pDLE1BQU1LLENBQUMsR0FBR2IsSUFBSSxDQUFDYyxHQUFHLENBQUMsQ0FBQ0gsSUFBSSxHQUFHQyxJQUFJLElBQUksQ0FBQyxDQUFDO0lBQ3JDLE1BQU1HLENBQUMsR0FBR2YsSUFBSSxDQUFDYyxHQUFHLENBQUMsSUFBSSxDQUFDaEIsT0FBTyxDQUFDSyxJQUFJLENBQUNNLEdBQUcsR0FBR0wsRUFBRSxDQUFDSyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7O0lBRXZEO0lBQ0EsTUFBTU8sQ0FBQyxHQUFJSCxDQUFDLEdBQUdBLENBQUMsR0FBSWIsSUFBSSxDQUFDaUIsR0FBRyxDQUFDTixJQUFJLENBQUMsR0FBR1gsSUFBSSxDQUFDaUIsR0FBRyxDQUFDTCxJQUFJLENBQUMsSUFBSUcsQ0FBQyxHQUFHQSxDQUFDLENBQUM7SUFDN0QsTUFBTUcsR0FBRyxHQUFHLENBQUMsR0FBR2xCLElBQUksQ0FBQ21CLElBQUksQ0FBQ25CLElBQUksQ0FBQ29CLElBQUksQ0FBQ0osQ0FBQyxDQUFDLENBQUM7O0lBRXZDO0lBQ0EsT0FBT0UsR0FBRyxHQUFHWixDQUFDO0VBQ2hCLENBQUM7QUFDSCxDQUFDLEVBQUVsRCxNQUFNLENBQUMifQ==
