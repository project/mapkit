"use strict";

(({
  behaviors,
  Toolshed: ts
}, Mapkit) => {
  /**
   * Location autocomplete provider plugins.
   *
   * The generator function should create a Map object that implements the
   * following prototype (still in progress). The map parameters should accept
   * common Mapkit marker settings, and allow the marker builder (Mapkit.marker)
   * handle the implementation and API specifics.
   *
   * See example class in places-autocomplete.es6.js from the mapkit_gmap
   * module for an example and more implementation details.
   *
   * Autocomplete.prototype = {
   *   isReady(),
   *   init(),
   *   attachInput(inputEl),
   *   clearInputValues(),
   *   formatSuggestText(item),
   *   selectItem(obj),
   *   fetchSuggestions(keywords),
   *   destroy(),
   * }
   *
   * @todo Write better documentation on map class prototypes.
   */
  Mapkit.ac = {};

  /**
   * Google Places API autocomplete handler to link an autocomplete
   * entry to a Drupal input filter.
   */
  Mapkit.Autocomplete = class extends ts.Autocomplete {
    /**
     * Create a new instance of the autocomplete handler. Requires a call to
     * Mapkit.Autocomplete.init() in order to initialize the autocomplete.
     *
     * @param {DOMElement} el
     *   The HTML input to submit the autocomplete results and interaction to.
     * @param {Object} config
     *   Configuration to use for limiting the bounds of the autocomplete.
     */
    constructor(el, config) {
      const input = el.getElementsByClassName('mapkit-autocomplete__text')[0];
      if (!input) {
        throw new Error('Invalid autocomplete input element');
      }
      if (!Mapkit.ac[config.handler]) {
        throw new Error(`Invalid autocomplete handler specified ${config.handler}`);
      }

      // The input elements here have special handling and the values need to
      // be managed by this class.
      // This prevents Toolshed autocomplete from managing values of the
      // textfield inputs and autocomplete elements.
      config.separateValue = false;
      super(input, config);
      this.wrapper = el;
      this.pending = false;
      this.handler = new Mapkit.ac[config.handler](this);

      // Find the child input elements and bind them to the AC handler.
      ts.walkByClass(el, 'mapkit-autocomplete__value', dataEl => {
        const {
          key
        } = dataEl.dataset;
        if (key) {
          // Capture this input for device_location to know where to put the
          // lat/long data from the device / browser.
          if (key === 'latlng') {
            this.latlng = dataEl;
          }
          this.handler.attachInput(key, dataEl);
        }
      });
    }

    /**
     * The loader queue identifier of the autocomplete provider. This is used
     * for providers that need to asynchronously load dependencies, and will
     * trigger an initializer when the libraries have completed loading.
     *
     * @return {string|undefined|null}
     *   The loader identifier to register the init() callback to. If not
     *   provided this handler does not require a loader queue.
     */
    getLoaderId() {
      return this.handler.loaderId;
    }

    /**
     * Should be called after the Google Maps API scripts have been loaded.
     * This function will initialize the Places Autocomplete and hook-up
     * the autocomplete functionality to the input element.
     */
    init() {
      this.handler.init();

      // Attach the device location functionality if it was enabled here.
      if (window.isSecureContext !== false && navigator && navigator.geolocation) {
        ts.walkByClass(this.wrapper, 'mapkit-autocomplete__geolocation-link', link => {
          this.geolocationLink = link;
          link.style.display = '';
          this.onClientGeolocation = this.onClientGeolocation.bind(this);
          link.addEventListener('click', this.onClientGeolocation);
        });
      }

      // If a query before initialization, fetch the suggestions now that the
      // dependencies are loaded.
      if (this.lastQuery && this.lastQuery.length) {
        const {
          lastQuery
        } = this;
        delete this.lastQuery;
        this.fetchSuggestions(lastQuery);
      }
    }

    /**
     * The click callback for the geolocation link for the mapkit autocomplete.
     *
     * @param {MouseClickEvent} e
     *   Mouse click event.
     */
    onClientGeolocation(e) {
      e.preventDefault();
      navigator.geolocation.getCurrentPosition(pos => {
        if (this.latlng) {
          this.ac.value = 'My location';
          this.latlng.value = `${pos.coords.latitude.toFixed(7)},${pos.coords.longitude.toFixed(7)}`;
        }
      }, () => {
        // eslint-disable-next-line no-alert
        alert('Unable to retrieve your location.');
      });
    }

    /**
     * @inheritdoc
     */
    selectItem(item) {
      this.clearSuggestions();
      this.hideSuggestions();

      // Set the display text into the form element.
      this.ac.value = item.text;
      this.handler.select(item);
    }

    /**
     * @inheritdoc
     */
    fetchSuggestions(text) {
      this.handler.clearInputValues();

      // Ensure a minimal query value to fetch suggestions for.
      if (!text || text.length < this.config.minLength) {
        return;
      }
      if (this.pending || !this.handler.isReady()) {
        // Unable to cancel the existing request, so instead we'll store
        // any updates for when the pending request completes.
        this.lastQuery = text;
        return;
      }

      // Turn on the loading spinner, and let the system know we already
      // have a request in progress.
      this.ac.addClass('ui-autocomplete-loading');
      this.pending = true;

      // Get the suggestions from the autocomplete handler.
      this.handler.fetchSuggestions(text);
    }

    /**
     * Suggests have been returned and should be presented.
     *
     * This can be empty list of suggestions, which just indicates that the
     * pending suggestions fetch has been completed.
     *
     * @param {object|null} suggested
     *   List of suggested items to build into the suggestions pane.
     */
    setFetchedSuggestions(suggested) {
      this.pending = false;
      if (suggested) {
        this.createSuggestions(suggested);
        this.displaySuggestions();
      } else {
        this.clearSuggestions();
        this.hideSuggestions();
      }

      // If there was another query requested while the previous one was
      // pending, go request that now that we've completed the previous one.
      if (this.lastQuery && this.lastQuery.length) {
        const {
          lastQuery
        } = this;
        delete this.lastQuery;
        this.fetchSuggestions(lastQuery);
      } else {
        this.ac.removeClass('ui-autocomplete-loading');
      }
    }

    /**
     * Clean up event listeners and create DOM elements.
     */
    destroy() {
      this.handler.destroy();
      delete this.handler;
      if (this.geolocationLink) {
        this.geolocationLink.removeEventListener('click', this.onClientGeolocation);
        delete this.geolocationLink;
      }
      delete this.pending;
      delete this.lastQuery;
      super.destroy();
    }
  };

  /**
   * Drupal behaviors for rigging up a Mapkit autocomplete input.
   */
  behaviors.mapkitAutocomplete = {
    instances: new Map(),
    urlParams: null,
    attach(context) {
      ts.walkByClass(context, 'mapkit-autocomplete', item => {
        const config = item.dataset.autocomplete ? JSON.parse(item.dataset.autocomplete) : {};
        try {
          const ac = new Mapkit.Autocomplete(item, config);
          Mapkit.addInit(ac.getLoaderId(), ac.init.bind(ac));
          this.instances.set(item.id || item, ac);
        } catch (e) {
          // Unable to attach autocomplete functionality.
          console.error(e);
        }
      }, 'autocomplete--processed');
    },
    detach(context, settings, trigger) {
      if (trigger === 'unload') {
        ts.walkBySelector(context, '.mapkit-autocomplete.autocomplete--processed', item => {
          item.classList.remove('autocomplete--processed');

          // If this has been processed and added as an autocomplete cleanup.
          const ac = this.instances.get(item.id || item);
          if (ac) ac.destroy();
        });
      }
    }
  };
})(Drupal, Mapkit);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0b2NvbXBsZXRlLmpzIiwibmFtZXMiOlsiYmVoYXZpb3JzIiwiVG9vbHNoZWQiLCJ0cyIsIk1hcGtpdCIsImFjIiwiQXV0b2NvbXBsZXRlIiwiY29uc3RydWN0b3IiLCJlbCIsImNvbmZpZyIsImlucHV0IiwiZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSIsIkVycm9yIiwiaGFuZGxlciIsInNlcGFyYXRlVmFsdWUiLCJ3cmFwcGVyIiwicGVuZGluZyIsIndhbGtCeUNsYXNzIiwiZGF0YUVsIiwia2V5IiwiZGF0YXNldCIsImxhdGxuZyIsImF0dGFjaElucHV0IiwiZ2V0TG9hZGVySWQiLCJsb2FkZXJJZCIsImluaXQiLCJ3aW5kb3ciLCJpc1NlY3VyZUNvbnRleHQiLCJuYXZpZ2F0b3IiLCJnZW9sb2NhdGlvbiIsImxpbmsiLCJnZW9sb2NhdGlvbkxpbmsiLCJzdHlsZSIsImRpc3BsYXkiLCJvbkNsaWVudEdlb2xvY2F0aW9uIiwiYmluZCIsImFkZEV2ZW50TGlzdGVuZXIiLCJsYXN0UXVlcnkiLCJsZW5ndGgiLCJmZXRjaFN1Z2dlc3Rpb25zIiwiZSIsInByZXZlbnREZWZhdWx0IiwiZ2V0Q3VycmVudFBvc2l0aW9uIiwicG9zIiwidmFsdWUiLCJjb29yZHMiLCJsYXRpdHVkZSIsInRvRml4ZWQiLCJsb25naXR1ZGUiLCJhbGVydCIsInNlbGVjdEl0ZW0iLCJpdGVtIiwiY2xlYXJTdWdnZXN0aW9ucyIsImhpZGVTdWdnZXN0aW9ucyIsInRleHQiLCJzZWxlY3QiLCJjbGVhcklucHV0VmFsdWVzIiwibWluTGVuZ3RoIiwiaXNSZWFkeSIsImFkZENsYXNzIiwic2V0RmV0Y2hlZFN1Z2dlc3Rpb25zIiwic3VnZ2VzdGVkIiwiY3JlYXRlU3VnZ2VzdGlvbnMiLCJkaXNwbGF5U3VnZ2VzdGlvbnMiLCJyZW1vdmVDbGFzcyIsImRlc3Ryb3kiLCJyZW1vdmVFdmVudExpc3RlbmVyIiwibWFwa2l0QXV0b2NvbXBsZXRlIiwiaW5zdGFuY2VzIiwiTWFwIiwidXJsUGFyYW1zIiwiYXR0YWNoIiwiY29udGV4dCIsImF1dG9jb21wbGV0ZSIsIkpTT04iLCJwYXJzZSIsImFkZEluaXQiLCJzZXQiLCJpZCIsImNvbnNvbGUiLCJlcnJvciIsImRldGFjaCIsInNldHRpbmdzIiwidHJpZ2dlciIsIndhbGtCeVNlbGVjdG9yIiwiY2xhc3NMaXN0IiwicmVtb3ZlIiwiZ2V0IiwiRHJ1cGFsIl0sInNvdXJjZXMiOlsiYXV0b2NvbXBsZXRlLmVzNi5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIoKHsgYmVoYXZpb3JzLCBUb29sc2hlZDogdHMgfSwgTWFwa2l0KSA9PiB7XG4gIC8qKlxuICAgKiBMb2NhdGlvbiBhdXRvY29tcGxldGUgcHJvdmlkZXIgcGx1Z2lucy5cbiAgICpcbiAgICogVGhlIGdlbmVyYXRvciBmdW5jdGlvbiBzaG91bGQgY3JlYXRlIGEgTWFwIG9iamVjdCB0aGF0IGltcGxlbWVudHMgdGhlXG4gICAqIGZvbGxvd2luZyBwcm90b3R5cGUgKHN0aWxsIGluIHByb2dyZXNzKS4gVGhlIG1hcCBwYXJhbWV0ZXJzIHNob3VsZCBhY2NlcHRcbiAgICogY29tbW9uIE1hcGtpdCBtYXJrZXIgc2V0dGluZ3MsIGFuZCBhbGxvdyB0aGUgbWFya2VyIGJ1aWxkZXIgKE1hcGtpdC5tYXJrZXIpXG4gICAqIGhhbmRsZSB0aGUgaW1wbGVtZW50YXRpb24gYW5kIEFQSSBzcGVjaWZpY3MuXG4gICAqXG4gICAqIFNlZSBleGFtcGxlIGNsYXNzIGluIHBsYWNlcy1hdXRvY29tcGxldGUuZXM2LmpzIGZyb20gdGhlIG1hcGtpdF9nbWFwXG4gICAqIG1vZHVsZSBmb3IgYW4gZXhhbXBsZSBhbmQgbW9yZSBpbXBsZW1lbnRhdGlvbiBkZXRhaWxzLlxuICAgKlxuICAgKiBBdXRvY29tcGxldGUucHJvdG90eXBlID0ge1xuICAgKiAgIGlzUmVhZHkoKSxcbiAgICogICBpbml0KCksXG4gICAqICAgYXR0YWNoSW5wdXQoaW5wdXRFbCksXG4gICAqICAgY2xlYXJJbnB1dFZhbHVlcygpLFxuICAgKiAgIGZvcm1hdFN1Z2dlc3RUZXh0KGl0ZW0pLFxuICAgKiAgIHNlbGVjdEl0ZW0ob2JqKSxcbiAgICogICBmZXRjaFN1Z2dlc3Rpb25zKGtleXdvcmRzKSxcbiAgICogICBkZXN0cm95KCksXG4gICAqIH1cbiAgICpcbiAgICogQHRvZG8gV3JpdGUgYmV0dGVyIGRvY3VtZW50YXRpb24gb24gbWFwIGNsYXNzIHByb3RvdHlwZXMuXG4gICAqL1xuICBNYXBraXQuYWMgPSB7fTtcblxuICAvKipcbiAgICogR29vZ2xlIFBsYWNlcyBBUEkgYXV0b2NvbXBsZXRlIGhhbmRsZXIgdG8gbGluayBhbiBhdXRvY29tcGxldGVcbiAgICogZW50cnkgdG8gYSBEcnVwYWwgaW5wdXQgZmlsdGVyLlxuICAgKi9cbiAgTWFwa2l0LkF1dG9jb21wbGV0ZSA9IGNsYXNzIGV4dGVuZHMgdHMuQXV0b2NvbXBsZXRlIHtcbiAgICAvKipcbiAgICAgKiBDcmVhdGUgYSBuZXcgaW5zdGFuY2Ugb2YgdGhlIGF1dG9jb21wbGV0ZSBoYW5kbGVyLiBSZXF1aXJlcyBhIGNhbGwgdG9cbiAgICAgKiBNYXBraXQuQXV0b2NvbXBsZXRlLmluaXQoKSBpbiBvcmRlciB0byBpbml0aWFsaXplIHRoZSBhdXRvY29tcGxldGUuXG4gICAgICpcbiAgICAgKiBAcGFyYW0ge0RPTUVsZW1lbnR9IGVsXG4gICAgICogICBUaGUgSFRNTCBpbnB1dCB0byBzdWJtaXQgdGhlIGF1dG9jb21wbGV0ZSByZXN1bHRzIGFuZCBpbnRlcmFjdGlvbiB0by5cbiAgICAgKiBAcGFyYW0ge09iamVjdH0gY29uZmlnXG4gICAgICogICBDb25maWd1cmF0aW9uIHRvIHVzZSBmb3IgbGltaXRpbmcgdGhlIGJvdW5kcyBvZiB0aGUgYXV0b2NvbXBsZXRlLlxuICAgICAqL1xuICAgIGNvbnN0cnVjdG9yKGVsLCBjb25maWcpIHtcbiAgICAgIGNvbnN0IGlucHV0ID0gZWwuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSgnbWFwa2l0LWF1dG9jb21wbGV0ZV9fdGV4dCcpWzBdO1xuXG4gICAgICBpZiAoIWlucHV0KSB7XG4gICAgICAgIHRocm93IG5ldyBFcnJvcignSW52YWxpZCBhdXRvY29tcGxldGUgaW5wdXQgZWxlbWVudCcpO1xuICAgICAgfVxuICAgICAgaWYgKCFNYXBraXQuYWNbY29uZmlnLmhhbmRsZXJdKSB7XG4gICAgICAgIHRocm93IG5ldyBFcnJvcihgSW52YWxpZCBhdXRvY29tcGxldGUgaGFuZGxlciBzcGVjaWZpZWQgJHtjb25maWcuaGFuZGxlcn1gKTtcbiAgICAgIH1cblxuICAgICAgLy8gVGhlIGlucHV0IGVsZW1lbnRzIGhlcmUgaGF2ZSBzcGVjaWFsIGhhbmRsaW5nIGFuZCB0aGUgdmFsdWVzIG5lZWQgdG9cbiAgICAgIC8vIGJlIG1hbmFnZWQgYnkgdGhpcyBjbGFzcy5cbiAgICAgIC8vIFRoaXMgcHJldmVudHMgVG9vbHNoZWQgYXV0b2NvbXBsZXRlIGZyb20gbWFuYWdpbmcgdmFsdWVzIG9mIHRoZVxuICAgICAgLy8gdGV4dGZpZWxkIGlucHV0cyBhbmQgYXV0b2NvbXBsZXRlIGVsZW1lbnRzLlxuICAgICAgY29uZmlnLnNlcGFyYXRlVmFsdWUgPSBmYWxzZTtcbiAgICAgIHN1cGVyKGlucHV0LCBjb25maWcpO1xuXG4gICAgICB0aGlzLndyYXBwZXIgPSBlbDtcbiAgICAgIHRoaXMucGVuZGluZyA9IGZhbHNlO1xuICAgICAgdGhpcy5oYW5kbGVyID0gbmV3IChNYXBraXQuYWNbY29uZmlnLmhhbmRsZXJdKSh0aGlzKTtcblxuICAgICAgLy8gRmluZCB0aGUgY2hpbGQgaW5wdXQgZWxlbWVudHMgYW5kIGJpbmQgdGhlbSB0byB0aGUgQUMgaGFuZGxlci5cbiAgICAgIHRzLndhbGtCeUNsYXNzKGVsLCAnbWFwa2l0LWF1dG9jb21wbGV0ZV9fdmFsdWUnLCAoZGF0YUVsKSA9PiB7XG4gICAgICAgIGNvbnN0IHsga2V5IH0gPSBkYXRhRWwuZGF0YXNldDtcblxuICAgICAgICBpZiAoa2V5KSB7XG4gICAgICAgICAgLy8gQ2FwdHVyZSB0aGlzIGlucHV0IGZvciBkZXZpY2VfbG9jYXRpb24gdG8ga25vdyB3aGVyZSB0byBwdXQgdGhlXG4gICAgICAgICAgLy8gbGF0L2xvbmcgZGF0YSBmcm9tIHRoZSBkZXZpY2UgLyBicm93c2VyLlxuICAgICAgICAgIGlmIChrZXkgPT09ICdsYXRsbmcnKSB7XG4gICAgICAgICAgICB0aGlzLmxhdGxuZyA9IGRhdGFFbDtcbiAgICAgICAgICB9XG4gICAgICAgICAgdGhpcy5oYW5kbGVyLmF0dGFjaElucHV0KGtleSwgZGF0YUVsKTtcbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogVGhlIGxvYWRlciBxdWV1ZSBpZGVudGlmaWVyIG9mIHRoZSBhdXRvY29tcGxldGUgcHJvdmlkZXIuIFRoaXMgaXMgdXNlZFxuICAgICAqIGZvciBwcm92aWRlcnMgdGhhdCBuZWVkIHRvIGFzeW5jaHJvbm91c2x5IGxvYWQgZGVwZW5kZW5jaWVzLCBhbmQgd2lsbFxuICAgICAqIHRyaWdnZXIgYW4gaW5pdGlhbGl6ZXIgd2hlbiB0aGUgbGlicmFyaWVzIGhhdmUgY29tcGxldGVkIGxvYWRpbmcuXG4gICAgICpcbiAgICAgKiBAcmV0dXJuIHtzdHJpbmd8dW5kZWZpbmVkfG51bGx9XG4gICAgICogICBUaGUgbG9hZGVyIGlkZW50aWZpZXIgdG8gcmVnaXN0ZXIgdGhlIGluaXQoKSBjYWxsYmFjayB0by4gSWYgbm90XG4gICAgICogICBwcm92aWRlZCB0aGlzIGhhbmRsZXIgZG9lcyBub3QgcmVxdWlyZSBhIGxvYWRlciBxdWV1ZS5cbiAgICAgKi9cbiAgICBnZXRMb2FkZXJJZCgpIHtcbiAgICAgIHJldHVybiB0aGlzLmhhbmRsZXIubG9hZGVySWQ7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogU2hvdWxkIGJlIGNhbGxlZCBhZnRlciB0aGUgR29vZ2xlIE1hcHMgQVBJIHNjcmlwdHMgaGF2ZSBiZWVuIGxvYWRlZC5cbiAgICAgKiBUaGlzIGZ1bmN0aW9uIHdpbGwgaW5pdGlhbGl6ZSB0aGUgUGxhY2VzIEF1dG9jb21wbGV0ZSBhbmQgaG9vay11cFxuICAgICAqIHRoZSBhdXRvY29tcGxldGUgZnVuY3Rpb25hbGl0eSB0byB0aGUgaW5wdXQgZWxlbWVudC5cbiAgICAgKi9cbiAgICBpbml0KCkge1xuICAgICAgdGhpcy5oYW5kbGVyLmluaXQoKTtcblxuICAgICAgLy8gQXR0YWNoIHRoZSBkZXZpY2UgbG9jYXRpb24gZnVuY3Rpb25hbGl0eSBpZiBpdCB3YXMgZW5hYmxlZCBoZXJlLlxuICAgICAgaWYgKHdpbmRvdy5pc1NlY3VyZUNvbnRleHQgIT09IGZhbHNlICYmIG5hdmlnYXRvciAmJiBuYXZpZ2F0b3IuZ2VvbG9jYXRpb24pIHtcbiAgICAgICAgdHMud2Fsa0J5Q2xhc3ModGhpcy53cmFwcGVyLCAnbWFwa2l0LWF1dG9jb21wbGV0ZV9fZ2VvbG9jYXRpb24tbGluaycsIChsaW5rKSA9PiB7XG4gICAgICAgICAgdGhpcy5nZW9sb2NhdGlvbkxpbmsgPSBsaW5rO1xuICAgICAgICAgIGxpbmsuc3R5bGUuZGlzcGxheSA9ICcnO1xuXG4gICAgICAgICAgdGhpcy5vbkNsaWVudEdlb2xvY2F0aW9uID0gdGhpcy5vbkNsaWVudEdlb2xvY2F0aW9uLmJpbmQodGhpcyk7XG4gICAgICAgICAgbGluay5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIHRoaXMub25DbGllbnRHZW9sb2NhdGlvbik7XG4gICAgICAgIH0pO1xuICAgICAgfVxuXG4gICAgICAvLyBJZiBhIHF1ZXJ5IGJlZm9yZSBpbml0aWFsaXphdGlvbiwgZmV0Y2ggdGhlIHN1Z2dlc3Rpb25zIG5vdyB0aGF0IHRoZVxuICAgICAgLy8gZGVwZW5kZW5jaWVzIGFyZSBsb2FkZWQuXG4gICAgICBpZiAodGhpcy5sYXN0UXVlcnkgJiYgdGhpcy5sYXN0UXVlcnkubGVuZ3RoKSB7XG4gICAgICAgIGNvbnN0IHsgbGFzdFF1ZXJ5IH0gPSB0aGlzO1xuICAgICAgICBkZWxldGUgdGhpcy5sYXN0UXVlcnk7XG4gICAgICAgIHRoaXMuZmV0Y2hTdWdnZXN0aW9ucyhsYXN0UXVlcnkpO1xuICAgICAgfVxuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFRoZSBjbGljayBjYWxsYmFjayBmb3IgdGhlIGdlb2xvY2F0aW9uIGxpbmsgZm9yIHRoZSBtYXBraXQgYXV0b2NvbXBsZXRlLlxuICAgICAqXG4gICAgICogQHBhcmFtIHtNb3VzZUNsaWNrRXZlbnR9IGVcbiAgICAgKiAgIE1vdXNlIGNsaWNrIGV2ZW50LlxuICAgICAqL1xuICAgIG9uQ2xpZW50R2VvbG9jYXRpb24oZSkge1xuICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgICBuYXZpZ2F0b3IuZ2VvbG9jYXRpb24uZ2V0Q3VycmVudFBvc2l0aW9uKChwb3MpID0+IHtcbiAgICAgICAgaWYgKHRoaXMubGF0bG5nKSB7XG4gICAgICAgICAgdGhpcy5hYy52YWx1ZSA9ICdNeSBsb2NhdGlvbic7XG4gICAgICAgICAgdGhpcy5sYXRsbmcudmFsdWUgPSBgJHtwb3MuY29vcmRzLmxhdGl0dWRlLnRvRml4ZWQoNyl9LCR7cG9zLmNvb3Jkcy5sb25naXR1ZGUudG9GaXhlZCg3KX1gO1xuICAgICAgICB9XG4gICAgICB9LCAoKSA9PiB7XG4gICAgICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBuby1hbGVydFxuICAgICAgICBhbGVydCgnVW5hYmxlIHRvIHJldHJpZXZlIHlvdXIgbG9jYXRpb24uJyk7XG4gICAgICB9KTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBAaW5oZXJpdGRvY1xuICAgICAqL1xuICAgIHNlbGVjdEl0ZW0oaXRlbSkge1xuICAgICAgdGhpcy5jbGVhclN1Z2dlc3Rpb25zKCk7XG4gICAgICB0aGlzLmhpZGVTdWdnZXN0aW9ucygpO1xuXG4gICAgICAvLyBTZXQgdGhlIGRpc3BsYXkgdGV4dCBpbnRvIHRoZSBmb3JtIGVsZW1lbnQuXG4gICAgICB0aGlzLmFjLnZhbHVlID0gaXRlbS50ZXh0O1xuICAgICAgdGhpcy5oYW5kbGVyLnNlbGVjdChpdGVtKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBAaW5oZXJpdGRvY1xuICAgICAqL1xuICAgIGZldGNoU3VnZ2VzdGlvbnModGV4dCkge1xuICAgICAgdGhpcy5oYW5kbGVyLmNsZWFySW5wdXRWYWx1ZXMoKTtcblxuICAgICAgLy8gRW5zdXJlIGEgbWluaW1hbCBxdWVyeSB2YWx1ZSB0byBmZXRjaCBzdWdnZXN0aW9ucyBmb3IuXG4gICAgICBpZiAoIXRleHQgfHwgdGV4dC5sZW5ndGggPCB0aGlzLmNvbmZpZy5taW5MZW5ndGgpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICBpZiAodGhpcy5wZW5kaW5nIHx8ICF0aGlzLmhhbmRsZXIuaXNSZWFkeSgpKSB7XG4gICAgICAgIC8vIFVuYWJsZSB0byBjYW5jZWwgdGhlIGV4aXN0aW5nIHJlcXVlc3QsIHNvIGluc3RlYWQgd2UnbGwgc3RvcmVcbiAgICAgICAgLy8gYW55IHVwZGF0ZXMgZm9yIHdoZW4gdGhlIHBlbmRpbmcgcmVxdWVzdCBjb21wbGV0ZXMuXG4gICAgICAgIHRoaXMubGFzdFF1ZXJ5ID0gdGV4dDtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICAvLyBUdXJuIG9uIHRoZSBsb2FkaW5nIHNwaW5uZXIsIGFuZCBsZXQgdGhlIHN5c3RlbSBrbm93IHdlIGFscmVhZHlcbiAgICAgIC8vIGhhdmUgYSByZXF1ZXN0IGluIHByb2dyZXNzLlxuICAgICAgdGhpcy5hYy5hZGRDbGFzcygndWktYXV0b2NvbXBsZXRlLWxvYWRpbmcnKTtcbiAgICAgIHRoaXMucGVuZGluZyA9IHRydWU7XG5cbiAgICAgIC8vIEdldCB0aGUgc3VnZ2VzdGlvbnMgZnJvbSB0aGUgYXV0b2NvbXBsZXRlIGhhbmRsZXIuXG4gICAgICB0aGlzLmhhbmRsZXIuZmV0Y2hTdWdnZXN0aW9ucyh0ZXh0KTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBTdWdnZXN0cyBoYXZlIGJlZW4gcmV0dXJuZWQgYW5kIHNob3VsZCBiZSBwcmVzZW50ZWQuXG4gICAgICpcbiAgICAgKiBUaGlzIGNhbiBiZSBlbXB0eSBsaXN0IG9mIHN1Z2dlc3Rpb25zLCB3aGljaCBqdXN0IGluZGljYXRlcyB0aGF0IHRoZVxuICAgICAqIHBlbmRpbmcgc3VnZ2VzdGlvbnMgZmV0Y2ggaGFzIGJlZW4gY29tcGxldGVkLlxuICAgICAqXG4gICAgICogQHBhcmFtIHtvYmplY3R8bnVsbH0gc3VnZ2VzdGVkXG4gICAgICogICBMaXN0IG9mIHN1Z2dlc3RlZCBpdGVtcyB0byBidWlsZCBpbnRvIHRoZSBzdWdnZXN0aW9ucyBwYW5lLlxuICAgICAqL1xuICAgIHNldEZldGNoZWRTdWdnZXN0aW9ucyhzdWdnZXN0ZWQpIHtcbiAgICAgIHRoaXMucGVuZGluZyA9IGZhbHNlO1xuXG4gICAgICBpZiAoc3VnZ2VzdGVkKSB7XG4gICAgICAgIHRoaXMuY3JlYXRlU3VnZ2VzdGlvbnMoc3VnZ2VzdGVkKTtcbiAgICAgICAgdGhpcy5kaXNwbGF5U3VnZ2VzdGlvbnMoKTtcbiAgICAgIH1cbiAgICAgIGVsc2Uge1xuICAgICAgICB0aGlzLmNsZWFyU3VnZ2VzdGlvbnMoKTtcbiAgICAgICAgdGhpcy5oaWRlU3VnZ2VzdGlvbnMoKTtcbiAgICAgIH1cblxuICAgICAgLy8gSWYgdGhlcmUgd2FzIGFub3RoZXIgcXVlcnkgcmVxdWVzdGVkIHdoaWxlIHRoZSBwcmV2aW91cyBvbmUgd2FzXG4gICAgICAvLyBwZW5kaW5nLCBnbyByZXF1ZXN0IHRoYXQgbm93IHRoYXQgd2UndmUgY29tcGxldGVkIHRoZSBwcmV2aW91cyBvbmUuXG4gICAgICBpZiAodGhpcy5sYXN0UXVlcnkgJiYgdGhpcy5sYXN0UXVlcnkubGVuZ3RoKSB7XG4gICAgICAgIGNvbnN0IHsgbGFzdFF1ZXJ5IH0gPSB0aGlzO1xuICAgICAgICBkZWxldGUgdGhpcy5sYXN0UXVlcnk7XG5cbiAgICAgICAgdGhpcy5mZXRjaFN1Z2dlc3Rpb25zKGxhc3RRdWVyeSk7XG4gICAgICB9XG4gICAgICBlbHNlIHtcbiAgICAgICAgdGhpcy5hYy5yZW1vdmVDbGFzcygndWktYXV0b2NvbXBsZXRlLWxvYWRpbmcnKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBDbGVhbiB1cCBldmVudCBsaXN0ZW5lcnMgYW5kIGNyZWF0ZSBET00gZWxlbWVudHMuXG4gICAgICovXG4gICAgZGVzdHJveSgpIHtcbiAgICAgIHRoaXMuaGFuZGxlci5kZXN0cm95KCk7XG4gICAgICBkZWxldGUgdGhpcy5oYW5kbGVyO1xuXG4gICAgICBpZiAodGhpcy5nZW9sb2NhdGlvbkxpbmspIHtcbiAgICAgICAgdGhpcy5nZW9sb2NhdGlvbkxpbmsucmVtb3ZlRXZlbnRMaXN0ZW5lcignY2xpY2snLCB0aGlzLm9uQ2xpZW50R2VvbG9jYXRpb24pO1xuICAgICAgICBkZWxldGUgdGhpcy5nZW9sb2NhdGlvbkxpbms7XG4gICAgICB9XG5cbiAgICAgIGRlbGV0ZSB0aGlzLnBlbmRpbmc7XG4gICAgICBkZWxldGUgdGhpcy5sYXN0UXVlcnk7XG5cbiAgICAgIHN1cGVyLmRlc3Ryb3koKTtcbiAgICB9XG4gIH07XG5cbiAgLyoqXG4gICAqIERydXBhbCBiZWhhdmlvcnMgZm9yIHJpZ2dpbmcgdXAgYSBNYXBraXQgYXV0b2NvbXBsZXRlIGlucHV0LlxuICAgKi9cbiAgYmVoYXZpb3JzLm1hcGtpdEF1dG9jb21wbGV0ZSA9IHtcbiAgICBpbnN0YW5jZXM6IG5ldyBNYXAoKSxcbiAgICB1cmxQYXJhbXM6IG51bGwsXG5cbiAgICBhdHRhY2goY29udGV4dCkge1xuICAgICAgdHMud2Fsa0J5Q2xhc3MoY29udGV4dCwgJ21hcGtpdC1hdXRvY29tcGxldGUnLCAoaXRlbSkgPT4ge1xuICAgICAgICBjb25zdCBjb25maWcgPSBpdGVtLmRhdGFzZXQuYXV0b2NvbXBsZXRlID8gSlNPTi5wYXJzZShpdGVtLmRhdGFzZXQuYXV0b2NvbXBsZXRlKSA6IHt9O1xuXG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgY29uc3QgYWMgPSBuZXcgTWFwa2l0LkF1dG9jb21wbGV0ZShpdGVtLCBjb25maWcpO1xuICAgICAgICAgIE1hcGtpdC5hZGRJbml0KGFjLmdldExvYWRlcklkKCksIGFjLmluaXQuYmluZChhYykpO1xuICAgICAgICAgIHRoaXMuaW5zdGFuY2VzLnNldChpdGVtLmlkIHx8IGl0ZW0sIGFjKTtcbiAgICAgICAgfVxuICAgICAgICBjYXRjaCAoZSkge1xuICAgICAgICAgIC8vIFVuYWJsZSB0byBhdHRhY2ggYXV0b2NvbXBsZXRlIGZ1bmN0aW9uYWxpdHkuXG4gICAgICAgICAgY29uc29sZS5lcnJvcihlKTtcbiAgICAgICAgfVxuICAgICAgfSwgJ2F1dG9jb21wbGV0ZS0tcHJvY2Vzc2VkJyk7XG4gICAgfSxcblxuICAgIGRldGFjaChjb250ZXh0LCBzZXR0aW5ncywgdHJpZ2dlcikge1xuICAgICAgaWYgKHRyaWdnZXIgPT09ICd1bmxvYWQnKSB7XG4gICAgICAgIHRzLndhbGtCeVNlbGVjdG9yKGNvbnRleHQsICcubWFwa2l0LWF1dG9jb21wbGV0ZS5hdXRvY29tcGxldGUtLXByb2Nlc3NlZCcsIChpdGVtKSA9PiB7XG4gICAgICAgICAgaXRlbS5jbGFzc0xpc3QucmVtb3ZlKCdhdXRvY29tcGxldGUtLXByb2Nlc3NlZCcpO1xuXG4gICAgICAgICAgLy8gSWYgdGhpcyBoYXMgYmVlbiBwcm9jZXNzZWQgYW5kIGFkZGVkIGFzIGFuIGF1dG9jb21wbGV0ZSBjbGVhbnVwLlxuICAgICAgICAgIGNvbnN0IGFjID0gdGhpcy5pbnN0YW5jZXMuZ2V0KGl0ZW0uaWQgfHwgaXRlbSk7XG4gICAgICAgICAgaWYgKGFjKSBhYy5kZXN0cm95KCk7XG4gICAgICAgIH0pO1xuICAgICAgfVxuICAgIH0sXG4gIH07XG59KShEcnVwYWwsIE1hcGtpdCk7XG4iXSwibWFwcGluZ3MiOiI7O0FBQUEsQ0FBQyxDQUFDO0VBQUVBLFNBQVM7RUFBRUMsUUFBUSxFQUFFQztBQUFHLENBQUMsRUFBRUMsTUFBTSxLQUFLO0VBQ3hDO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtFQUNFQSxNQUFNLENBQUNDLEVBQUUsR0FBRyxDQUFDLENBQUM7O0VBRWQ7QUFDRjtBQUNBO0FBQ0E7RUFDRUQsTUFBTSxDQUFDRSxZQUFZLEdBQUcsY0FBY0gsRUFBRSxDQUFDRyxZQUFZLENBQUM7SUFDbEQ7QUFDSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0lBQ0lDLFdBQVcsQ0FBQ0MsRUFBRSxFQUFFQyxNQUFNLEVBQUU7TUFDdEIsTUFBTUMsS0FBSyxHQUFHRixFQUFFLENBQUNHLHNCQUFzQixDQUFDLDJCQUEyQixDQUFDLENBQUMsQ0FBQyxDQUFDO01BRXZFLElBQUksQ0FBQ0QsS0FBSyxFQUFFO1FBQ1YsTUFBTSxJQUFJRSxLQUFLLENBQUMsb0NBQW9DLENBQUM7TUFDdkQ7TUFDQSxJQUFJLENBQUNSLE1BQU0sQ0FBQ0MsRUFBRSxDQUFDSSxNQUFNLENBQUNJLE9BQU8sQ0FBQyxFQUFFO1FBQzlCLE1BQU0sSUFBSUQsS0FBSyxDQUFFLDBDQUF5Q0gsTUFBTSxDQUFDSSxPQUFRLEVBQUMsQ0FBQztNQUM3RTs7TUFFQTtNQUNBO01BQ0E7TUFDQTtNQUNBSixNQUFNLENBQUNLLGFBQWEsR0FBRyxLQUFLO01BQzVCLEtBQUssQ0FBQ0osS0FBSyxFQUFFRCxNQUFNLENBQUM7TUFFcEIsSUFBSSxDQUFDTSxPQUFPLEdBQUdQLEVBQUU7TUFDakIsSUFBSSxDQUFDUSxPQUFPLEdBQUcsS0FBSztNQUNwQixJQUFJLENBQUNILE9BQU8sR0FBRyxJQUFLVCxNQUFNLENBQUNDLEVBQUUsQ0FBQ0ksTUFBTSxDQUFDSSxPQUFPLENBQUMsQ0FBRSxJQUFJLENBQUM7O01BRXBEO01BQ0FWLEVBQUUsQ0FBQ2MsV0FBVyxDQUFDVCxFQUFFLEVBQUUsNEJBQTRCLEVBQUdVLE1BQU0sSUFBSztRQUMzRCxNQUFNO1VBQUVDO1FBQUksQ0FBQyxHQUFHRCxNQUFNLENBQUNFLE9BQU87UUFFOUIsSUFBSUQsR0FBRyxFQUFFO1VBQ1A7VUFDQTtVQUNBLElBQUlBLEdBQUcsS0FBSyxRQUFRLEVBQUU7WUFDcEIsSUFBSSxDQUFDRSxNQUFNLEdBQUdILE1BQU07VUFDdEI7VUFDQSxJQUFJLENBQUNMLE9BQU8sQ0FBQ1MsV0FBVyxDQUFDSCxHQUFHLEVBQUVELE1BQU0sQ0FBQztRQUN2QztNQUNGLENBQUMsQ0FBQztJQUNKOztJQUVBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtJQUNJSyxXQUFXLEdBQUc7TUFDWixPQUFPLElBQUksQ0FBQ1YsT0FBTyxDQUFDVyxRQUFRO0lBQzlCOztJQUVBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7SUFDSUMsSUFBSSxHQUFHO01BQ0wsSUFBSSxDQUFDWixPQUFPLENBQUNZLElBQUksRUFBRTs7TUFFbkI7TUFDQSxJQUFJQyxNQUFNLENBQUNDLGVBQWUsS0FBSyxLQUFLLElBQUlDLFNBQVMsSUFBSUEsU0FBUyxDQUFDQyxXQUFXLEVBQUU7UUFDMUUxQixFQUFFLENBQUNjLFdBQVcsQ0FBQyxJQUFJLENBQUNGLE9BQU8sRUFBRSx1Q0FBdUMsRUFBR2UsSUFBSSxJQUFLO1VBQzlFLElBQUksQ0FBQ0MsZUFBZSxHQUFHRCxJQUFJO1VBQzNCQSxJQUFJLENBQUNFLEtBQUssQ0FBQ0MsT0FBTyxHQUFHLEVBQUU7VUFFdkIsSUFBSSxDQUFDQyxtQkFBbUIsR0FBRyxJQUFJLENBQUNBLG1CQUFtQixDQUFDQyxJQUFJLENBQUMsSUFBSSxDQUFDO1VBQzlETCxJQUFJLENBQUNNLGdCQUFnQixDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUNGLG1CQUFtQixDQUFDO1FBQzFELENBQUMsQ0FBQztNQUNKOztNQUVBO01BQ0E7TUFDQSxJQUFJLElBQUksQ0FBQ0csU0FBUyxJQUFJLElBQUksQ0FBQ0EsU0FBUyxDQUFDQyxNQUFNLEVBQUU7UUFDM0MsTUFBTTtVQUFFRDtRQUFVLENBQUMsR0FBRyxJQUFJO1FBQzFCLE9BQU8sSUFBSSxDQUFDQSxTQUFTO1FBQ3JCLElBQUksQ0FBQ0UsZ0JBQWdCLENBQUNGLFNBQVMsQ0FBQztNQUNsQztJQUNGOztJQUVBO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtJQUNJSCxtQkFBbUIsQ0FBQ00sQ0FBQyxFQUFFO01BQ3JCQSxDQUFDLENBQUNDLGNBQWMsRUFBRTtNQUVsQmIsU0FBUyxDQUFDQyxXQUFXLENBQUNhLGtCQUFrQixDQUFFQyxHQUFHLElBQUs7UUFDaEQsSUFBSSxJQUFJLENBQUN0QixNQUFNLEVBQUU7VUFDZixJQUFJLENBQUNoQixFQUFFLENBQUN1QyxLQUFLLEdBQUcsYUFBYTtVQUM3QixJQUFJLENBQUN2QixNQUFNLENBQUN1QixLQUFLLEdBQUksR0FBRUQsR0FBRyxDQUFDRSxNQUFNLENBQUNDLFFBQVEsQ0FBQ0MsT0FBTyxDQUFDLENBQUMsQ0FBRSxJQUFHSixHQUFHLENBQUNFLE1BQU0sQ0FBQ0csU0FBUyxDQUFDRCxPQUFPLENBQUMsQ0FBQyxDQUFFLEVBQUM7UUFDNUY7TUFDRixDQUFDLEVBQUUsTUFBTTtRQUNQO1FBQ0FFLEtBQUssQ0FBQyxtQ0FBbUMsQ0FBQztNQUM1QyxDQUFDLENBQUM7SUFDSjs7SUFFQTtBQUNKO0FBQ0E7SUFDSUMsVUFBVSxDQUFDQyxJQUFJLEVBQUU7TUFDZixJQUFJLENBQUNDLGdCQUFnQixFQUFFO01BQ3ZCLElBQUksQ0FBQ0MsZUFBZSxFQUFFOztNQUV0QjtNQUNBLElBQUksQ0FBQ2hELEVBQUUsQ0FBQ3VDLEtBQUssR0FBR08sSUFBSSxDQUFDRyxJQUFJO01BQ3pCLElBQUksQ0FBQ3pDLE9BQU8sQ0FBQzBDLE1BQU0sQ0FBQ0osSUFBSSxDQUFDO0lBQzNCOztJQUVBO0FBQ0o7QUFDQTtJQUNJWixnQkFBZ0IsQ0FBQ2UsSUFBSSxFQUFFO01BQ3JCLElBQUksQ0FBQ3pDLE9BQU8sQ0FBQzJDLGdCQUFnQixFQUFFOztNQUUvQjtNQUNBLElBQUksQ0FBQ0YsSUFBSSxJQUFJQSxJQUFJLENBQUNoQixNQUFNLEdBQUcsSUFBSSxDQUFDN0IsTUFBTSxDQUFDZ0QsU0FBUyxFQUFFO1FBQ2hEO01BQ0Y7TUFFQSxJQUFJLElBQUksQ0FBQ3pDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQ0gsT0FBTyxDQUFDNkMsT0FBTyxFQUFFLEVBQUU7UUFDM0M7UUFDQTtRQUNBLElBQUksQ0FBQ3JCLFNBQVMsR0FBR2lCLElBQUk7UUFDckI7TUFDRjs7TUFFQTtNQUNBO01BQ0EsSUFBSSxDQUFDakQsRUFBRSxDQUFDc0QsUUFBUSxDQUFDLHlCQUF5QixDQUFDO01BQzNDLElBQUksQ0FBQzNDLE9BQU8sR0FBRyxJQUFJOztNQUVuQjtNQUNBLElBQUksQ0FBQ0gsT0FBTyxDQUFDMEIsZ0JBQWdCLENBQUNlLElBQUksQ0FBQztJQUNyQzs7SUFFQTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7SUFDSU0scUJBQXFCLENBQUNDLFNBQVMsRUFBRTtNQUMvQixJQUFJLENBQUM3QyxPQUFPLEdBQUcsS0FBSztNQUVwQixJQUFJNkMsU0FBUyxFQUFFO1FBQ2IsSUFBSSxDQUFDQyxpQkFBaUIsQ0FBQ0QsU0FBUyxDQUFDO1FBQ2pDLElBQUksQ0FBQ0Usa0JBQWtCLEVBQUU7TUFDM0IsQ0FBQyxNQUNJO1FBQ0gsSUFBSSxDQUFDWCxnQkFBZ0IsRUFBRTtRQUN2QixJQUFJLENBQUNDLGVBQWUsRUFBRTtNQUN4Qjs7TUFFQTtNQUNBO01BQ0EsSUFBSSxJQUFJLENBQUNoQixTQUFTLElBQUksSUFBSSxDQUFDQSxTQUFTLENBQUNDLE1BQU0sRUFBRTtRQUMzQyxNQUFNO1VBQUVEO1FBQVUsQ0FBQyxHQUFHLElBQUk7UUFDMUIsT0FBTyxJQUFJLENBQUNBLFNBQVM7UUFFckIsSUFBSSxDQUFDRSxnQkFBZ0IsQ0FBQ0YsU0FBUyxDQUFDO01BQ2xDLENBQUMsTUFDSTtRQUNILElBQUksQ0FBQ2hDLEVBQUUsQ0FBQzJELFdBQVcsQ0FBQyx5QkFBeUIsQ0FBQztNQUNoRDtJQUNGOztJQUVBO0FBQ0o7QUFDQTtJQUNJQyxPQUFPLEdBQUc7TUFDUixJQUFJLENBQUNwRCxPQUFPLENBQUNvRCxPQUFPLEVBQUU7TUFDdEIsT0FBTyxJQUFJLENBQUNwRCxPQUFPO01BRW5CLElBQUksSUFBSSxDQUFDa0IsZUFBZSxFQUFFO1FBQ3hCLElBQUksQ0FBQ0EsZUFBZSxDQUFDbUMsbUJBQW1CLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQ2hDLG1CQUFtQixDQUFDO1FBQzNFLE9BQU8sSUFBSSxDQUFDSCxlQUFlO01BQzdCO01BRUEsT0FBTyxJQUFJLENBQUNmLE9BQU87TUFDbkIsT0FBTyxJQUFJLENBQUNxQixTQUFTO01BRXJCLEtBQUssQ0FBQzRCLE9BQU8sRUFBRTtJQUNqQjtFQUNGLENBQUM7O0VBRUQ7QUFDRjtBQUNBO0VBQ0VoRSxTQUFTLENBQUNrRSxrQkFBa0IsR0FBRztJQUM3QkMsU0FBUyxFQUFFLElBQUlDLEdBQUcsRUFBRTtJQUNwQkMsU0FBUyxFQUFFLElBQUk7SUFFZkMsTUFBTSxDQUFDQyxPQUFPLEVBQUU7TUFDZHJFLEVBQUUsQ0FBQ2MsV0FBVyxDQUFDdUQsT0FBTyxFQUFFLHFCQUFxQixFQUFHckIsSUFBSSxJQUFLO1FBQ3ZELE1BQU0xQyxNQUFNLEdBQUcwQyxJQUFJLENBQUMvQixPQUFPLENBQUNxRCxZQUFZLEdBQUdDLElBQUksQ0FBQ0MsS0FBSyxDQUFDeEIsSUFBSSxDQUFDL0IsT0FBTyxDQUFDcUQsWUFBWSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBRXJGLElBQUk7VUFDRixNQUFNcEUsRUFBRSxHQUFHLElBQUlELE1BQU0sQ0FBQ0UsWUFBWSxDQUFDNkMsSUFBSSxFQUFFMUMsTUFBTSxDQUFDO1VBQ2hETCxNQUFNLENBQUN3RSxPQUFPLENBQUN2RSxFQUFFLENBQUNrQixXQUFXLEVBQUUsRUFBRWxCLEVBQUUsQ0FBQ29CLElBQUksQ0FBQ1UsSUFBSSxDQUFDOUIsRUFBRSxDQUFDLENBQUM7VUFDbEQsSUFBSSxDQUFDK0QsU0FBUyxDQUFDUyxHQUFHLENBQUMxQixJQUFJLENBQUMyQixFQUFFLElBQUkzQixJQUFJLEVBQUU5QyxFQUFFLENBQUM7UUFDekMsQ0FBQyxDQUNELE9BQU9tQyxDQUFDLEVBQUU7VUFDUjtVQUNBdUMsT0FBTyxDQUFDQyxLQUFLLENBQUN4QyxDQUFDLENBQUM7UUFDbEI7TUFDRixDQUFDLEVBQUUseUJBQXlCLENBQUM7SUFDL0IsQ0FBQztJQUVEeUMsTUFBTSxDQUFDVCxPQUFPLEVBQUVVLFFBQVEsRUFBRUMsT0FBTyxFQUFFO01BQ2pDLElBQUlBLE9BQU8sS0FBSyxRQUFRLEVBQUU7UUFDeEJoRixFQUFFLENBQUNpRixjQUFjLENBQUNaLE9BQU8sRUFBRSw4Q0FBOEMsRUFBR3JCLElBQUksSUFBSztVQUNuRkEsSUFBSSxDQUFDa0MsU0FBUyxDQUFDQyxNQUFNLENBQUMseUJBQXlCLENBQUM7O1VBRWhEO1VBQ0EsTUFBTWpGLEVBQUUsR0FBRyxJQUFJLENBQUMrRCxTQUFTLENBQUNtQixHQUFHLENBQUNwQyxJQUFJLENBQUMyQixFQUFFLElBQUkzQixJQUFJLENBQUM7VUFDOUMsSUFBSTlDLEVBQUUsRUFBRUEsRUFBRSxDQUFDNEQsT0FBTyxFQUFFO1FBQ3RCLENBQUMsQ0FBQztNQUNKO0lBQ0Y7RUFDRixDQUFDO0FBQ0gsQ0FBQyxFQUFFdUIsTUFBTSxFQUFFcEYsTUFBTSxDQUFDIn0=
