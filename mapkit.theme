<?php

/**
 * @file
 * Preprocess, and theme hooks for mapkit module.
 */

use Drupal\Component\Utility\Html;
use Drupal\Core\Render\Element;
use Drupal\Core\Template\Attribute;

/**
 * Implements hook_preprocess_mapkit_autocomplete().
 */
function template_preprocess_mapkit_autocomplete(array &$variables) {
  $element = &$variables['element'];

  // Pass elements disabled status to template.
  $variables['disabled'] = $element['#disabled'] ?? FALSE;

  foreach (Element::children($element) as $key) {
    $input = $element[$key];
    if ($input['#id']) {
      $input['#attributes']['id'] = $input['#id'];
    }

    $variables['inputs'][$key] = $input;
  }

  // Labels and errors are all based on the internal text input form element.
  $textInput = &$variables['inputs']['text'];
  $variables['errors'] = $textInput['#errors'] ?? [];

  if (!empty($element['#device_location'])) {
    $variables['geolocation_link'] = [
      '#theme' => 'mapkit_geolocation_link',
    ];
  }

  // Apply the label and make sure it has the correct attributes like "for".
  if (!empty($element['#title'])) {
    $variables['label_display'] = $element['#title_display'] ?? 'before';

    $variables['label'] = [
      '#theme' => 'form_element_label',
      '#title' => $element['#title'],
      '#title_display' => $variables['label_display'],
      '#required' => $element['#required'] ?? FALSE,
      '#attributes' => $element['#label_attributes'] ?? [],
    ];

    if (!empty($textInput['#id'])) {
      $variables['label']['#for'] = $textInput['#id'];
      $variables['label']['#id'] = $textInput['#id'] . '--label';
    }
  }
  else {
    $variables['title_display'] = 'none';
  }

  // Manage any input instructions or descriptions.
  if (!empty($element['#description'])) {
    $variables['description_display'] = $element['#description_display'] ?? 'after';

    $description_attributes = [];
    if (!empty($textInput['#id'])) {
      $description_attributes['id'] = $textInput['#id'] . '--description';
    }

    $variables['description'] = [
      'attributes' => new Attribute($description_attributes),
      'content' => $element['#description'],
    ];
  }
}

/**
 * Implements hook_preprocess_mapkit_geolocation_link().
 */
function template_preprocess_mapkit_geolocation_link(&$variables) {
  $variables['attributes']['href'] = '#';
  $variables['attributes']['style'] = 'display:none';
  $variables['attributes']['class'][] = 'mapkit-autocomplete__geolocation-link';
}

/**
 * Implements hook_preprocess_mapkit_location_list().
 */
function template_preprocess_mapkit_location_list(&$variables) {
  $variables['attributes']['class'][] = 'location-list';
  $variables['attributes']['class'][] = 'js-mapkit-location-list';

  // Add attributes needed for the JS to find and bind data to each location.
  foreach ($variables['locations'] as $delta => &$location) {
    $location['attributes'] = new Attribute([
      'class' => ['location-item'],
      'data-index' => $delta,
    ]);

    // By default the first location is the one which is visible.
    if ($delta == 0) {
      $location['attributes']['class'][] = 'location-item--active';
    }
  }
  unset($location);
}

/**
 * Implements hook_preprocess_mapkit_map().
 */
function template_preprocess_mapkit_map(&$variables) {
  $domId = Html::getUniqueId($variables['map_id']);
  $variables['attributes']['id'] = $domId;
  $variables['attributes']['class'][] = 'js-mapkit-map';
  $variables['attributes']['style'] = "width: {$variables['width']}; height: {$variables['height']};";

  // Include the base map handling functionality.
  $provider = $variables['map_provider'];

  $jsSettings = [];
  $jsSettings['map'] = $provider->getJsSettings();
  $jsSettings['map']['type'] = $provider->getPluginId();
  $jsSettings['markers'] = [];
  $jsSettings['data'] = $variables['data'];

  // Load and provide the marker configurations.
  $markerLibs = [];
  foreach ($variables['markers'] as $markerId => $marker) {
    $markerLibs = array_merge($markerLibs, $marker->getLibraries($provider));
    $jsSettings['markers'][$markerId] = $marker->getJsSettings($provider);
  }

  $variables['#attached']['library'][] = 'mapkit/map-display';
  $variables['#attached']['library'] = array_merge(
    $variables['#attached']['library'],
    $provider->getLibraries(),
    array_unique($markerLibs)
  );

  $variables['#attached']['drupalSettings']['Mapkit']['instance'][$domId] = $jsSettings;
}
