(({ behaviors, Toolshed: ts }, Mapkit) => {
  /**
   * Location autocomplete provider plugins.
   *
   * The generator function should create a Map object that implements the
   * following prototype (still in progress). The map parameters should accept
   * common Mapkit marker settings, and allow the marker builder (Mapkit.marker)
   * handle the implementation and API specifics.
   *
   * See example class in places-autocomplete.es6.js from the mapkit_gmap
   * module for an example and more implementation details.
   *
   * Autocomplete.prototype = {
   *   isReady(),
   *   init(),
   *   attachInput(inputEl),
   *   clearInputValues(),
   *   formatSuggestText(item),
   *   selectItem(obj),
   *   fetchSuggestions(keywords),
   *   destroy(),
   * }
   *
   * @todo Write better documentation on map class prototypes.
   */
  Mapkit.ac = {};

  /**
   * Google Places API autocomplete handler to link an autocomplete
   * entry to a Drupal input filter.
   */
  Mapkit.Autocomplete = class extends ts.Autocomplete {
    /**
     * Create a new instance of the autocomplete handler. Requires a call to
     * Mapkit.Autocomplete.init() in order to initialize the autocomplete.
     *
     * @param {DOMElement} el
     *   The HTML input to submit the autocomplete results and interaction to.
     * @param {Object} config
     *   Configuration to use for limiting the bounds of the autocomplete.
     */
    constructor(el, config) {
      const input = el.getElementsByClassName('mapkit-autocomplete__text')[0];

      if (!input) {
        throw new Error('Invalid autocomplete input element');
      }
      if (!Mapkit.ac[config.handler]) {
        throw new Error(`Invalid autocomplete handler specified ${config.handler}`);
      }

      // The input elements here have special handling and the values need to
      // be managed by this class.
      // This prevents Toolshed autocomplete from managing values of the
      // textfield inputs and autocomplete elements.
      config.separateValue = false;
      super(input, config);

      this.wrapper = el;
      this.pending = false;
      this.handler = new (Mapkit.ac[config.handler])(this);

      // Find the child input elements and bind them to the AC handler.
      ts.walkByClass(el, 'mapkit-autocomplete__value', (dataEl) => {
        const { key } = dataEl.dataset;

        if (key) {
          // Capture this input for device_location to know where to put the
          // lat/long data from the device / browser.
          if (key === 'latlng') {
            this.latlng = dataEl;
          }
          this.handler.attachInput(key, dataEl);
        }
      });
    }

    /**
     * The loader queue identifier of the autocomplete provider. This is used
     * for providers that need to asynchronously load dependencies, and will
     * trigger an initializer when the libraries have completed loading.
     *
     * @return {string|undefined|null}
     *   The loader identifier to register the init() callback to. If not
     *   provided this handler does not require a loader queue.
     */
    getLoaderId() {
      return this.handler.loaderId;
    }

    /**
     * Should be called after the Google Maps API scripts have been loaded.
     * This function will initialize the Places Autocomplete and hook-up
     * the autocomplete functionality to the input element.
     */
    init() {
      this.handler.init();

      // Attach the device location functionality if it was enabled here.
      if (window.isSecureContext !== false && navigator && navigator.geolocation) {
        ts.walkByClass(this.wrapper, 'mapkit-autocomplete__geolocation-link', (link) => {
          this.geolocationLink = link;
          link.style.display = '';

          this.onClientGeolocation = this.onClientGeolocation.bind(this);
          link.addEventListener('click', this.onClientGeolocation);
        });
      }

      // If a query before initialization, fetch the suggestions now that the
      // dependencies are loaded.
      if (this.lastQuery && this.lastQuery.length) {
        const { lastQuery } = this;
        delete this.lastQuery;
        this.fetchSuggestions(lastQuery);
      }
    }

    /**
     * The click callback for the geolocation link for the mapkit autocomplete.
     *
     * @param {MouseClickEvent} e
     *   Mouse click event.
     */
    onClientGeolocation(e) {
      e.preventDefault();

      navigator.geolocation.getCurrentPosition((pos) => {
        if (this.latlng) {
          this.ac.value = 'My location';
          this.latlng.value = `${pos.coords.latitude.toFixed(7)},${pos.coords.longitude.toFixed(7)}`;
        }
      }, () => {
        // eslint-disable-next-line no-alert
        alert('Unable to retrieve your location.');
      });
    }

    /**
     * @inheritdoc
     */
    selectItem(item) {
      this.clearSuggestions();
      this.hideSuggestions();

      // Set the display text into the form element.
      this.ac.value = item.text;
      this.handler.select(item);
    }

    /**
     * @inheritdoc
     */
    fetchSuggestions(text) {
      this.handler.clearInputValues();

      // Ensure a minimal query value to fetch suggestions for.
      if (!text || text.length < this.config.minLength) {
        return;
      }

      if (this.pending || !this.handler.isReady()) {
        // Unable to cancel the existing request, so instead we'll store
        // any updates for when the pending request completes.
        this.lastQuery = text;
        return;
      }

      // Turn on the loading spinner, and let the system know we already
      // have a request in progress.
      this.ac.addClass('ui-autocomplete-loading');
      this.pending = true;

      // Get the suggestions from the autocomplete handler.
      this.handler.fetchSuggestions(text);
    }

    /**
     * Suggests have been returned and should be presented.
     *
     * This can be empty list of suggestions, which just indicates that the
     * pending suggestions fetch has been completed.
     *
     * @param {object|null} suggested
     *   List of suggested items to build into the suggestions pane.
     */
    setFetchedSuggestions(suggested) {
      this.pending = false;

      if (suggested) {
        this.createSuggestions(suggested);
        this.displaySuggestions();
      }
      else {
        this.clearSuggestions();
        this.hideSuggestions();
      }

      // If there was another query requested while the previous one was
      // pending, go request that now that we've completed the previous one.
      if (this.lastQuery && this.lastQuery.length) {
        const { lastQuery } = this;
        delete this.lastQuery;

        this.fetchSuggestions(lastQuery);
      }
      else {
        this.ac.removeClass('ui-autocomplete-loading');
      }
    }

    /**
     * Clean up event listeners and create DOM elements.
     */
    destroy() {
      this.handler.destroy();
      delete this.handler;

      if (this.geolocationLink) {
        this.geolocationLink.removeEventListener('click', this.onClientGeolocation);
        delete this.geolocationLink;
      }

      delete this.pending;
      delete this.lastQuery;

      super.destroy();
    }
  };

  /**
   * Drupal behaviors for rigging up a Mapkit autocomplete input.
   */
  behaviors.mapkitAutocomplete = {
    instances: new Map(),
    urlParams: null,

    attach(context) {
      ts.walkByClass(context, 'mapkit-autocomplete', (item) => {
        const config = item.dataset.autocomplete ? JSON.parse(item.dataset.autocomplete) : {};

        try {
          const ac = new Mapkit.Autocomplete(item, config);
          Mapkit.addInit(ac.getLoaderId(), ac.init.bind(ac));
          this.instances.set(item.id || item, ac);
        }
        catch (e) {
          // Unable to attach autocomplete functionality.
          console.error(e);
        }
      }, 'autocomplete--processed');
    },

    detach(context, settings, trigger) {
      if (trigger === 'unload') {
        ts.walkBySelector(context, '.mapkit-autocomplete.autocomplete--processed', (item) => {
          item.classList.remove('autocomplete--processed');

          // If this has been processed and added as an autocomplete cleanup.
          const ac = this.instances.get(item.id || item);
          if (ac) ac.destroy();
        });
      }
    },
  };
})(Drupal, Mapkit);
