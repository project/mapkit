(({ Toolshed: ts, Mapkit }) => {
  /**
   * Loading behaviors for mapkit map displays.
   */
  Drupal.behaviors.mapkitMap = {
    maps: new Map(),

    attach(context, settings) {
      const instances = settings.Mapkit.instance;

      // Find each of the locations views and build locations and maps.
      ts.walkByClass(context, 'js-mapkit-map', (el) => {
        if (el.id && instances[el.id]) {
          const { map: mapDef, markers, data } = instances[el.id];
          const map = Mapkit.createMap(el, mapDef, markers || []);

          // Create a map marker for each marker
          data.forEach((pt) => {
            map.createMarker({ latLng: pt }, false);
          });

          this.maps.set(el.id, map);
        }
      }, 'mapkit-map--processed');
    },

    detach(context, settings, trigger) {
      if (trigger !== 'unload') return;

      // Remove behaviors and clean up map / views resources. Important for AJAX
      // loading and unloading of generated views and map resources.
      ts.walkByClass(context, 'mapkit-map--processed', (el) => {
        const instance = this.maps.get(el.id);

        if (instance) {
          this.maps.delete(el.id);
          instance.destroy();
        }
      });
    },
  };
})(Drupal, drupalSettings);
