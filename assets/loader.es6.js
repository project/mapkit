(() => {
  /**
   *
   */
  class LoadQueue {
    constructor() {
      this.queue = [];
    }

    /**
     * Called when library dependencies are loaded and ready for use.
     *
     * This calls all queued initializers and changes the queue into add()
     * method so the methods immediately execute.
     */
    init() {
      // Run all queued functions, ensuring to remove them as we do, so there
      // are no references are kept after we're done with them here. This may
      // help with the JS garbage collector.
      let cb = this.queue.pop();
      while (cb) {
        const func = cb.shift();
        func(...cb);
        cb = this.queue.pop();
      }

      // Replaces the array.prototype.push function so that all future
      // push calls just execute the callback immediately.
      this.queue.push = ([func, ...args]) => func(...args);
    }

    /**
     * Add or execute a callback. If the provider dependencies are loaded, the
     * callback is called immediately (array.push), otherwise it is queue until
     * the this.init() method is called.
     *
     * This allows functionality that is dependent on provider libraries to
     * be used without having to worry about the load state of the libraries.
     *
     * @param {array|function} data
     *   Either a function to add to the callbacks queue or an array
     *   with the callback as the first item, and the function parameters
     *   in the array following the callback.
     */
    add(data) {
      if (typeof data === 'function') {
        this.queue.push([data]);
      }
      else if (Array.isArray(data) && typeof data[0] === 'function') {
        this.queue.push(data);
      }
      else {
        throw new Error('Method expects either a function or an array with a callback.');
      }
    }

    /**
     * Search for a callback init function to remove. This can will only remove
     * callbacks which are still pending. After they are run they are no longer
     * tracked and don't need to be removed anymore.
     *
     * @param {function} handler
     *   The callback handler to remove from initialization.
     */
    remove(handler) {
      // Go hunting for the callback to remove.
      for (let i = 0; i < this.queue.length; ++i) {
        const data = this.queue[i];

        if (data[0] === handler) {
          this.queue.splice(i, 1);
        }
      }
    }
  }

  /**
   * Maintains a queue of initialization functions for when the Mapkit
   * dependent libraries are loaded.
   *
   * Add initializer functions to the queue by calling "addInit()" and providing
   * the callback. The value pushed should be an array, with the first value
   * being a callable function, followed by each of the arguments for that
   * function, if there are any.
   *
   * EX: window.Mapkit.addInit('gmap', [func, arg1, arg2, arg3]);
   */
  window.Mapkit = {
    loader: {},

    /**
     * Creates an loader queue for a Mapkit provider.
     *
     * @param {string} id
     *   Idenitifier for the loader queue to create.
     */
    createQueue(id) {
      if (!this.loader[id]) {
        this.loader[id] = new LoadQueue();
      }
    },

    /**
     * Add or execute a callback. To the specified loader queue.
     *
     * Ensure that the queue has been created before adding the initializers
     * by calling "Mapkit.createQueue(id)".
     *
     * @param {string} id
     *   The loader queue to add the callback to.
     * @param {array|function} data
     *   Either a function to add to the callbacks queue or an array
     *   with the callback as the first item, and the function parameters
     *   in the array following the callback.
     */
    addInit(id, data) {
      this.loader[id].add(data);
    },

    /**
     * Search for an init function to remove from the specified loader queue.
     *
     * @param {string} id
     *   The loader queue to search for and remove this handler.
     * @param {function} handler
     *   The callback handler to remove from initialization.
     */
    removeInit(id, handler) {
      if (this.loader[id]) {
        this.laoder[id].remove(handler);
      }
    },
  };
})();
