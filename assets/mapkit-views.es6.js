(({ Toolshed: ts, Mapkit }) => {
  /**
   * A single location entry, which connects the result row to a map marker. A
   * location can have several lat/lng coordinates but only one may be active
   * at a time.
   */
  class Location {
    constructor(el, data, view) {
      this.el = el;
      this.active = 0;
      this.marker = null;
      this.data = [];
      this.list = el.getElementsByClassName('js-mapkit-location-list').item(0);

      const displays = [];
      if (this.list && data.length > 1) {
        ts.walkByClass(this.list, 'location-item', (item) => {
          displays[item.dataset.index] = item;
        });
      }

      // Add the geolocations for this location.
      let minDist = null;
      let closest = 0;
      data.forEach((pt, key) => {
        this.data[key] = {
          display: displays[key],
          latLng: pt,
          distance: null,
        };

        // If not the currently active display, hide the location result.
        if (this.active !== key && displays[key]) {
          displays[key].style.display = 'none';
        }

        // If there is a reference point, we can calculate distances. With
        // distances we set the closest geo-point as the active point.
        if (view.refPt) {
          const dist = Mapkit.calcDist(view.refPt, pt, view.units);
          this.data[key].distance = dist;

          if (minDist === null || minDist > dist) {
            closest = key;
            minDist = dist;
          }
        }
      });

      if (this.active !== closest) {
        this.setActive(closest);
      }
    }

    /**
     * Getter to get the currently active lat/lng for this location.
     *
     * @return {object|null}
     *   The active lat/lng coordinate for this location entry.
     */
    get latLng() {
      return (this.data[this.active] || {}).latLng;
    }

    /**
     * The gets the active location display.
     */
    get display() {
      return (this.data[this.active] || {}).display;
    }

    /**
     * Change the active location based on index. This updates the displayed
     * marker position if marker was already rendered.
     *
     * @param {int} index
     *   The index of the location to make active.
     */
    setActive(index) {
      // Hide the previously selected item.
      const prev = this.display;
      if (prev) {
        prev.style.display = 'none';
        prev.classList.remove('location-item--active');
      }

      this.active = index;

      // Make newly selected location visible.
      const curr = this.display;
      if (curr) {
        curr.style.display = '';
        curr.classList.add('location-item--active');
      }

      if (this.marker) {
        this.marker.position = this.latLng;
      }
    }
  }

  /**
   * Mapkit views style plugin object, which maintains the JS state, map
   * and location information for each of the views results.
   */
  class MapkitLocationView {
    constructor(el, settings, data) {
      this.el = el;

      // Apply location settings data settings to this instance.
      this.units = settings.units || 'km';
      this.precision = settings.percision || 2;
      this.refPt = settings.refLocation;
      this.locations = new Map();

      // Configure the map and popup event handlers.
      this.markerPopupContent = settings.markerPopupContent;
      this.events = settings.events || {};

      // Find and build each of the rows of data into coordinate map data.
      // These data locations will later be used to place map markers during
      // populateMarkers() if a map has been attached.
      ts.walkBySelector(el, settings.rowSelector || '.views-row', (rowEl) => {
        const { dataId } = rowEl.dataset;

        if (data[dataId]) {
          this.locations.set(dataId, new Location(rowEl, data[dataId], this));
        }
      });

      if (settings.map) {
        const mapEl = el.getElementsByClassName('js-view-mapkit-style-map')[0];

        if (mapEl) {
          mapEl.style.width = settings.mapWidth || '100%';
          mapEl.style.height = settings.mapHeight || '500px';
          this.map = Mapkit.createMap(mapEl, settings.map, settings.markers);

          if (this.events.onMapCreate) {
            this.events.onMapCreate.forEach((cb) => cb(this, this.map));
          }

          this.populateMarkers();
        }
      }
    }

    /**
     * Render all the map markers for each of the current map locations.
     */
    populateMarkers() {
      if (!this.map) return;

      this.locations.forEach((loc) => {
        if (loc.latLng && !loc.marker) {
          loc.marker = this.map.createMarker({ latLng: loc.latLng }, false);

          // If there are registered "onCreateMarker" listeners, apply them to
          // each of the newly created markers.
          if (this.events.onMarkerCreate) {
            this.events.onMarkerCreate.forEach((cb) => cb(this, loc, loc.marker));
          }

          // If there is a callback function to fetch contents for a marker
          // popup window, attach an "onClick" handler to trigger the popup.
          if (this.markerPopupContent instanceof Function) {
            loc.marker.on('click', () => {
              const content = this.markerPopupContent(this, loc);
              if (content) this.map.openMarkerPopup(loc.marker, content);
            });
          }
        }
      });
    }

    /**
     * Clean up map resources and event listeners.
     */
    destroy() {
      if (this.map) {
        if (this.events.onMapDestroy) {
          this.events.onMapDestroy.forEach((cb) => cb(this, this.map));
        }

        this.map.destroy();
      }
    }
  }

  /**
   * Load behaviors for mapkit location views style.
   */
  Drupal.behaviors.mapkitViewStyle = {
    dataKeyRegex: /(?:\s|^)((?:js-view-dom-id-)[^\s]+)/,
    views: new Map(),

    attach(context, settings) {
      const instances = (settings.mapkitViews || {}).instance;

      // Find each of the locations views and build locations and maps.
      ts.walkByClass(context, 'view-content', (el) => {
        const view = el.closest('.view');
        const match = view ? this.dataKeyRegex.exec(view.getAttribute('class')) : '';

        if (match && instances[match[1]]) {
          const data = instances[match[1]].locations;
          const config = Mapkit.config.applyOverrides(instances[match[1]]);
          this.views.set(match[1], new MapkitLocationView(el, config, data));
        }
      }, 'mapkit-view-processed');
    },

    detach(context, settings, trigger) {
      if (trigger !== 'unload') return;

      // Remove behaviors and clean up map / views resources. Important for AJAX
      // loading and unloading of generated views and map resources.
      ts.walkByClass(context, 'view-content', (el) => {
        const view = el.closest('.view');
        const match = view ? this.dataKeyRegex.exec(view.getAttribute('class')) : null;
        const instance = match ? this.views.get(match[1]) : null;

        if (instance) {
          this.views.delete(match[1]);
          instance.destroy();
        }
      });
    },
  };
})(Drupal, drupalSettings);
