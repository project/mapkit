Drupal.Mapkit = {} || Drupal.Mapkit;

(({ Mapkit }) => {
  /**
   * Map generator functions, keyed by the map plugin ID.
   *
   * The generator function should create a Map object that implements the
   * following prototype (still in progress). The map parameters should accept
   * common Mapkit marker settings, and allow the marker builder (Mapkit.marker)
   * handle the implementation and API specifics.
   *
   * See Gmap in mapkit_gmap/assets/gmap.es6.js from the mapkit_gmap module.
   *
   * Map.prototype = {
   *   getCenter(),
   *   setCenter({ lat, lng }),
   *   addMarker(markerObj),
   *   removeMarker(markerObj),
   * }
   *
   * @todo Write better documentation on map class prototypes.
   */
  Mapkit.handler = {};

  /**
   * Creates a Marker factory function, which creates the markers based on the
   * marker set configurations. The MarkerFactory is a function which generates
   * Marker objects that implement the following prototype.
   *
   * Marker.prototype = {
   *   get position(),
   *   set position({ lat, lng }),
   *   getHtml(),
   *   attach(map),
   *   detach(),
   *   on(event, callback),
   *   off(event, callback=),
   * }
   */
  Mapkit.marker = {};

  /**
   * Creates a map with map definition settings. Settings should contain info
   * about the Map type and options, and information about what map marker sets
   * are allowed for the map.
   *
   * @param {HTMLElement} el
   *   The HTML element to use place the generate map instance into.
   * @param {Object} mapDef
   *   The map definition settings, which must include a valid Mapkit handler
   *   map type. The map determines what kind of map get generated and what
   *   configurations are needed.
   * @param {Array|Object} markers
   *   Marker configurations that are used with this map.
   *
   * @return {Object}
   *   The generated map instance, build from the provided configurations.
   */
  Mapkit.createMap = function createMap(el, mapDef, markers) {
    const plugin = Mapkit.handler[mapDef.type] || false;

    if (plugin) {
      const mb = {};

      // Create the marker builders that are configured with this map.
      if (markers) {
        if (!Array.isArray(markers)) {
          markers = [markers];
        }

        markers.forEach((set) => {
          if (Mapkit.marker[set.type]) {
            if (!mapDef.defaultMarkerSet) mapDef.defaultMarkerSet = set.type;

            mb[set.type] = Mapkit.marker[set.type](set);
          }
        });
      }

      return plugin(el, mapDef, mb);
    }
  };

  /**
   * Manage mapkit configurations and support overriding of map functionality.
   */
  Mapkit.config = {
    events: [
      'onMapCreate',
      'onMapDestory',
      'onMarkerCreate',
    ],

    mergeConfig(base, overrides) {
      // Split the configurations into different settings based on how they
      // should be merged together.
      //  - Events are each arrays of callbacks, which should be additive.
      //  - Map settings are merge together.
      //  - All other configs are directly overridden.
      const { events, map, ...rest } = overrides;
      const config = { ...base, ...rest };

      // Merge the map settings.
      if (map) config.map = { ...base.map, ...map };

      // Build a entirely new events object so a shallow copy of event handlers
      // doesn't overwrite the original configuration object unintentionally.
      if (events) {
        config.events = {};
        const currEvents = base.events || {};
        this.events.forEach((eName) => {
          if (events[eName]) {
            config.events[eName] = [
              ...(currEvents[eName] || []),
              ...events[eName],
            ];
          }
        });
      }

      return config;
    },

    /**
     * Apply any settings overrides and additional event listeners from global
     * settings or instance specific overrides.
     *
     * @see MapkitLocationView class for settings and events.
     *
     * @param {object} config
     *   The original configuration from the instance being build.
     *
     * @return {object}
     *   A new configuration object, with the overrides applied.
     */
    applyOverrides(config) {
      const settings = drupalSettings.mapkitViews || {};
      let result = config;

      // Global settings for all mapkit location views style.
      if (settings.globalSettings) {
        result = this.mergeConfig(config, settings.globalSettings);
      }

      // If this configuration is for a view.
      const { viewId, displayId } = config;
      const overrides = viewId && settings.view ? settings.view[viewId] : null;
      if (overrides) {
        // Apply any settings override for the view, if any are available.
        if (overrides.settings) {
          result = this.mergeConfig(result, overrides.settings);
        }

        // If there are display specific overrides, apply them after any of the
        // view specific overrides. This allows display specific settings to
        // take precedence.
        if (displayId && (overrides.display || {})[displayId]) {
          result = this.mergeCOnfig(result, overrides.display[displayId]);
        }
      }

      return result;
    },

    addOverrides(overrides, scope = 'global', id = '') {
      drupalSettings.mapkitViews = drupalSettings.mapkitViews || {};
      const settings = drupalSettings.mapkitViews;

      switch (scope) {
        case 'global':
          settings.globalSettings = this.mergeConfig(settings.globalSettings || {}, overrides);
          break;

        case 'view':
          if (id) {
            const [view, display] = id.split(':');
            settings.view = settings.view || {};
            settings.view[view] = settings.view[view] || {};
            const viewConf = settings.view[view];

            if (display) {
              viewConf.display = viewConf.display || {};
              viewConf.display[display] = this.mergeConfig(viewConf.display[display], overrides);
            }
            else {
              viewConf.settings = this.mergeConfig(viewConf.settings, overrides);
            }
          }
          break;

        default:
          throw new Error('Override scope must be either "view" or "global".');
      }
    },
  };

  /**
   * Convert degrees to radians.
   *
   * @param {Number} deg
   *   The value in degrees.
   *
   * @return {Number}
   *   The value converted to radians.
   */
  Mapkit.deg2Rad = function deg2Rad(deg) {
    return (deg * Math.PI) / 180;
  };

  /**
   * Haversine formula to calculate distance in of two points on Earth.
   *
   * @param {Object} from
   *   A location object with lat and lng.
   * @param {Object} to
   *   A location object with lat and lng.
   * @param {string} unit
   *   The distance unit to use in the result. Use 'mi' or 'mile(s)' to get
   *   the results in miles, otherwise, the distance will be in kilometers.
   *
   * @return {Number}
   *   The calculated distance between two points on Earth. NaN is returned
   *   when a coordinate is invalid.
   */
  Mapkit.calcDist = function calcDist(from, to, unit = 'km') {
    // Mean radius of Earth in miles or km.
    const r = /^mi(les?)?/i.test(unit) ? 3963.1676 : 6378.1;

    // If any coordinates are missing a component, then we won't be able to
    // calculate distances, so just return "NaN".
    if (!(from.lat && from.lng && to.lat && to.lng)) {
      return NaN;
    }

    const lat1 = this.deg2Rad(from.lat);
    const lat2 = this.deg2Rad(to.lat);
    const b = Math.sin((lat1 - lat2) / 2);
    const c = Math.sin(this.deg2Rad(from.lng - to.lng) / 2);

    // Compute the arc length between the Earth coordinates.
    const a = (b * b) + Math.cos(lat1) * Math.cos(lat2) * (c * c);
    const arc = 2 * Math.asin(Math.sqrt(a));

    // Arc length times the radius of the Earth is the distance.
    return arc * r;
  };
})(Drupal);
