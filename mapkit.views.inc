<?php

/**
 * @file
 * Views hooks and helper functions.
 */

use Drupal\mapkit\Plugin\views\field\MapkitDistanceField;
use Drupal\search_api\Plugin\views\query\SearchApiQuery;
use Drupal\toolshed\Utility\ViewsCache;
use Drupal\views\Plugin\views\cache\CachePluginBase;
use Drupal\views\Plugin\views\row\Fields;
use Drupal\views\ViewExecutable;

/**
 * Implements hook_views_data_alter().
 */
function mapkit_views_data_alter(array &$data) {
  /** @var \Drupal\search_api\IndexInterface[] $indexes */
  $indexes = Drupal::entityTypeManager()
    ->getStorage('search_api_index')
    ->loadMultiple();

  // Override search API distance fields with Mapkit's distance formatter.
  foreach ($indexes as $index) {
    $table = &$data['search_api_index_' . $index->id()];
    $fields = $index->getFields(TRUE);

    foreach ($table as &$viewField) {
      if (!empty($viewField['real field']) && preg_match('#^([\w_]+)__distance$#', $viewField['real field'], $matches)) {
        if (isset($fields[$matches[1]]) && $fields[$matches[1]]->getType() === 'location') {
          $viewField['field']['id'] = 'mapkit_distance_field';
          $viewField['filter']['id'] = 'mapkit_proximity_filter';
          $viewField['argument']['id'] = 'mapkit_proximity_argument';
        }
      }
    }
  }
}

/**
 * Implements hook_views_post_render().
 */
function mapkit_views_post_render(ViewExecutable $view, &$output, CachePluginBase $cache) {
  // If the search query doesn't include a search location, then distance
  // will not have a value and can be cacheable.
  $query = $view->query;
  if (!$query instanceof SearchApiQuery || !$query->getOption('search_api_location')) {
    return;
  }

  $display = $view->getDisplay();
  $row_plugin = $display->getPlugin('row');

  // If views includes a display of DistanceField, which is dynamic per
  // location query, ensure that the rendered row doesn't get cached for all
  // views display that contain this value.
  //
  // @todo Identify other views fields that are dynamic and should not cache
  // the rows containing them.
  if ($row_plugin instanceof Fields) {
    foreach ($display->getHandlers('field') as $field) {
      if ($field instanceof MapkitDistanceField && empty($field->options['exclude'])) {
        ViewsCache::disableRowCache($output['#rows']);
        $cache->options['output_lifespan'] = 0;
        break;
      }
    }
  }
}
